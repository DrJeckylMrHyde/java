import com.company.Zadanie146Testy.Zadanie146;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestZadanie146 {

    @Test
    void czyZwracaLiczby() {
        double wynik = Zadanie146.obliczPodatek(100.0,23);
        Assertions.assertEquals(123.0,wynik);
    }

    @Test
    void sprawdzGdyPodatekJestUjemny() {
        double wynik = Zadanie146.obliczPodatek(100.0,-23);
        Assertions.assertEquals(100.0,wynik);
    }

    @Test
    void sprawdzGdyPodatekJest100() {
        double wynik = Zadanie146.obliczPodatek(100.0,100);
        Assertions.assertEquals(200.0,wynik);
    }

    @Test
    public void sprawdzCoBedzieGdyPodatekJestpowyzej105(){
        double wynik = Zadanie146.obliczPodatek(100.0,105);
        Assertions.assertEquals(100,wynik);
    }
    @Test
    public void sprawdzCoBedzieGryPodatekJest0(){
        double wynik = Zadanie146.obliczPodatek(100.0,0);
        Assertions.assertEquals(100,wynik);
    }


}
