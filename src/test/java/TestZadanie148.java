import com.company.Zadanie148.SumaElementow;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

 class TestZadanie148 {

    private SumaElementow sumaElementow = new SumaElementow();

    @Test
    void sprawdzeniePoprawnegoZakresuLiczb() {
        int[] tablica = {1, 2, 3, 4, 5, 6};
        Integer wynik = sumaElementow.ileElementowZsumowac(tablica, 9);
        Assertions.assertEquals(Integer.valueOf(4), wynik);
    }

    @Test
    void sprawdzenieJesliNieZnalezionoLiczby() {
        int[] tablica = {1, 2, 3, 4};
        Integer wynik = sumaElementow.ileElementowZsumowac(tablica, 11);
        Assertions.assertNull(wynik);
    }

    @Test
    void sprawdzenieJesliTablicaJestPusta() {
        int[] tablica = {};
        Integer wynik = sumaElementow.ileElementowZsumowac(tablica, 11);
        Assertions.assertNull(wynik);
    }

    @Test
    void sprawdzenieJesliPierwszaLiczbaSpelniaWarunek() {
        int[] tablica = {5, 8, 3, 6};
        Integer wynik = sumaElementow.ileElementowZsumowac(tablica, 4);
        Assertions.assertEquals(Integer.valueOf(1), wynik);
    }

    @Test
    void sprawdzenieJesliTablicaJestNull() {
        Integer wynik = sumaElementow.ileElementowZsumowac(null, 4);
        Assertions.assertNull(wynik);
    }
}
