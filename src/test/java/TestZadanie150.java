import com.company.Zadanie150.Zadanie150;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestZadanie150 {

    private Zadanie150 zadanko = new Zadanie150();

    @Test
    void ifArrayOK() {
        int expLength = 4;
        int[] expectedArray = {10, 11, 12, 13};
        int[] result = zadanko.zwrocTabliceOkreslonejDLugosci(expLength);
        Assertions.assertEquals(expLength, result.length);
        Assertions.assertArrayEquals(expectedArray, result);
    }

    @Test
    void shouldReturnArrayEmptyWhenPassedZero() {
        int[] expectedArray = {};
        int[] result = zadanko.zwrocTabliceOkreslonejDLugosci(0);
        Assertions.assertArrayEquals(expectedArray, result);
    }

    @Test
    void shouldReturnNegativeValues() {
        Assertions.assertThrows(NegativeArraySizeException.class,
                //lambda exp;functional programming
                () -> zadanko.zwrocTabliceOkreslonejDLugosci(-3));
    }

    @Test
    void shouldReturnArrayWhenMaxValue() {
        //ToDo - fix after classes from JVM (Java Virtual Machine)
        int expLength = Integer.MAX_VALUE;
        Assertions.assertThrows(OutOfMemoryError.class,
                () -> zadanko.zwrocTabliceOkreslonejDLugosci(expLength));
        int[] result = zadanko.zwrocTabliceOkreslonejDLugosci(expLength);
        Assertions.assertEquals(expLength, result.length);
    }

}
