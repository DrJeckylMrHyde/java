import com.company.Zadanie149.Zadanie149;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestZadanie149 {

    Zadanie149 zadanie149 = new Zadanie149();

    @Test
    void sprawdzenieCzyLiczbaJestWPrzedziale() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(0, 10, 5);
        Assertions.assertTrue(wynik);
    }

    @Test
    void sprawdzanieGdyLiczbaJestPrzedPrzedzialem() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(0, 10, -1);
        Assertions.assertFalse(wynik);
    }

    @Test
    void sprawdzanieGdyLiczbaJestZaPrzedzialem() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(0, 10, 15);
        Assertions.assertFalse(wynik);
    }

    @Test
    void sprawdzenieZlegoPrzedzialu() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(5, 2, 4);
        Assertions.assertFalse(wynik);
    }

    @Test
    void sprawdzenieGdyStartIStopSaRowne() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(5, 5, 7);
        Assertions.assertFalse(wynik);
    }

    @Test
    void sprawdzenieGdyStartStopILiczbaSaTeSame() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(5, 5, 5);
        Assertions.assertTrue(wynik);
    }
}
