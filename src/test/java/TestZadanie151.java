import com.company.Zadanie151.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestZadanie151 {
    private Person osoba;

    //metoda wywoła się co obrót przed każym testem(i będzie przypisywany nowy obiekt klasy User)
    @BeforeEach
    void ustawDaneOsoby() {
        osoba = new Person();
    }

    //Testy metody pobierzPelneDane(imie+nazwisko)
    @Test
    public void gdyPodaneImieOrazNazwisko() {
        osoba.setImie("Joanna");
        osoba.setNazwisko("Chomik");
        Assertions.assertEquals("Joanna Chomik", osoba.pobierzPelneDane());
    }

    @Test
    public void gdyPodaneTylkoImie() {
        osoba.setImie("Joanna");
        Assertions.assertEquals("Joanna", osoba.pobierzPelneDane());
    }

    @Test
    public void gdyPodaneTylkoNazwisko() {
        String nazwisko = "Chomik";
        osoba.setNazwisko(nazwisko);
        Assertions.assertEquals(nazwisko, osoba.pobierzPelneDane());
    }

    @Test
    public void gdyNiePodanoImieniaINazwiska() {
        Assertions.assertNull(osoba.pobierzPelneDane());
    }

    //testy dla metody czy osoba jest pełnoletnia:
    @Test
    public void czyZwrociTrueGdyOsobaJestPelnoletnia() {
        osoba.setWiek(19);
        Assertions.assertTrue(osoba.czyOsobaJestPelnoletnia());
    }

    @Test
    public void czyZwrociFalseGdyOsobaNieJestPelnoletnia() {
        osoba.setWiek(14);
        Assertions.assertFalse(osoba.czyOsobaJestPelnoletnia());
    }

    @Test
    public void czyWrociFalseGdyWiekJestUjemny() {
        Assertions.assertEquals(0, osoba.getWiek());
        //sprawdzam czy wartosc jest 0

        int wiekDobry = 15;
        osoba.setWiek(wiekDobry);
        Assertions.assertEquals(wiekDobry, osoba.getWiek());
        //ustawiam wartosc negatywna
        int wiekZly = -4;
        osoba.setWiek(wiekZly);
        Assertions.assertEquals(wiekDobry, osoba.getWiek());
        Assertions.assertNotEquals(wiekZly, osoba.getWiek());
    }

    //testy dla metody ile do emerytury
    @Test
    void czyTestDlaKobietyZwrociDobryWiekDoEmerytury() {
        int spodziewanyWiek = 66;

        osoba.setWiek(spodziewanyWiek);
        osoba.setCzyMezczyzna(false);

        Assertions.assertEquals(0, osoba.ileLatDoEmerytury());
    }

    @Test
    void czyTestDlaMezczyznZwrociDobryWiekDoEmerytury() {
        osoba.setWiek(80);
        osoba.setCzyMezczyzna(true);

        Assertions.assertEquals(0, osoba.ileLatDoEmerytury());
    }

    @Test
    void testCzyZwroCiPoprawnyWiekDoEmeryturyDlaKobietPonizej65() {
        osoba.setWiek(50);
        osoba.setCzyMezczyzna(false);

        Assertions.assertEquals(15, osoba.ileLatDoEmerytury());
    }

    @Test
    void testCzyZwroCiPoprawnyWiekDoEmeryturyDlaMezczyznyPonizej67() {
        osoba.setWiek(50);
        osoba.setCzyMezczyzna(true);

        Assertions.assertEquals(17, osoba.ileLatDoEmerytury());
    }

    @Test
    void testGdyNiePodanoWiekuOsoby() {
        osoba.setCzyMezczyzna(false);
        Assertions.assertEquals(-1, osoba.ileLatDoEmerytury());

        osoba.setCzyMezczyzna(true);
        Assertions.assertEquals(-1, osoba.ileLatDoEmerytury());
    }

    @Test
    void brakImieniaDoLoginu() {
        osoba.setNazwisko("Chomik");
        Assertions.assertEquals("XXXcho6", osoba.utworzLogin());
    }

    @Test
    void brakNazwiskaDoLoginu() {
        osoba.setImie("Anna");
        Assertions.assertEquals("annYYY4", osoba.utworzLogin());
    }

    @Test
    void brakImieniaINazwiska() {
        Assertions.assertNull(osoba.utworzLogin());
    }

    @Test
    void daneKrotszeNiz3Znaki(){
        osoba.setImie("Ma");
        osoba.setNazwisko("Wegner");
        Assertions.assertEquals("maZweg8",osoba.utworzLogin());
    }
}
