import com.company.Zadanie147.Kalkulator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestZadanie147 {

    Kalkulator kalk = new Kalkulator();

    @Test
    public void sprawdzCzySumujePoprawnie() {
        Integer wynik = kalk.sumowanieDwochLiczb(5, 5);
        //Integer valueOf zwraca typ klasowy
        Assertions.assertEquals(Integer.valueOf(10),wynik);
    }

    @Test
    public void sprawdzCzySumujePoprawnieKiedyPierwszaWartoscNull() {
        Integer wynik = kalk.sumowanieDwochLiczb(null, 5);
        Assertions.assertEquals(Integer.valueOf(5), wynik);
    }

    @Test
    public void sprawdzCzySumujePoprawnieKiedyDrugaWartoscNull() {
        Integer wynik = kalk.sumowanieDwochLiczb(5, null);
        Assertions.assertEquals(Integer.valueOf(5), wynik);
    }

    @Test
    public void sprawdzCzySumujePoprawnieKiedyObieWartosciSaNullem() {
        Integer wynik = kalk.sumowanieDwochLiczb(null, null);
        //metoda assertNull oznacza ze wynikiem musi byc NULL
        Assertions.assertNull(wynik);

    }

    @Test
    public void sprawdzCzySumujePoprawnieKiedyObieSaMax() {
        Integer wynik = kalk.sumowanieDwochLiczb(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Assertions.assertEquals(Integer.valueOf(-2), wynik);
    }

    @Test
    public void sprawdzCzyPoprawnieSumujeWieleLiczb() {
        Integer wynik = kalk.sumwanieWieluLiczb(5, 10, 15, 20);
        Assertions.assertEquals(Integer.valueOf(50), wynik);
    }

    @Test
    public void sprawdzCzyPoprawnieSumujeKiedyTablicaJestPusta() {
        Integer wynik = kalk.sumwanieWieluLiczb(null);
        Assertions.assertNull(wynik);
    }

    @Test
    public void sprawdzCzyPoprawnieSumujeKiedyNicNiePrzekazuje() {
        Integer wynik = kalk.sumwanieWieluLiczb();
        Assertions.assertEquals(Integer.valueOf(0), wynik);
    }

    @Test
    public void sprawdzCzyOdejmuje() {
        Integer wynik = kalk.odejmowanieDwochLiczb(3, 3);
        Assertions.assertEquals(Integer.valueOf(0), wynik);
    }

    @Test
    public void sprawdzGdyNull1() {
        Integer wynik = kalk.odejmowanieDwochLiczb(null, 4);
        Assertions.assertEquals(Integer.valueOf(-4), wynik);
    }

    @Test
    public void sprawdzGdyNull2() {
        Integer wynik = kalk.odejmowanieDwochLiczb(4, null);
        Assertions.assertEquals(Integer.valueOf(4), wynik);
    }

    @Test
    public void sprawdzGdyNull1i2() {
        Integer wynik = kalk.odejmowanieDwochLiczb(null, null);
        Assertions.assertNull(wynik);
    }
}
