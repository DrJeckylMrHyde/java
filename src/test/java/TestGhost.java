import com.company.CodeWars.Ghost;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestGhost {

    private Ghost ghost = new Ghost();

    @Test
    void isRandomColorOK() {
        String [] colorTable = new String []{"white",
                "yellow",
                "purple",
                "red"};
        String color = ghost.getColor();
        switch (color){
            case "white":
                Assertions.assertEquals("white",color);

            case "yellow":
                Assertions.assertEquals("yellow",color);

            case "purple":
                Assertions.assertEquals("purple",color);

            case "red":
                Assertions.assertEquals("red",color);
        }
    }
}
