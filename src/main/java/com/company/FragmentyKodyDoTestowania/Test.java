package com.company.FragmentyKodyDoTestowania;

public class Test {
    public static void main(String args[]) {
        Base obj1 = new Derived();
        Derived obj2 = new Derived();
        // As per overriding rules this should call to class Derive's static
        // overridden method. Since static method can not be overridden, it
        // calls Base's display()
        obj1.display();
        obj2.display();
    }
}