package com.company.FragmentyKodyDoTestowania;

public class Base {
    // Static method in base class which will be hidden in subclass
    public static void display() {
        System.out.println("Static or class method from Base");
    }
}

        // Subclass
        class Derived extends Base {
// This method hides display() in Base
public static void display() {
        System.out.println("Static or class method from Derived");
        }
        }

