package com.company.Zadanie121;

//klasa abstrakcyjna powoduje ze nie moge tworzyc obiektow tej klasy
public abstract class Pojazd {
    protected String nazwa;
    protected int maxPredkosc;
    protected int liczbaPasazerow;

    public Pojazd(String nazwa, int maxPredkosc, int liczbaPasazerow) {
        this.nazwa = nazwa;
        this.maxPredkosc = maxPredkosc;
        this.liczbaPasazerow = liczbaPasazerow;
    }

    @Override
    public String toString() {
        return String.format("Prezentowany pojazd to %s jego maksymalna prędkość wynosi %s km/h, jest w stanie przewieźć %s pasażerów",
                nazwa,
                maxPredkosc,
                liczbaPasazerow);
    }

    //jesli metoda jest abstrakcyjna to kazda klasa dziedziczaca musi miec nadpisana te metode
    abstract void przedstawSie();
}
