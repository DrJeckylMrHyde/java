package com.company.Zadanie121;

public class Pociag extends Pojazd {
    private int liczbaWagonow;
    private boolean czyMaWagonBarowy;

    public Pociag(String nazwa, int maxPredkosc, int liczbaPasazerow, int liczbaWagonow, boolean czyMaWagonBarowy) {
        super(nazwa, maxPredkosc, liczbaPasazerow);
        this.liczbaWagonow = liczbaWagonow;
        this.czyMaWagonBarowy = czyMaWagonBarowy;
    }

    @Override
    void przedstawSie() {

    }
}
