package com.company.Zadanie121;

public class Samochod extends Pojazd {
    private int LiczbaKol = 4;
    private int LiczbaDrzwi = 5;

    public Samochod(String nazwa, int maxPredkosc, int liczbaPasazerow, int liczbaKol, int liczbaDrzwi) {
        super(nazwa, maxPredkosc, liczbaPasazerow);
        LiczbaKol = liczbaKol;
        LiczbaDrzwi = liczbaDrzwi;
    }

    public Samochod(String nazwa, int maxPredkosc, int liczbaPasazerow) {
        super(nazwa, maxPredkosc, liczbaPasazerow);
    }

    @Override
    void przedstawSie() {
        System.out.printf("Nazwa: %s maxPredkosc: %s km/h liczbaPasażerów: %s",
                nazwa,
                maxPredkosc,
                liczbaPasazerow);
    }
}
