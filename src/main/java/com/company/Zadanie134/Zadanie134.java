package com.company.Zadanie134;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Zadanie134 {
    public static void main(String[] args) {
        try {
            zwracaLiczbeLiniiIZnakow("plikiDoCwiczenOdZad132/zadanie132.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void zwracaLiczbeLiniiIZnakow(String sciezkaDoPliku) throws IOException {
        BufferedReader bufor = new BufferedReader(new FileReader(sciezkaDoPliku));
        String line = bufor.readLine();
        int lineCounter = 0;
        int signCounter = 0;
        while (line != null) {
            signCounter += line.length();
            lineCounter++;
            line = bufor.readLine();
        }
        System.out.println("Ilość linii wynosi: " + lineCounter + ", ilość znaków wynosi: " + signCounter);
    }
}
