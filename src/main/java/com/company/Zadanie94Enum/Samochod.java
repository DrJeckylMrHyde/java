package com.company.Zadanie94Enum;

public class Samochod {
    private String nazwa;
    private int rocznik;
    private Kolor kolor;

    public Samochod(String nazwa, int rocznik, Kolor kolor){
        this.nazwa = nazwa;
        this.rocznik = rocznik;
        this.kolor = kolor;

    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s",nazwa,rocznik,kolor);
    }
}
