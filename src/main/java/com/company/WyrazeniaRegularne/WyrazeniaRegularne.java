package com.company.WyrazeniaRegularne;

public class WyrazeniaRegularne {
    public static void main(String[] args) {
        String imie = "Jan Marian Kowalski";
        String mail = "marian.kowalski@gmail.com";
        String numer = "784888567";

        System.out.println(czyImie(imie));
        System.out.println(czyMail(mail));
        System.out.println(czyNumer(numer));

    }

    public static boolean czyImie(String imie){
        return imie.toLowerCase().matches("[a-z]{3,}( [a-z]{3,})? [a-z]{3,}");
    }
    public static boolean czyMail(String mail){
        return mail.toLowerCase().matches("[a-z1-9\\.]+@[a-z1-9]+\\.[a-z]{2,}");
    }
    public static boolean czyNumer(String numer){
        //albo w regex | zamiast typowego ||
        return numer.toLowerCase().matches("[0-9]{9}|([0-9]{3}-){2}[0-9]{3}");
    }
}
