package com.company.Zadanie165Fabryka;

public class HtmlDocument extends Document {

    public HtmlDocument(DocumentGenerator.DocumentType type, String content) {
        super(type, formatText(content));


    }

    private static String formatText(String text) {
        String bold = String.format("<h1>%s</h1>", text);
        return bold.replaceAll("\n","</br>");
    }
}
