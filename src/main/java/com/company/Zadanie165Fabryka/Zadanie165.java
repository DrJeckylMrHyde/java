package com.company.Zadanie165Fabryka;

import com.company.Zadanie165Fabryka.DocumentGenerator.DocumentType;

import static com.company.Zadanie165Fabryka.DocumentGenerator.DocumentType.*;

public class Zadanie165 {
    public static void main(String[] args) {
        DocumentGenerator generator = new DocumentGenerator();

        String text = "Jest niedziela\nale nie pada śnieg";

        //ostatecznie otrzymuje document ale dla kazdego typu dziala nieco inaczej
        Document txt = generator.createDocument(text, TXT);
        txt.saveFile("Zadanie165");

        Document html = generator.createDocument(text, HTML);
        html.saveFile("Zadanie165");
    }
}
