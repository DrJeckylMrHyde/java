package com.company.Zadanie165Fabryka;

import com.company.Zadanie165Fabryka.DocumentGenerator.DocumentType;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

class Document {
    private DocumentType type;
    private String content;

    Document(DocumentType type, String content) {
        this.type = type;
        this.content = content;
    }

    void saveFile(String fileName) {
        String outputPath = String.format("plikiDoCwiczenOdZad132/%s.%s",
                fileName,
                type.getExtension());
        //to jest taki uchwyt do pliku
        File file = new File(outputPath);
        // try with resources
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    String getContent() {

        return content;
    }
}
