package com.company.Zadanie165Fabryka;

class DocumentGenerator {

    // korzystamy tutaj z kompozycji, czyli wykorzystania klasy wewnatrz innej bez dziedziczenia
    Document createDocument(String textToSave, DocumentType type) {
        Document document = null;
        switch (type) {
            case TXT:
                document = new TxtDocument(type,textToSave);
                break;
            case HTML:
                document = new HtmlDocument(type,textToSave);
                break;
        }
        return document;
    }

    enum DocumentType {
        TXT("txt"),
        HTML("html");

        //pole jest potrzebne jesli enum ma dodatkowa wartosc
        private String extension;

        DocumentType(String extension) {

            this.extension = extension;
        }

        String getExtension() {

            return extension;
        }
    }
}
