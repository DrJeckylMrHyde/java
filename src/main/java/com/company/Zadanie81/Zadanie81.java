package com.company.Zadanie81;

public class Zadanie81 {
    public static void main(String[] args) {
//        System.out.println(sumaDlugosciWyrazow("Ala ma kota"));
        System.out.println(sumaDlugosciWyrazow2("Ala ma kota"));
    }

    public static int sumaDlugosciWyrazow(String zdanie){
        int suma = 0;
        String [] tablica = zdanie.split(" ");
        for (String s : tablica) {
           suma += s.length();
        }
        return suma;
    }

    public static int sumaDlugosciWyrazow2(String zdanie){
        return zdanie.replace(" ","").length();
    }
}
