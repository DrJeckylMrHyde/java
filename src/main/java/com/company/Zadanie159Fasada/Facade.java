package com.company.Zadanie159Fasada;

import java.util.Arrays;
import java.util.List;

public class Facade {
    private final int ENTERTEMP = 21;
    private final int OUTTEMP = 8;
    private List<Windows> windowsList = Arrays.asList(
            new Windows(),
            new Windows(),
            new Windows(),
            new Windows()
    );
    private Alarm alarm1 = new Alarm();
    private Light light1 = new Light();
    private Heating heating1 = new Heating();

    void imOut(){
        windowsList
                .stream()
//                .forEach(windows -> windows.close());
                .forEach(windows -> windows.close());
        alarm1.enable();
        light1.switchOn();
        heating1.setTemp(OUTTEMP);
    }

    void imBack(){
        windowsList
                .stream()
//                .forEach(windows -> windows.close());
                .forEach(windows -> windows.open());
        alarm1.disable();
        light1.switchOff();
        heating1.setTemp(ENTERTEMP);
    }

}
