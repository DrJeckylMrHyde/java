package com.company.Zadanie159Fasada;

import java.util.Scanner;

public class Zadanie159 {
    private static final int ENTERCODE = 1;
    private static final int OUTCODE = 2;

    public static void main(String[] args) {
        Facade panel = new Facade();
        Scanner input = new Scanner(System.in);
        System.out.println("Możliwe opcje: ");
        System.out.println(ENTERCODE + ". Wejście");
        System.out.println(OUTCODE + ". Wyjście");

        int decyzja = input.nextInt();
        switch (decyzja) {
            case ENTERCODE:
                panel.imBack();
                break;
            case OUTCODE:
                panel.imOut();
                break;
            default:
                break;
        }
    }
}
