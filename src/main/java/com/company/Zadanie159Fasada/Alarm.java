package com.company.Zadanie159Fasada;

public class Alarm {

    void enable() {
        System.out.println("Alarm włączony");
    }

    void disable() {
        System.out.println("Alarm wyłączony");
    }
}
