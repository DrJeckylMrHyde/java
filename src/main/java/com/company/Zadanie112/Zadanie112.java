package com.company.Zadanie112;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Zadanie112 {

    public static void main(String[] args) {
        System.out.println(zwracaMapeZLiczbaWystapien());
    }

    private static Map<Integer, Integer> zwracaMapeZLiczbaWystapien() {
        Map<Integer, Integer> mapa = new HashMap<>();
        Scanner inputNum = new Scanner(System.in);

        while (true) {
            System.out.println("Podaj liczbę do mapy");
            Integer wprowadzonaLiczba = inputNum.nextInt();
            if (wprowadzonaLiczba == -1) {
                break;
            }
            //sprawdza czy mapa posiada klucz
            if (mapa.containsKey(wprowadzonaLiczba)) {
                int staraLiczbaWystapien = mapa.get(wprowadzonaLiczba);
                // jesli sie powtarza to nadpisuje kazdorazowo ten klucz
                mapa.put(wprowadzonaLiczba, staraLiczbaWystapien + 1);
            } else {
                mapa.put(wprowadzonaLiczba, 1);
            }
        }
        return mapa;
    }
}
