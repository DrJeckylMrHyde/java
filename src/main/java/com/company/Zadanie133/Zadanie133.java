package com.company.Zadanie133;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Zadanie133 {
    public static void main(String[] args) {
        try {
            odczytajPlikLiniaPoLinii("plikiDoCwiczenOdZad132/zadanie132.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void odczytajPlikLiniaPoLinii(String sciezkaDoPliku) throws IOException {
        FileReader plik = new FileReader(sciezkaDoPliku);
        BufferedReader bufor = new BufferedReader(plik);
        String linia = bufor.readLine();
        while(linia != null){
            System.out.println(linia);
            linia = bufor.readLine();
        }
    }
}
