package com.company.ProteinChain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProteinChain {
    public static void main(String[] args) {
        System.out.println(changePossible("ACBBCA","BCBACA"));
        System.out.println(changePossible2("ACBBCA","BCBACA"));
    }

    private static boolean changePossible(String chain1, String chain2) {
        if (chain1.length() != chain2.length()) {
            return false;
        }
        List<Integer> pozycje = new ArrayList<>();
        char[] chainArr1 = chain1.toCharArray();
        char[] chainArr2 = chain2.toCharArray();

        for (int i = 0; i < chain2.length(); i++) {
            if (chainArr1[i] != chainArr2[i]) {
                pozycje.add(i);
                if (pozycje.size() > 2) {
                    return false;
                }
            }
        }
        if (pozycje.size() == 1) {
            return false;
        }
        char bufor = chainArr2[pozycje.get(0)];
        chainArr2[pozycje.get(0)] = chainArr2[pozycje.get(1)];
        chainArr2[pozycje.get(1)] = bufor;
        // tablice porownuje Arrays.equals
        return Arrays.equals(chainArr1, chainArr2);
    }

    private static boolean changePossible2(String chain1, String chain2){
        char[] chainArr1 = chain1.toCharArray();
        char[] chainArr2 = chain2.toCharArray();
        Arrays.sort(chainArr1);
        Arrays.sort(chainArr2);
        return Arrays.equals(chainArr1,chainArr2);
    }
}
