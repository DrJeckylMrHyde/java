package com.company.Zadanie119;

public class Osoba {

    private String imie;
    private int wiek;
    private Plec plec;

    enum Plec{
        KOBIETA,MEZCZYZNA
    }

    public Osoba(String imie, int wiek, Plec plec) {
        this.imie = imie;
        this.wiek = wiek;
        this.plec = plec;
    }

    @Override
    public String toString() {
        return String.format("Mam na imię %s mam %s lat i jestem %s", imie, wiek, plec);
    }
}
