package com.company.Zadanie119;

import java.util.HashMap;
import java.util.Map;

public class Student extends Osoba {
    private Uczelnia uczelnia;
    private int rokStudiow;
    // nie dodaje mapy ocen bo student nie zna
    private Map<PrzedmiotUczelniany, Integer> oceny = new HashMap<>();

    public Student(String imie, int wiek, Plec plec, int rokStudiow) {
        super(imie, wiek, plec);
        this.rokStudiow = rokStudiow;
    }

    void dodajOcene (PrzedmiotUczelniany przedmiotUczelniany, int ocena){
        oceny.put(przedmiotUczelniany,ocena);
    }

    @Override
    public String toString() {

        return super.toString() +" jestem na "+ " i mam takie oceny:"+wyswietlaMape();
    }

    String wyswietlaMape() {
        StringBuilder output = new StringBuilder();
        for (PrzedmiotUczelniany przedmiotUczelniany : oceny.keySet()) {
            // pobranie listy klucz
            output.append(" ")
                    .append(przedmiotUczelniany)
                    .append(": ")
                    .append(oceny.get(przedmiotUczelniany));
        }
        return output.toString();
    }
}
