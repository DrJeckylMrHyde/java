package com.company.Zadanie119;

import java.util.ArrayList;
import java.util.List;

public class Uczelnia {
    private String nazwaUczelni;
    private String adres;
    private List<Student> listaStudentow = new ArrayList<>();
    private List<Wykladowca> listaWykladowcow = new ArrayList<>();

    public Uczelnia(String nazwaUczelni, String adres) {
        this.nazwaUczelni = nazwaUczelni;
        this.adres = adres;
    }

    void dodajStudenta(Student student) {
        listaStudentow.add(student);
    }

    void zatrudnijWykladowce(Wykladowca wykladowca) {
        listaWykladowcow.add(wykladowca);
    }

    void wyswietlListeStudentow(){
        for (Student student : listaStudentow) {
            System.out.println(student);
        }
    }
}
