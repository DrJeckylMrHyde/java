package com.company.Zadanie119;

public class Wykladowca extends Osoba{

    private StopienNaukowy stopienNaukowy;


    enum StopienNaukowy{
        MAGISTER,DOKTOR,PROFESOR
    }

    public Wykladowca(String imie, int wiek, Plec plec, StopienNaukowy stopienNaukowy) {
        super(imie, wiek, plec);
        this.stopienNaukowy = stopienNaukowy;
    }
}
