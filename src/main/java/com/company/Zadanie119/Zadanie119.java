package com.company.Zadanie119;


import com.company.Zadanie119.Wykladowca.StopienNaukowy;

import static com.company.Zadanie119.Osoba.Plec.MEZCZYZNA;

public class Zadanie119 {
    public static void main(String[] args) {
        Student StudentMatematyki = new Student("Marcin", 24, MEZCZYZNA, 4);
        Wykladowca wykladowcaMatematyki = new Wykladowca("Janusz", 65, MEZCZYZNA, StopienNaukowy.PROFESOR);
        // parametrem przedmiotu jest obiekt wykladowcaMatematyki
        PrzedmiotUczelniany matematyka = new PrzedmiotUczelniany("matematyka", wykladowcaMatematyki);
        PrzedmiotUczelniany fizyka = new PrzedmiotUczelniany("fizyka", wykladowcaMatematyki);
        StudentMatematyki.dodajOcene(matematyka, 4);
        StudentMatematyki.dodajOcene(fizyka, 5);
        Uczelnia Politechnika = new Uczelnia("Politechnika", "al. Politechniki");
        Politechnika.zatrudnijWykladowce(wykladowcaMatematyki);
        Politechnika.dodajStudenta(StudentMatematyki);
        Politechnika.wyswietlListeStudentow();

    }
}
