package com.company.Zadanie119;

public class PrzedmiotUczelniany {
    private String nazwa;
    private Wykladowca wykladowca;

    public PrzedmiotUczelniany(String nazwa, Wykladowca wykladowca) {
        this.nazwa = nazwa;
        this.wykladowca = wykladowca;
    }

    @Override
    public String toString() {
        return nazwa;
    }
}
