package com.company.Zadanie147;

public class Kalkulator {

    public Integer sumowanieDwochLiczb(Integer liczba1, Integer liczba2) {
        if (liczba1 == null) {
            return liczba2;
        }
        if (liczba2 == null) {
            return liczba1;
        }
        return liczba1 + liczba2;
    }

    public Integer sumwanieWieluLiczb(int... liczby) {
        if (liczby == null) {
            return null;
        }
        int suma = 0;
        for (int i : liczby) {
            suma += i;
        }
        return suma;
    }

    public Integer odejmowanieDwochLiczb(Integer liczba1, Integer liczba2) {
        if (liczba1 == null && liczba2 == null) {
            return null;
        }
        if (liczba1 == null) {
            return -liczba2;
        }
        if (liczba2 == null) {
            return liczba1;
        }

        return liczba1 - liczba2;
    }
}
