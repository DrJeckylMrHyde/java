package com.company.zadanie174Thread;

public class Zadanie174 {
    public static void main(String[] args) {
//        metoda1();
//        metoda2();
//        metoda3();
//        metoda4();
//        metoda5();

    }

    private static void metoda5() {
        System.out.println("wyświetl w jakim wątku (ID) pracuje main: " + Thread.currentThread().getId());
        WlasnyWatek w1 = new WlasnyWatek();
        WlasnyWatek w2 = new WlasnyWatek();
        w1.start();
        w2.start();
        try {
//            metoda join kaze poczekac
            w1.join();
            w2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("wyświetl w jakim wątku (ID) pracę kończy main: " + Thread.currentThread().getId());
    }

    private static void metoda4() {
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                long id = Thread.currentThread().getId();
                for (int i = 0; i < 20; i++) {
                    System.out.printf("%s (id = %s)\n",
                            i,
                            id);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread t1 = new Thread(r1);
        t1.start();
    }

    private static void metoda3() {
        Thread t1 = new Thread() {
            @Override
            public void run() {
                long id = Thread.currentThread().getId();
                for (int i = 0; i < 20; i++) {
                    System.out.printf("%s (id = %s)\n",
                            i,
                            id);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread t2 = new Thread() {

            @Override
            public void run() {
                for (int i = 0; i < 20; i++) {
                    long id = Thread.currentThread().getId();
                    System.out.printf("%s (id = %s)\n",
                            i,
                            id);
                    try {
                        Thread.sleep(800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        t1.start();
        t2.start();
    }

    ;

    private static void metoda2() {
        Thread t1 = new Thread() {
            @Override
            public void run() {
                System.out.println("Identyfikator nowego wątku " + Thread.currentThread().getId());
            }
        };
        t1.start();
    }

    private static void metoda1() {
        System.out.println("Identyfikator wątku " + Thread.currentThread().getId());
    }
}
