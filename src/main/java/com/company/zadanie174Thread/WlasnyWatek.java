package com.company.zadanie174Thread;

public class WlasnyWatek extends Thread {

    @Override
    public void run() {
        long id = Thread.currentThread().getId();
        System.out.println("Wątek startuje " + id);
        for (int i = 0; i < 20; i++) {
            System.out.printf("%s (id = %s)\n",
                    i,
                    id);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Wątek konczy " + Thread.currentThread().getId());
    }
}
