package com.company.Zadanie140;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Zadanie140 {
    public static void main(String[] args) {
        int [] table = new int []{1,2,3,4,5,6,7,8,9};
        try {
            String komunikat = odczytajPlikLiniaPoLinii("plikiDoCwiczenOdZad132/Zadanie140.txt");
            pokazMaxITablice(table,komunikat);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void pokazMaxITablice(int[] table, String odczytajPlikLiniaPoLinii) {

        if (odczytajPlikLiniaPoLinii.equals("tak")) {
            System.out.printf("Największa liczba to: %s \nZbiór: %s", znajdzMax(table), Arrays.toString(table));
        }
    }

    private static String odczytajPlikLiniaPoLinii(String sciezkaDoPliku) throws IOException {
        BufferedReader bufor = new BufferedReader(new FileReader(sciezkaDoPliku));
        return bufor.readLine();
    }

    private static int znajdzMax(int [] table){
        int max = table[0];
        for (int i = 1; i < table.length; i++) {
            if (table[i] > max) {
                max = table[i];
            }
        }
        return max;
    }
}
