package com.company.Zadanie150;

import java.util.Arrays;

public class Zadanie150 {

//    public int [] zwrocTabliceOkreslonejDLugosci(int dlugoscTablicy){
//        int [] tablica = new int [dlugoscTablicy];
//        for (int i = 0; i < dlugoscTablicy; i++) {
//            tablica[i] = 10 + i;
//        }
//        return tablica;
//    }

    public int[] zwrocTabliceOkreslonejDLugosci(int liczba) {
        int[] tablica = new int[liczba];
        for (int i = 0; i < liczba; i++) {
            tablica[i] = 10 + i;
        }
        return tablica;
    }
}
