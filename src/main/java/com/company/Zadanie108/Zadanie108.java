package com.company.Zadanie108;

public class Zadanie108 {
    private final static int limit = 50_000;
    private final static String napisTest = "ziomek";

    public static void main(String[] args) {


        long czasStart = System.currentTimeMillis();
        laczenieStringBuilder();
        long czasKoniec = System.currentTimeMillis();
        System.out.println("Czas konkatenacji String Builder: " + (czasKoniec - czasStart));

        czasStart = System.currentTimeMillis();
        laczenieStringBuffer();
        czasKoniec = System.currentTimeMillis();
        System.out.println("Czas konkatenacji String Buffer: " + (czasKoniec - czasStart));

        czasStart = System.currentTimeMillis();
        laczenie();
        czasKoniec = System.currentTimeMillis();
        System.out.println("Czas konkatenacji: " + (czasKoniec - czasStart));
    }

    static String laczenie() {
        String napis = "";
        for (int i = 0; i < limit; i++) {
            napis += napisTest;
        }

        return napis;
    }
    static String laczenieStringBuilder() {
        // StringBuilder magazynuje napisy w tablicy
        StringBuilder napis = new StringBuilder();
        for (int i = 0; i < limit; i++) {
            napis.append(napisTest);
        }

        return napis.toString();
    }

    static String laczenieStringBuffer() {
        // StringBuffer jest troche dluzszy ale jest odporny na wielowatkowosc
        StringBuffer napis = new StringBuffer();
        for (int i = 0; i < limit; i++) {
            napis.append(napisTest);
        }

        return napis.toString();
    }
}
