package com.company.Zadanie83;

public class Zadanie83 {
    public static void main(String[] args) {
        System.out.println(czyZawieraSamogloski2("KOTA"));
    }

    static boolean czyZawieraSamogloski(String wyraz) {
        String[] tablicaSamoglosek = new String[]{"a", "ą", "e", "ę", "o", "ó", "u", "i", "y"};
        for (String litera : tablicaSamoglosek) {
            if (wyraz.contains(litera)) {
                return true;
            }
        }
        return false;
    }

    static boolean czyZawieraSamogloski2(String wyraz) {
        wyraz = wyraz.toLowerCase();
        return wyraz.contains("a")
                || wyraz.contains("ą")
                || wyraz.contains("e")
                || wyraz.contains("ę")
                || wyraz.contains("o")
                || wyraz.contains("ó")
                || wyraz.contains("u")
                || wyraz.contains("y")
                || wyraz.contains("i");
    }
}
