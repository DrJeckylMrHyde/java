package com.company.Zadanie107;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Zadanie107 {
    public static void main(String[] args) {
        Set<Integer> secik = new HashSet<>(Arrays.asList(1,2,1,4,15,23,1,3,12));
        System.out.println(zwrociWiekszeOdPoszukiwanej(secik, 2));
    }

    private static Set<Integer> zwrociWiekszeOdPoszukiwanej(Set<Integer> startSet, double poszukiwana){
        Set<Integer> secik = new HashSet<>();
        for (Integer integer : startSet) {
            // integer jest w tym przypadku jedna liczba z Seta
            if(integer > poszukiwana){
                secik.add(integer);
            }
        }
        return secik;
    }
}
