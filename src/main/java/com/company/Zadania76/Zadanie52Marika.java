package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie52Marika {
    public static void main(String[] args) {
        int[] tab1 = {1, 2, 3, 4};
        int[] tab2 = {6, 7, 8, 2, 9};
        System.out.println(Arrays.toString(sumaTablic(tab1, tab2)));
    }


    static int[] sumaTablic(int[] tablica1, int[] tablica2) {
        int[] tabdluzsza = tablica1.length > tablica2.length ? tablica1 : tablica2;
        int[] tabkrotsza = tablica1.length < tablica2.length ? tablica1 : tablica2;
        int[] tablicaWynik = new int[2 * tabdluzsza.length];
        int n = tablica1.length - 1;
        int k = tablica2.length - 1;
        int parametr = 0;

        for (int i = 0; i < tablicaWynik.length; i++) {
            tablicaWynik[i] = -1;
        }

        for (int i = tablicaWynik.length - 1; i >= tablicaWynik.length - tabkrotsza.length; i--) {
            if (tablica1[n] + tablica2[k] + parametr <= 9) {

                tablicaWynik[i] = tablica1[n] + tablica2[k] + parametr;
                parametr = 0;
            } else {

                tablicaWynik[i] = (parametr + tablica1[n] + tablica2[k]) % 10;
                parametr = (parametr + tablica1[n] + tablica2[k] - tablicaWynik[i]) / 10;


            }

            k--;
            n--;
        }

        int licznik1 = k > n ? k : n;


        int licznik2 = tablicaWynik.length - tabkrotsza.length - 1;
        for (int licznik = licznik1; licznik >= 0; licznik--) {
            tablicaWynik[licznik2] = tabdluzsza[licznik] + parametr;
            licznik2--;

        }

        int count = 0;
        for (int i = 0; i < tablicaWynik.length; i++) {
            if (tablicaWynik[i] >= 0) {
                count += 1;
            }
        }


        int[] tablicaFinal = new int[count];
        int licznik3 = tablicaFinal.length - 1;
        for (int i = tablicaWynik.length - 1; i >= count; i--) {
            tablicaFinal[licznik3] = tablicaWynik[i];
            licznik3--;
        }
        return tablicaFinal;
    }
}
