package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie53 {
    public static void main(String[] args) {
        int [] tablica = new int [] {1,1};
        System.out.println(zwracaRozniceMiedzyMaxIMinWTablicy(tablica));
    }

    public static int zwracaRozniceMiedzyMaxIMinWTablicy(int[] table) {
        int max = table[0];
        int min = table[0];
        for (int i = 0; i < table.length; i++) {
            if (table[i] > max) {
                max = table[i];
            }
            if (table[i] < min) {
                min = table[i];
            }
        }
        return max - min;
    }
}
