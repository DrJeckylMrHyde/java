package com.company.Zadania76;

public class Zadanie30 {
    public static void main(String[] args) {
        showingMultiplyTable(10);
    }

    static void showingMultiplyTable(int size) {
        for (int i = 1; i <= size; i++) {               //pierwsza petla jest pionowa
            for (int j = 1; j <= size; j++) {
                System.out.printf("%s\t", i * j);         //druga petla jest pozioma
//                if (j == size) {
//                    System.out.printf("\n");
//               }

            }
            System.out.println();
        }
    }
}
