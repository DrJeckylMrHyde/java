package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie50 {
    public static void main(String[] args) {

        //System.out.println(iloscPowtorzenWTablicy(new int[]{1, 2, 3, 3, 3, 3, 4, 5}, 3));
        System.out.println(Arrays.toString(usuniePowtarzajacySieElement(new int[]{3, 33, 32, 13, 3, 3, 3, 3, 2}, 3)));
    }

    public static int iloscPowtorzenWTablicy(int[] table, int element) {
        int counter = 0;
        for (int poTablicy : table) {
            if (poTablicy == element) {
                counter++;
            }
        }
        return counter;
    }

    public static int[] usuniePowtarzajacySieElement(int[] table, int element) {
        int rozmiarNowejTablicy = table.length - iloscPowtorzenWTablicy(table, element);
        int[] tablica = new int[rozmiarNowejTablicy];
        //System.out.println(rozmiarNowejTablicy);
        int indexNowejTablicy = 0;                                  // musimy nadac nowa zmienna jaka jest indeks nowej tablicy,
        for (int i = 0; i < table.length; i++) {        // mowi pod jakim indeksem ma wyladowac wartosc w nowej tablicy
            if (element != table[i]) {
                tablica[indexNowejTablicy] = table[i];
                indexNowejTablicy++;
            }
        }
        return tablica;
    }
}
