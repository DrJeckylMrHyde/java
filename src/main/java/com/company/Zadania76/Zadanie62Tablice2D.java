package com.company.Zadania76;

import java.util.Arrays;
import java.util.Random;

public class Zadanie62Tablice2D {
    public static void main(String[] args) {
        wyswietliTabliceDwuwymiarowa(zwrocWypelnionTablice(4,3));
    }

    static int[][] zwrocWypelnionTablice(int wiersze, int kolumny) {               // pierwsze wsp traktuj jako wiersz, druga jako kolumne
        int[][] nowaTablica = new int[wiersze][kolumny];
        Random r = new Random();
        for (int i = 0; i < wiersze; i++) {
            for (int j = 0; j < kolumny; j++) {
                nowaTablica[i][j] = r.nextInt(100);
            }
        }
        return nowaTablica;
    }

    static void wyswietliTabliceDwuwymiarowa(int[][] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica[i].length; j++) {               // tablica[i].length sprawdza to dlugosc kazdego wiersza gdyby byly rozne
                System.out.print(tablica[i][j]+"\t");
            }
            System.out.println();
        }
    }


}
