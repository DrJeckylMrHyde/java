package com.company.Zadania76;

//Utwórz metodę, do której przekazujesz dwa parametry a ona zwróci informację
// (w formie wartości logicznej) czy *obydwie* są parzyste. Oraz metodę, która zwróci informację czy
// chociaż jeden z przekazywanych parametrów z nich* jest większy od zera.


public class Zadanie15 {
    public static void main(String[] args) {
        boolean rezultat = czyParzyste(1, 3);
        System.out.println(rezultat);
        System.out.println(czyParzyste(1, 3));
        boolean result = greaterThanZero(-1, 3);
        System.out.println(result);
    }

    static boolean czyParzyste(int pierwsza, int druga) {
        return pierwsza % 2 == 0 && druga % 2 == 0;                     // alt + enter wywołanie korekty kodu <- zółta lampeczka
    }

    static boolean greaterThanZero(int first, int second) {
        return first > 0 || second > 0;
    }
}
