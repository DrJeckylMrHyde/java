package com.company.Zadania76;

public class Zadanie3 {

    public static void main(String[] args) {
        System.out.println(1 + 1);

        System.out.println(1 + 1 + "1");

        System.out.println("1" + 1 + 1);        //String+liczba to napis czyli "a" + 3 = "a3"
        System.out.println("1" + (1 + 1));      //ctrl+d
        System.out.println("1" + 4 * 5);

        System.out.println(7 * 1);

        System.out.println(7 / 2.0);
    }
}
