package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie69 {
    public static void main(String[] args) {

        int a =20;
        a = ++a + a++;
        System.out.println(a);

        wyswietlTrojkat(pokazeTrojkatPascala(5));
    }

    public static int[][] pokazeTrojkatPascala(int wysokosc) {
        int[][] trojkat = new int[wysokosc][];
        for (int wiersz = 0; wiersz < wysokosc; wiersz++) {
            trojkat[wiersz] = new int[wiersz + 1];                                              // dla kazdego wiersza tablicy tworzymy tablice o okreslonej ilosci elementow
            int number = 1;
            for (int kolumna = 0; kolumna <= wiersz; kolumna++) {                               //trojkatPascala.length albo < wiersz + 1
                trojkat[wiersz][kolumna] = number;
                number = number * (wiersz - kolumna) / (kolumna + 1);
            }
        }
        return trojkat;
    }

    public static void wyswietlTrojkat(int[][] trojkatPascala) {
        for (int[] wiersz : trojkatPascala) {
            System.out.println(Arrays.toString(wiersz));
        }
    }
}
