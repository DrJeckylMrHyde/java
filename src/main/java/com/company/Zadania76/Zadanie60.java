package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie60 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(pokazeCzyElementJestDodatni(-1, -2, 0, 4, -1)));
    }

    public static boolean[] pokazeCzyElementJestDodatni(int... tablicaIntow) {
        boolean[] nowaTablica = new boolean[tablicaIntow.length];
        for (int i = 0; i < tablicaIntow.length; i++) {
            if (tablicaIntow[i] >= 0) {                         // tablica boolean przyjmuje wstepnie kazdy element false
                nowaTablica[i] = true;                          // dlatego sprawdzam tylko warunek dla dodatnich
            }
        }
        return nowaTablica;
    }
}
