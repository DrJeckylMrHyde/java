package com.company.Zadania76;

public class Zadanie26 {
    public static void main(String[] args) {
        sumElement(3);
        sumElement(5);
        sumElement(11);

        sumElementWhile(3);
        sumElementWhile(5);
        sumElementWhile(11);
    }

    static void sumElement(int parameter) {
        int sum = 0;
        for (int i = 1; i <= parameter; i++) {
                sum += i;
                System.out.print(i + " + ");
        }
        System.out.print("\b\b= " + sum);
        System.out.println();
    }

    static void sumElementWhile(int parameter) {
        int sum = 0;
        int i = 1;
        while (i <= parameter) {
            sum += i;
            System.out.print(i+" + ");
            i++;
        }
        System.out.println("\b\b= " + sum);
    }
}
