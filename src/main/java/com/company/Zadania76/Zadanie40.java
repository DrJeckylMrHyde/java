package com.company.Zadania76;

import java.util.Arrays;
import java.util.Random;

//*ZADANIE #40*
//        Utwórz metodę, która zwraca tablicę o podanym rozmierza wypełnioną losowymi liczbami (użyj klasy `Random()`).
//        Rozmiar tablicy ma być parametrem metody.

public class Zadanie40 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(pokazeTablice(5)));       //metoda klasy Array Array.toString wyswietli tablice w println
    }

    public static double[] pokazeTablice(int rozmiar) {
        double[] table = new double[rozmiar];
        Random losuj = new Random();
        for (int i = 0; i < table.length; i++) {
            table[i] = losuj.nextDouble()-10;
        }

        return table;
    }


}
