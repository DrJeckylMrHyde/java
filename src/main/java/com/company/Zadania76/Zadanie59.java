package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie59 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(podaUjemneOdLewej(1, 0, -1,-1,-3,4,5,6,-11,12,15)));
    }

    public static int[] podaUjemneOdLewej(int... tablicaIntow) {
        int[] nowaTablica = new int[tablicaIntow.length];
        int liczbaujemnych = 0;
        int liczbadodatnich = 0;
        for (int element : tablicaIntow) {
            if (element >= 0) {
                liczbadodatnich++;
            } else {
                liczbaujemnych++;
            }
        }
//        System.out.println(liczbadodatnich);
//        System.out.println(liczbaujemnych);
        int indeks = 0;
        for (int i = 0; i < tablicaIntow.length; i++) {
            if (tablicaIntow[i] >= 0) {
                nowaTablica[liczbaujemnych] = tablicaIntow[i];
                liczbaujemnych++;
            }
            else {
                nowaTablica[indeks] = tablicaIntow[i];
                indeks++;
            }
        }
        return nowaTablica;
    }
}
