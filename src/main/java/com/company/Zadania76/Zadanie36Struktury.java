package com.company.Zadania76;

public class Zadanie36Struktury {
    public static void main(String[] args) {
        int[] tab = new int[]{177, 2, 3, 5};

        //System.out.println(zwracaSumeElementowTablicy(tab));
        System.out.println(returnSumOfElement2(tab));
        System.out.println(podajSrednia(tab));
    }

    static int zwracaSumeElementowTablicy(int[] tab) {
        int sum = 0;
        for (int i = 0; i < tab.length; i++) {
            sum += tab[i];
        }
        return sum;
    }

    static int returnSumOfElement(int[] tab) {
        int sum = 0;
        for (int liczba : tab) {
            sum += liczba;
        }
        return sum;
    }

    static int returnSumOfElement2(int[] tab) {
        int sum = 0;
        for (int i : tab) {                            //skrót ITER do petli for-each, petal sama szuka domyslnej kolekcji
            sum += i;
        }
        return sum;
    }
    static double podajSrednia (int [] tab){
        return (double) returnSumOfElement2(tab)/ tab.length;
    }
}
