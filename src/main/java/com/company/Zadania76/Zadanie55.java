package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie55 {
    public static void main(String[] args) {
        int[] table1 = new int[]{5, 1, 10, 1, 89, -9};
        int[] table2 = new int[]{-2, 2, 3, 4, 5, 6};
        przeplataTabliceZDwoch(table1, table2);
    }

    public static int[] przeplataTabliceZDwoch(int[] table1, int[] table2) {
        int[] tablica = new int[table1.length + table2.length];
        int[] dluzszaTablica = table1.length > table2.length ? table1 : table2;             //zdefiniowano tablice jezeli spelnia warunek to dluzsza to table1
        int[] krotszaTablica = table1.length < table2.length ? table1 : table2;
//        int index = krotszaTablica.length;
//        System.out.println(index);
        System.out.println(tablica.length);
        for (int i = 0; i < krotszaTablica.length; i++) {                                   //wykonujemy petle do mniejszej tablicy bo mnozymy iterator razy 2
            tablica[i * 2] = table1[i];                                                     //przez to wypelniamy 2 razy wiecej pozycji w tablicy
        }                                                                                   //np krotsza tablica ma 3 elementy a zajmujemy w nowej lacznej 6
        for (int i = 0; i < krotszaTablica.length; i++) {
            tablica[i * 2 + 1] = table2[i];
        }
        for (int i = krotszaTablica.length * 2; i < tablica.length; i++) {                  //w momencie kiedy mamy rowne tablice petla nie wykona sie ani razu
            tablica[i] = dluzszaTablica[i - krotszaTablica.length];
        }

        System.out.println(Arrays.toString(tablica));
        return tablica;
    }
}
