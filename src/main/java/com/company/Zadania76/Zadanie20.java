package com.company.Zadania76;

public class Zadanie20 {
    public static void main(String[] args) {

        petlaZParametrem(45);
        petlaZParametrem(3);
        petlaZParametrem(9);
        petlaZParametrem(18);
    }

    static void petlaZParametrem(int a) {
        for (int i = 0; i <= a; i++){               // skrót fori fo wywołania pętli for
            System.out.print(i+", ");
            if(i%a==0 && i!=0){
                System.out.println();
            }
        }

    }
}
