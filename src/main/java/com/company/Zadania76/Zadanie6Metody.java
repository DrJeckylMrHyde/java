package com.company.Zadania76;

public class Zadanie6Metody {           //Shift+f6

    //    int multiply(int liczba){
//        return liczba * 2;
//    }
//    void introduce(String name){
//        System.out.println("Hello"+ name);
//    }
    public static void main(String[] args) {

//        metoda(2, 5);
        operacjeMatematyczne(2, 3);
    }


    static void metoda(int first, int second) {                                     //shift+f6 hurtowa zmiana nazwy
        System.out.println(first + "+" + second + " = " + (first + second));
        System.out.println(first + "-" + second + " = " + (first - second));
        System.out.println(first + "*" + second + " = " + (first * second));
        System.out.println(first + "/" + second + " = " + (first / second));
    }

    static void operacjeMatematyczne(int pierwsza, int druga) {
        System.out.printf("%s + %s = %s \n", pierwsza, druga, pierwsza + druga);    // % to znak specjalny w tej metodzie
        System.out.printf("%s - %s = %s \n", pierwsza, druga, pierwsza - druga);    // jak chce miec znak procenta to wstawiam %%
        System.out.printf("%s * %s = %s \n", pierwsza, druga, pierwsza * druga);    // metoda printf zawsze zwraca napis
        System.out.printf("%s / %s = %s \n", pierwsza, druga, pierwsza / druga);

    }
}
