package com.company.Zadania76;

import java.util.Arrays;


//*ZADANIE #41*
//Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną kolejnymi wartościami zaczynając od `10`:
//        > Dla `4` zwróci: `[10, 11, 12, 13]`
//        >
//        > Dla `8` zwróci: `[10, 11, 12, 13, 14, 15, 16, 17]`
public class Zadanie41 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(zwrociTabliceOZadanymWymiarze(8)));
    }

    public static int[] zwrociTabliceOZadanymWymiarze(int rozmiar) {
        int[] table = new int[rozmiar];
        //int poczatek = 10;
        //table[0] = 10;
        for (int i = 0; i < rozmiar; i++) {
            //for (int i = 1; i < rozmiar; i++) {
            table[i] = 10 + i;
            table[i] = table[0] + i;
        }
        return table;
    }
}
