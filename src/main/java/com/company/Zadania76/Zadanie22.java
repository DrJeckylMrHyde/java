package com.company.Zadania76;

public class Zadanie22 {
    public static void main(String[] args) {
        przedzial(10, 20);
        przedzial(110, 90);
        przedzialWhile(10, 20);
        przedzialDoWhile(10, 20);
    }

    static void przedzial(int begin, int end) {
        if (begin <= end) {
            for (int i = begin; i <= end; i++) {
                System.out.print(i + ", ");
            }
            System.out.println();
        } else {
            for (int i = begin; i >= end; i--) {
                System.out.print(i + ", ");
            }
            System.out.println();
        }
    }

    static void przedzialWhile(int begin, int end) {
        int i = 10;
        while (i <= 20) {
            System.out.print(i + " ");
            i++;
        }
        System.out.println();
    }

    static void przedzialDoWhile(int begin, int end) {
        int i = 10;
        do {
            System.out.print(i + " ");
            i++;
        } while (i <= 20);
        System.out.println();
    }

}
