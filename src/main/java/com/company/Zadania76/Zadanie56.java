package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie56 {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 2, 3, 4, 5, 9};
        System.out.println(pokazeOdKtoregoElementuPrzekroczyLiczbe(tablica, 9));
    }

    public static int pokazeOdKtoregoElementuPrzekroczyLiczbe(int[] table, int number) {
        int sum = 0;
            for (int i = table.length - 1; i >= 0; i--) {
                sum += table[i];
                if (sum >= number) {
                    return table.length - 1 - i;
                }

            }

        return -1;
    }
}
