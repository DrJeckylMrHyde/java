package com.company.Zadania76;

public class Zadanie23 {
    public static void main(String[] args) {
        parzysteZPrzedzialu(0,14);
        parzysteZPrzedzialu(0,4);
        parzysteZPrzedzialu(6,14);
    }

    static void parzysteZPrzedzialu(int begin, int end) {
        for (int i = begin; i < end; i++) {
            if (i % 2 == 0) {
                System.out.print(i+" ");
            }
        }
        System.out.println();
    }
}
