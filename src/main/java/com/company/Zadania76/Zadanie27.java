package com.company.Zadania76;

public class Zadanie27 {
    public static void main(String[] args) {
        multiplyColumn(15);
    }

    static void multiplyColumn(int mnoznik) {
        for (int i = 1; i <= 10; i++) {
            System.out.printf("%s * %s  = %s\n", mnoznik, i, i * mnoznik);
        }
    }
}
