package com.company.Zadania76;

public class Zadanie54 {
    public static void main(String[] args) {
        int[] tablica = new int[]{1,2,2};
        System.out.println(zwracaSumeElementowBezMax(tablica));
    }

    public static double zwracaSumeElementowBezMax(int[] table) {
        double sum = 0;
        int iloscElementow = 0;
        for (int i = 0; i < table.length; i++) {
            if (table[i] != zwrocMax(table)) {
                iloscElementow++;
                sum += table[i];
            }
        }
        if (iloscElementow == 0) {
            System.out.println("Wszystkie elementy są równe, zatem każdy jest MAX!");
            System.exit(0);
        }
        //System.out.println(sum);
        return sum / iloscElementow;
    }


    public static int zwrocMax(int[] table) {
        int max = table[0];
        for (int i = 0; i < table.length; i++) {
            if (table[i] > max) {
                max = table[i];
            }
        }
        //System.out.println(max);
        return max;
    }
}
