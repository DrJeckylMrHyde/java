package com.company.Zadania76;

import java.util.Arrays;

//Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby.
//        Metoda ma zwrócić nową tablicę do której na wybranej pozycji (podanej jako drugi parametr) wstawi nowy element (podany jako trzeci parametr).
//        > Dla `([1, 2, 3, 4, 5], 2, 77)`
//        > powinno zwrócić `[1, 2, 77, 3, 4, 5]`
public class Zadanie44 {
    public static void main(String[] args) {
        int[] table = new int[]{1, 2, 3, 4, 5};

        System.out.println(Arrays.toString(zamianaElementyTablicy(table, 2, 77)));
    }

    public static int[] zamianaElementyTablicy(int[] table, int pozycja, int liczba) {
        int[] tableChange = new int[table.length + 1];
        if (pozycja < table.length - 1) {
            for (int i = 0; i < pozycja; i++) {
                tableChange[i] = table[i];
            }
            tableChange[pozycja] = liczba;
            for (int i = pozycja + 1; i < tableChange.length; i++) {
                tableChange[i] = table[i - 1];
            }
            return tableChange;
        }
        System.out.println("Próbujesz wybrać liczbę spoza tablicy, zwracam wyjściową tablicę");
        return table;
    }

}
