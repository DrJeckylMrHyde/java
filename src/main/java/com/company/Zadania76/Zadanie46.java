package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie46 {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 2, 3, 4, 5};
        usunElementZTablicy(tablica, 4);
        //wyswietlTablice(tablica);
        wyswietlTablice(usunElementZTablicy(tablica, 2));
    }

    static int[] usunElementZTablicy(int[] table, int index) {
        int[] tab = new int[table.length - 1];
        for (int i = 0; i < index; i++) {
            tab[i] = table[i];
        }
        for (int i = index; i < tab.length; i++) {
            tab[i] = table[i + 1];
        }
        //System.out.println(Arrays.toString(tab));
        return tab;
    }

    static void wyswietlTablice(int[] table) {
        for (int i = 0; i < table.length; i++) {
            System.out.print(table[i] + " ");
        }
        System.out.println("\b\b");
    }
}
