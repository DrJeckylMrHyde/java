package com.company.Zadania76;

public class Zadanie29 {
    public static void main(String[] args) {

        pokazeDzielniki(0, 10);
    }

    static void pokazeDzielniki(int begin, int end) {
        for (int currentNumber = begin; currentNumber <= end; currentNumber++) {
            System.out.printf("%s <-- ", currentNumber);
            for (int dzielnik = 1; dzielnik <= currentNumber; dzielnik++) {

                if (currentNumber % dzielnik == 0) {
                    System.out.printf(" %s, ", dzielnik);               //shift + alt kursor wielokrotny

                }

            }

            System.out.println("\b\b");
        }

    }
}
