package com.company.Zadania76;

import java.util.Scanner;
import java.util.Arrays;

//*ZADANIE #45*
//        Utwórz metodę, w której użytkownik podaje z klawiatury rozmiar tablicy, a następnie podaje wszystkie elementy (jako `int`):
//        >
//        ```Podaj rozmiar tablicy:  4
//Liczba[1]:  88
//Liczba[2]:  12
//Liczba[3]:  -7
//Liczba[4]:  195
//Podana tablica to: [88, 12, -7, 195]
//Suma elementów wynosi: 288```
public class Zadanie45 {
    public static void main(String[] args) {
        sumaElementowTablicy();
    }

    public static void sumaElementowTablicy() {
        int suma = 0;
        Scanner liczbyTablica = new Scanner(System.in);
        System.out.print("Podaj rozmiar tablicy: ");
        int rozmiar = liczbyTablica.nextInt();
        int[] table = new int[rozmiar];
        for (int i = 0; i < table.length; i++) {
            System.out.printf("Podaj %s element: ", i);
            table[i] = liczbyTablica.nextInt();
            suma += table[i];
        }
        System.out.println("Podana tablica to: " + Arrays.toString(table));

        System.out.println("Suma elementów tablicy wynosi: " + suma);
    }
}
