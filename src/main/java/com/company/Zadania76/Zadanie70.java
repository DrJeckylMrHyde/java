package com.company.Zadania76;

public class Zadanie70 {
    public static void main(String[] args) {
        pokazTablice2D(wyplenijspiralnieTablice2D(3, 4));
    }

    public static int[][] wyplenijspiralnieTablice2D(int wiersz, int kolumna) {
        int[][] tablicaSpiralna = new int[wiersz][kolumna];
        int liniaprosta = 0;
        int wartosc = 1;
        int odejmowanieWierszy = 1;
        int odejmowanieKolumn = 1;
        //petla zewn od gory od lewej do prawej
        for (int i = 0; i < kolumna; i++) {
            tablicaSpiralna[liniaprosta][i] = wartosc;
            wartosc++;
        }
        liniaprosta = kolumna - odejmowanieKolumn;
        //petla zewn od prawej z gory do dolu
        for (int i = 1; i < wiersz; i++) {
            tablicaSpiralna[i][liniaprosta] = wartosc;
            wartosc++;
        }
        odejmowanieKolumn++;
        liniaprosta = wiersz - odejmowanieWierszy;
        //petla zewn z dolu od prawej do lewej
        for (int i = kolumna - odejmowanieKolumn; i >= 0; i--) {
            tablicaSpiralna[liniaprosta][i] = wartosc;
            wartosc++;
        }
        odejmowanieWierszy++;

        liniaprosta = 0;
        //petla zewn z lewej od dolu do gory
        for (int i = wiersz-odejmowanieWierszy; i >= 1; i--) {
            tablicaSpiralna[i][liniaprosta] = wartosc;
            wartosc++;
        }
        //odejmowanieWierszy++;
        liniaprosta = wiersz - odejmowanieWierszy;
        //
        for (int i = 1; i < kolumna - 1; i++) {
            tablicaSpiralna[liniaprosta][i] = wartosc;
           wartosc++;
       }
        return tablicaSpiralna;
    }

    public static void pokazTablice2D(int[][] tablica2D) {
        for (int[] wiersz : tablica2D) {
            for (int kolumna : wiersz) {
                System.out.print(kolumna + "\t");
            }
            System.out.println();
        }
    }
}
