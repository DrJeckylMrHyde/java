package com.company.Zadania76;

public class Zadanie25 {
    public static void main(String[] args) {
        skokElemntow(0, 6, 2.5);
        skokElementowWhile(0,5,5);
    }

    static void skokElemntow(int start, int amount, double jump) {
        for (int i = start; i < amount; i++) {
            System.out.println(i * jump + " ");
        }
        System.out.println();
    }

    static void skokElementowWhile(int start, int amount, double jump) {
        int i = start;
        while (i < amount) {
            System.out.println(i * jump + " ");
            i++;
        }
    }
}
