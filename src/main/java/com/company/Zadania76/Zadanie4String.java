package com.company.Zadania76;

public class Zadanie4String {

    public static void main(String[] args) {

        String name = "Marcin";         // operator "=" to przypisanie
        String surname = "Wegner";
        String myCity = "Łódź";
        String dane = name+" "+surname;         // camelCase


//        int number;                     // deklaracja
//        number = 5;                     // przypisanie

        System.out.println(name);
        System.out.println(surname);
        System.out.println(dane);
    }
}
