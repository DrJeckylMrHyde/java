package com.company.Zadania76;

import static java.lang.Math.*;

public class Zadanie7MetodyCD {

    public static void main(String[] args) {

//        System.out.println(zeZwracaniem(2, 11));            // takie rozwiązanie stosuje w przypadku gdy potrzebuje tego wyniku jednorazowo
//        int wynik = zeZwracaniem(-10, 3);                   // to z kolei kiedy zmienna "wynik" bedzie mi potrzebna wielokrotnie
//        System.out.println(wynik);

        int potega = potegi(2);
        System.out.println("wynik " + potega);
        System.out.println("wynik " + potegi(3));


    }

    private static int zeZwracaniem(int first, int second) {
        return first + second;
    }

    private static int potegi(int podstawa) {
        System.out.print(podstawa+" -> ");
        return (int) pow(podstawa, 3);
    }
}
