package com.company.Zadania76;

public class Zadanie32 {
    public static void main(String[] args) {
        pokazeNowaChoinke(4);
    }

    static void pokazeNowaChoinke(int lineNumber) {
        int currentValue = 1;
        for (int i = 1; i <= lineNumber; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(currentValue++ + " ");

            }
            System.out.println();
        }
    }
}
