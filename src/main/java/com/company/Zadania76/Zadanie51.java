package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie51 {
    public static void main(String[] args) {

        int[] table1 = new int[]{1, 4, 3, 6};
        int[] table2 = new int[]{1, 8, 8, 7, 3 ,4};
        sumujeKolumnyDwochTablic2(table1, table2);

    }

//    public static int[] sumujeKolumnyDwochTablic(int[] table1, int[] table2) {
//        int[] newTable = new int[4];
//        for (int i = 0; i < newTable.length; i++) {
//            newTable[i] = table1[i] + table2[i];
//        }
//
//        System.out.println(Arrays.toString(table1));
//        System.out.println(Arrays.toString(table2));
//        System.out.println(Arrays.toString(newTable));
//        return newTable;
//    }

    public static int[] sumujeKolumnyDwochTablic2(int[] table1, int[] table2) {
        int[] dluzszaTablica = table1.length > table2.length ? table1 : table2;             //zdefiniowano tablice jezeli spelnia warunek to dluzsza to table1
        int[] krotszaTablica = table1.length < table2.length ? table1 : table2;
        int roznicaDlugosci = dluzszaTablica.length-krotszaTablica.length;
        int[] wyrownajTablica = new int [dluzszaTablica.length];
        int indeks = 0;
        for (int i = roznicaDlugosci; i < wyrownajTablica.length; i++) {
            wyrownajTablica[i] = krotszaTablica[indeks];
            indeks++;
        }
        int iloscDyszek = 0;
        int[] newTable = new int[dluzszaTablica.length];
        System.out.println("   " + Arrays.toString(dluzszaTablica));
        System.out.println("+  " + Arrays.toString(wyrownajTablica));
        System.out.println("---------------");

        for (int i = newTable.length - 1; i >= 0; i--) {
            newTable[i] = dluzszaTablica[i] + wyrownajTablica[i] + iloscDyszek;
            System.out.println(newTable[i]);
            if (newTable[i] >= 10 && i > 0) {
                iloscDyszek = (int) Math.floor(newTable[i] / 10);
                newTable[i] = newTable[i] % 10;
            } else if (newTable[i] < 10 && i > 0) {
                iloscDyszek = 0;
            } else if (newTable[i] >= 10 && i == 0) {
                int[] newTable2 = new int[newTable.length + 1];
                iloscDyszek = (int) Math.floor(newTable[i] / 10);
                newTable2[0] = iloscDyszek;
                newTable2[1] = newTable[i] % 10;
                for (int j = 2; j < newTable2.length; j++) {
                    newTable2[j] = newTable[j - 1];
                }
                System.out.println(Arrays.toString(newTable2));
            } else if (newTable[i] < 10 && i == 0) {
                System.out.println("   "+Arrays.toString(newTable));
            }
        }
        return newTable;
    }
}
