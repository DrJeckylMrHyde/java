package com.company.Zadania76;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Zadanie52HOMEWORK {
    public static void main(String[] args) {
        int[] table1 = new int[]{4, 5};
        int[] table2 = new int[]{3, 4, 5};
        int[] table3 = new int[]{5};
        int[] table4 = new int[]{3, 4, 5};
        int[] wynik = sumujTablice(table1, table2, table3, table4);
        //System.out.println(Arrays.toString(wynik));
        System.out.println(Arrays.toString(usunPustePozycje(wynik)));
    }

    static int[] usunPustePozycje(int[] tablica) {
        int licznikZer = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == 0) {
                licznikZer++;
            } else {
                break;
            }
        }
        int[] skroconaTablica = new int[tablica.length - licznikZer];
        for (int j = 0; j < skroconaTablica.length; j++) {
            skroconaTablica[j] = tablica[j + licznikZer];
        }
        return skroconaTablica;
    }

    static int[] sumujTablice(int[] table1, int[] table2, int[] table3, int[] table4) {

        int[] tablicaIndeksow = new int[]{table1.length - 1, table2.length - 1, table3.length - 1, table4.length - 1};
        int[] tablicaKoncowa = new int[10];

        for (int indeks = tablicaKoncowa.length - 1; indeks > 0; indeks--) {
            int suma = tablicaKoncowa[indeks];
            if (tablicaIndeksow[0] >= 0) {
                suma += table1[tablicaIndeksow[0]];
                tablicaIndeksow[0]--;
            }
            if (tablicaIndeksow[1] >= 0) {
                suma += table2[tablicaIndeksow[1]];
                tablicaIndeksow[1]--;
            }
            if (tablicaIndeksow[2] >= 0) {
                suma += table3[tablicaIndeksow[2]];
                tablicaIndeksow[2]--;
            }
            if (tablicaIndeksow[3] >= 0) {
                suma += table4[tablicaIndeksow[3]];
                tablicaIndeksow[3]--;
            }
            if (suma > 9) {
                tablicaKoncowa[indeks] = suma % 10;
                tablicaKoncowa[indeks - 1] += suma / 10;
            } else {
                tablicaKoncowa[indeks] = suma;
            }
        }

        return tablicaKoncowa;
    }
}

