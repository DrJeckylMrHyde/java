package com.company.Zadania76;

public class Zadanie16 {
    public static void main(String[] args) {

        boolean rezultat2 = czyWPrzedziale(1, 10, 5);
        boolean rezultat3 = czyWPrzedziale(1, 10, 15);
        boolean rezultat4 = czyWPrzedziale(-10, 0, 3);
        boolean rezultat5 = czyWPrzedziale(-10, 10, 0);
        boolean rezultat6 = czyWPrzedziale(4, 8, 4);
        boolean rezultat7 = czyWPrzedziale(3, 3, 3);

        System.out.println(rezultat2);
        System.out.println(rezultat3);
        System.out.println(rezultat4);
        System.out.println(rezultat5);
        System.out.println(rezultat6);
        System.out.println(rezultat7);
    }

    static boolean czyWPrzedziale(int begin, int end, int number) {
        return number >= begin && number <= end;
    }
}
