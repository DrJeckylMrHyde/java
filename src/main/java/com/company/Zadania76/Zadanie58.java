package com.company.Zadania76;

public class Zadanie58 {
    public static void main(String[] args) {
        System.out.println(sklejaNapis("Ala", "ma", "kota", "o", "szarej", "sierści", "z", "czarną","plamką"));
    }

    public static String sklejaNapis(String... tablicaStringow){
        String tekst = "";
        for (String s : tablicaStringow) {
            tekst += s+" ";
        }
        return tekst+"\b";
    }
}
