package com.company.Zadania76;

public class Zadanie12 {
    public static void main(String[] args) {

        comparisonProgram(51);
        comparisonProgram(29);
        comparisonProgram(50);
    }

    static void comparisonProgram(int a) {
        int limes = 50;                                                                             // wprowadzamy zmienna odpowiadajaca za liczbe do porownania
        if (a > limes) System.out.printf("Liczba %s jest większa od " + limes + "\n",a);            // zeby nie zmieniac jej kazdorazowo w kodzie
        else if(a == limes) System.out.printf("Liczba %s jest równe " + limes + "\n", a);
        else System.out.printf("Liczba %s jest mniejsze od " + limes + "\n", a);

    }
}
