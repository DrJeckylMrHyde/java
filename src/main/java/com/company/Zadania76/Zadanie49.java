package com.company.Zadania76;

public class Zadanie49 {
    public static void main(String[] args) {

        zwrociSumeElementowZPodanegoPrzedzialuTablicy(new int[]{1, 2, 3, 4, 5}, 1, 3);
    }

    public static int zwrociSumeElementowZPodanegoPrzedzialuTablicy(int[] table, int begin, int end) {
        int suma = 0;
        for (int i = begin; i <= end; i++) {
            suma += table[i];
        }
        System.out.printf("Suma elementów z przedziału od %s do %s wynosi %s", begin, end, suma);
        return suma;
    }
}
