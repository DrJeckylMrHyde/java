package com.company.Zadania76;

public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println(prezentacja("Marek", 32, true));
        System.out.println(prezentacja("Marta", 23, false));
    }

    static String prezentacja(String imie, int wiek, boolean plec) {
        String formulka = "Cześć! Jestem " + imie + ", mam " + wiek + " lat i jestem ";
        if (plec) {
            formulka += "mężczyzną";
        } else {
            formulka += "kobietą";
        }
        return formulka;
    }
}
