package com.company.Zadania76;

public class Zadanie33 {
    private static String znak = "x";
    public static void main(String[] args) {
//        pokazeRamkeIksow(10);
        pokazeRamkeIksow2(10);
    }

    static void pokazeRamkeIksow(int rozmiar) {

        pokazeWierszIksow(10);
        for (int i = 1; i <= rozmiar; i++) {

            if (i > 1 && i < rozmiar) {
                System.out.print(" ");
                continue;
            }
            System.out.print(znak);
        }
        System.out.println();
        pokazeWierszIksow(10);
    }

    static void pokazeRamkeIksow2(int rozmiar) {

        pokazeWierszIksow(10);
        System.out.print(znak);
        for (int i = 0; i < rozmiar - 2; i++) {
            System.out.print(" ");

            }
        System.out.print(znak);
        System.out.println();

        pokazeWierszIksow(10);
    }

    static void pokazeWierszIksow(int rozmiar) {

        for (int i = 1; i <= rozmiar; i++) {
            System.out.print(znak);
        }
        System.out.println();
    }
}

