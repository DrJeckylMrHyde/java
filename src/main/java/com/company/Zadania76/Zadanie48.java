package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie48 {
    public static void main(String[] args) {
        int[] tabliczka = new int[]{1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(zwracaTabliceZPodanegoPrzedzialu(tabliczka, 1, 3)));
    }

    public static int[] zwracaTabliceZPodanegoPrzedzialu(int[] table, int begin, int end) {
        int[] tablica = new int[Math.abs(end - begin+1)];
        if (begin < end && begin >= 0 && end >= 0) {
            for (int i = 0; i < tablica.length; i++) {
                tablica[i] = table[begin + i];
            }
        } else if (begin > end && begin >= 0 && end >= 0) {
            for (int i = 0; i < tablica.length; i++) {
                tablica[i] = table[end + i];
            }
        }
        return tablica;
    }
}
