package com.company.Zadania76;

//Utwórz metodę, która przyjmuje dwa parametry - tablicę (wartości `short`) oraz liczbę.
// Metoda ma zwrócić informację (jako wartość logiczna) czy dana liczba znajduje się w tablicy.
public class Zadanie39 {
    static boolean rezultat;

    public static void main(String[] args) {
        short[] matrix = new short[]{1, 2, 3, 4};
        rezultat = powieCzyLiczbaJestWTablicy(matrix, (short) 5);
        System.out.println(rezultat ? "Jest w przedziale" : "Nie ma jej w przedziale");         // zeby urozmaicic odbior dla uzytkowniak nadajemy komunikat
    }

    public static boolean powieCzyLiczbaJestWTablicy(short[] matrix, short number) {
        for (short liczbaZTablicy : matrix) {
            if (number == liczbaZTablicy) {
                return true;
            }
        }
        return false;
    }
}
