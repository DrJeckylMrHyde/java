package com.company.Zadania76;

//Utwórz metodę, która przyjmuje tablicę liczb i zwraca rozmiar tablicy po usunięciu duplikatów
//        > Dla `[-7, 8, 8, 0, 2, 7, 2]` (ma 7 elementów)
//        > zwróci `5` (bo  tablica bez duplikatów to `[-7, 8, 0, 2, 7]`)
public class Zadanie64 {
    public static void main(String[] args) {

        int[] tablica = new int[]{-7, 8, 8, 0, 2, 7, 2, 8};
    }

    private static int numberOfUniqueElements(int[] array) {
        int counter = 0;

        external:
        for (int i = 0; i < array.length; i++) {
            System.out.println("licznik " + counter);
            System.out.println("i = " + i);
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    System.out.println("element na którym stanał bo się powtarza " + j);
                    continue external;
                }
            }
            counter++;
        }

        return counter;
    }
}