package com.company.Zadania76;

import java.util.Random;

public class Zadanie66Saper {
    public static void main(String[] args) {
        boolean[][] plansza = wylosujPlansze(4);
        //pokazTablice(plansza);
        //pokazTabliceZSymbolemBomby(plansza);
        int[][] doWyswietlenia = wypelniTabliceIlosciaBomb(plansza);
        pokazTablice(doWyswietlenia);
    }

    static boolean[][] wylosujPlansze(int wymiar) {
        boolean[][] plansza = new boolean[wymiar][wymiar];
        Random size = new Random();
        for (int i = 0; i < wymiar * wymiar * 0.2; i++) {
            //plansze[size.nextInt(wymiar)][size.nextInt(wymiar)] = ;
            int wiersz = size.nextInt(wymiar);
            int kolumna = size.nextInt(wymiar);
            if (plansza[wiersz][kolumna] == true) {
                i--;
//                System.out.println("Powtórzyło się "+wiersz+", "+kolumna);
            } else {
                plansza[wiersz][kolumna] = true;
//                System.out.println("Wylosowano pole "+wiersz+", "+kolumna);
            }
        }
        return plansza;
    }

    static void pokazTablice(boolean[][] plansza) {
        for (boolean[] wiersz : plansza) {
            for (boolean poleNaPlanszy : wiersz) {
                System.out.print(poleNaPlanszy + "\t");
            }
            System.out.println();
        }
    }

    static void pokazTabliceZSymbolemBomby(boolean[][] plansza) {
        for (boolean[] wiersz : plansza) {
            for (boolean poleNaPlanszy : wiersz) {
                System.out.print(poleNaPlanszy ? "*" : "-");
                System.out.print("\t");
            }
            System.out.println();
        }
    }

    static int[][] wypelniTabliceIlosciaBomb(boolean[][] plansza) {
        //pokazTablice(plansza);
        int[][] zBombami = new int[plansza.length][plansza.length];
        for (int wiersz = 0; wiersz < plansza.length; wiersz++) {
            for (int kolumna = 0; kolumna < plansza[wiersz].length; kolumna++) {
                int iloscSasiednichBomb = 0;
                if (plansza[wiersz][kolumna]) {
                    zBombami[wiersz][kolumna] = -1;
                } else {
                    zBombami[wiersz][kolumna] = zwrocLiczbaBomb(plansza, wiersz, kolumna);
                }
            }
        }
        return zBombami;
    }

    static int zwrocLiczbaBomb(boolean[][] plansza, int wiersz, int kolumna) {
        int liczbaBomb = 0;
        for (int i = wiersz - 1; i <= wiersz + 1; i++) {
            for (int j = kolumna - 1; j <= kolumna + 1; j++) {
                if (i >= 0 && j >= 0 && j < plansza.length && i < plansza.length) {
                    if (plansza[i][j]) {
                        liczbaBomb++;
                    }
                }
            }
        }
        return liczbaBomb;
    }

    static void pokazTablice(int[][] plansza) {
        for (int[] wiersz : plansza) {
            for (int poleNaPlanszy : wiersz) {
                if (poleNaPlanszy == -1) {
                    System.out.print("*\t");
                } else {
                    System.out.print(poleNaPlanszy + "\t");
                }
            }
            System.out.println();
        }
    }
}
