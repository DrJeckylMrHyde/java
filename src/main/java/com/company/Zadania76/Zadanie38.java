package com.company.Zadania76;

public class Zadanie38 {
    public static void main(String[] args) {
        double[] tablica = new double[]{12.0, 22.0, 17.0};

        System.out.println(zwrocMax2(tablica));
    }

    public static double zwrocMax(double[] tab) {
        double max = tab[0];
        for (double elementTablicy : tab) {         // elementTablicy to pojedynczy element
            if (elementTablicy > max) {               // jezeli el.tablicy jest wiekszy od max to instr ponizej
                max = elementTablicy;               // zmiana wartosci zmiennej max
            }

        }
        return max;
    }

    public static double zwrocMax2(double[] tab) {
        double max = tab[0];
        for (int i = 1; i < tab.length; i++) {
            if (tab[i] > max) {
                max = tab[i];
            }
        }
        return max;
    }
}