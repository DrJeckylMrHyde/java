package com.company.Zadania76;

import java.util.Arrays;

//twórz metodę, która jako parametr przyjmuje tablicę i zwraca drugą największą liczbę w tablicy.
//dla `[1, 2, 3, 4, 5]` zwróci `4`
public class Zadanie43 {
    public static void main(String[] args) {
        int[] table = new int[]{5, 2, 5, 5, 5};
        System.out.println(zwracaDrugieMax2(table));
    }

    public static int zwracaDrugieMax(int[] table) {

        Arrays.sort(table);
        int secondMax = table[table.length - 2];
        return secondMax;
    }

    public static int zwracaDrugieMax2(int[] table) {
        int max = Integer.MIN_VALUE;                                // MIN_VALUE najmniejsza mozliwa wartosc dla danej zmiennej
        int secondMax = Integer.MIN_VALUE;
        for (int i = 0; i < table.length; i++) {

            if (table[i] > max) {
                secondMax = max;                                    // jezeli wartosc max zostala zmieniona przez kolejny element tablicy
                max = table[i];                                     // to stary max od razu przypisuje jako drugi max
            } else if (table[i] > secondMax && table[i] != max) {
                secondMax = table[i];
            }
        }

        return secondMax;
    }


}
