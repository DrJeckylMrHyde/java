package com.company.Zadania76;

//Utwórz metodę, która przyjmuje 3 parametry. Pierwsze dwa to krańce przedziału. Trzeci to “skok” pomiędzy liczbami. Gdy przekazany przedział będzie błędny, metoda ma wyświetlić komunikat.
//        > Dla `1, 10, 3` wyświetli `1, 4, 7, 10`

public class Zadanie34 {
    public static void main(String[] args) {
        displayJump(10, 20, 12);
        displayJump(10, 20, 3);
        displayJump(1, 10, 3);
    }

    public static void displayJump(int begin, int end, int jump) {

        if (begin <= end) {
            if (jump > end - begin) {
                System.out.println("Skok większy od rozmiaru przedziału");

            } else {
                for (int i = begin; i <= end; i += jump) {
                    System.out.printf("%s ", i);
                }
                System.out.println();
            }
        } else {
            for (int i = begin; i >= end; i -= jump) {
                System.out.printf("%s ", i);
            }
            System.out.println();
        }

    }
}
