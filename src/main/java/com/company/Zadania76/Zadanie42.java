package com.company.Zadania76;

//*ZADANIE #42*
//        Utwórz metodę, która jako parametr przyjmuje tablicę i *zwraca nową tablicę* z liczbami w odwrotnej kolejności.

import java.util.Arrays;

public class Zadanie42 {
    public static void main(String[] args) {
        int[] table = new int[]{1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(odwrociTablice(table)));
    }

    public static int[] odwrociTablice(int[] table) {
        int[] revTable = new int[table.length];
        int n = table.length-1;
        for (int i = 0; i < table.length; i++) {
            revTable[i] = table[n];
            n--;
        }
        return revTable;
    }
}
