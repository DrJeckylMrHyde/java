package com.company.Zadania76;

public class Zadanie17 {
    public static void main(String[] args) {
    czyWartLogiczna();
    }

    static void czyWartLogiczna (){
        String message = "Potwierdzam";
        System.out.println(message == "Potwierdzam" ? "tak" : "nie");

        String komunikat = message == "Potwierdzam" ? "tak" : "nie";
        System.out.println(komunikat);
    }
}
