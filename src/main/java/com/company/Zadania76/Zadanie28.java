package com.company.Zadania76;

import static java.lang.Math.*;

public class Zadanie28 {
    public static void main(String[] args) {
        potegiDoPrzedzialu(3, 100);
    }

    static void potegiDoPrzedzialu(int podstawa, int przedzial) {

        for (int i = 0; ; i++) {
            double wynik = pow(podstawa, i);
            if (wynik > przedzial) {                        // ifa z breakiem uzywamy gdy nie wiemy kiedy petla ma skonczyc prace
                break;
            }
            System.out.printf("%s -> %s\n", i, wynik);          //ctrl shift strzalka gora dol przesuwanie linijek

        }
    }
}

