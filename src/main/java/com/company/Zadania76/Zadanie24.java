package com.company.Zadania76;

public class Zadanie24 {
    public static void main(String[] args) {
        wielokrotnosci(8);
        wielokrotnosci(4);
        wielokrotnosci(7);
        wielokrotnosciWhile(4);
        wielokrotnosciWhile(7);
    }

    static void wielokrotnosci(int wielokrotnosc) {
        for (int i = 1; i <= wielokrotnosc; i++) {
            System.out.print(i * 10 + " ");
        }
        System.out.println();
    }

    static void wielokrotnosciWhile(int wielokrotnosc) {
        int i = 1;
        while (i <= wielokrotnosc) {
            System.out.print(i*10+ " ");
            i++;
        }
        System.out.println();
    }
}
