package com.company.Zadania76;

//Utwórz metodę, która przyjmuje dwa parametry oraz *wyświetla* ciąg elementów. Pierwszy parametr metody określa wyświetlany element, a drugi parametr liczbę wystąpień,
//        > Dla `9, 5` wyświetli: `9 99 999 9999 99999`
//        >
//        > dla `3, 3` wyświetli: `3 33 333`
//        >
//        > dla `8 4` wyświetli: `8 88 888 8888`

public class Zadanie35 {
    public static void main(String[] args) {
        displayNumbers(8, 4);
        //displayNumbers(8, 4);
        //displayNumbers(8, 4);
    }

    static void displayNumbers(int number, int display) {

        for (int i = 0; i < display; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(number);
            }
            System.out.print(" ");
        }


    }
}