package com.company.Zadania76;

public class Zadanie14 {
    public static void main(String[] args) {
        int digit = 44;
        boolean rezultat = czyParzysta(digit);
        System.out.println(rezultat);
    }

    static boolean czyParzysta(int liczba) {
        return liczba % 2 == 0;
    }


}

