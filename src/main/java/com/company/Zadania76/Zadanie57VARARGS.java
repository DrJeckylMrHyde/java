package com.company.Zadania76;                                        //  to mi tworzy tablice o nieokreslonej liczbie elementow
// tylko jeden taki parametr moze byc w metodzie
// jesli jest musi wystepowac jako ostatni

public class Zadanie57VARARGS {
    public static void main(String[] args) {
        System.out.println(zwrocSume(1, 2, 3, 4, 5));
    }

    static int zwrocSume(int... liczbydoTablicy) {
        int suma = 0;
//        for (int i = 0; i < liczbydoTablicy.length; i++) {
//            suma += liczbydoTablicy[i];
//        }
        for (int skladnik : liczbydoTablicy) {
            suma += skladnik;
        }
        return suma;
    }
}
