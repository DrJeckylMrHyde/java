package com.company.Zadania76;

import java.util.Arrays;

public class Zadanie63 {
    public static void main(String[] args) {
        int [][] tablica2D = new int [][]{{1,2,3},{3,2,1},{5,6,5}};             //
        System.out.println(Arrays.toString(zwracaSumeKolumn(tablica2D)));
    }

    static int [] zwracaSumeKolumn(int [][] tablica2D){
        int [] sumaKolumn = new int [tablica2D.length];
        for (int wiersz = 0; wiersz < tablica2D.length; wiersz++) {
            for (int kolumna = 0; kolumna < tablica2D[wiersz].length; kolumna++) {      // tu moze byc tez tablica[0].length bo wszystkie wiersze rowne
                sumaKolumn[kolumna] += tablica2D[wiersz][kolumna];
            }
        }
        return sumaKolumn;
    }
}
