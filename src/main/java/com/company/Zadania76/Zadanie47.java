package com.company.Zadania76;

//*ZADANIE #47*
//        Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz wartość logiczną. 
//        Gdy drugi parametr ma wartość `true` metoda ma zwrócić *największa* liczbę z tablicy. 
//        Gdy parametr będzie miał wartość `false`, metoda ma zwrócić *najmniejszą* liczbę z tablicy.
public class Zadanie47 {
    public static void main(String[] args) {
        int [] tablica = new int [] {4,2,5,4,3};
        
        System.out.println(zwrociMaxLubMin(tablica, false));         // zadawac pytanie dla boolean
        System.out.println(zwrociMaxLubMin(tablica, true));         // zadawac pytanie dla boolean
    }

    public static int zwrociMaxLubMin(int[] table, boolean isMax) {

        int max = table[0];
        int min = table[0];
        for (int indeks = 1; indeks < table.length; indeks++) {
            if (table[indeks] > max) {
                max = table[indeks];
            }
            if (table[indeks] < min) {
                min = table[indeks];
            }
        }
        return isMax ? max : min;          // warunek ? jesli warunek spelniony : jesli nie spelniony
    }
}
