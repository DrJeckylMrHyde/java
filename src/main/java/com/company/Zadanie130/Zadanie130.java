package com.company.Zadanie130;

//Utwórz metodę, która przyjmuje parametry typu `int` korzystając z mechanizmu `varargs`.
// W przypadku nie przekazania żadnego parametru, metoda powinna rzucić własny wyjątek (z komunikatem o powodzie).
// W przypadku podania parametrów, metoda powinna zwrócić ich sumę.
public class Zadanie130 {
    public static void main(String[] args) {
        //jesli moj wyjatek nie dziedziczy po Runtime exception to musze to otrajkeczowac
        try {
            System.out.println(zwracaSume(1, 2, 3, 4, 5, 6, 7, 8, 9));
            System.out.println(zwracaSume());
        } catch (MojWlasnyWyjatek wyjatek) {
            //wyświetla nam komunikat z konstruktora wywołanego w metodzie
            System.out.println(wyjatek.getMessage());
        }
    }

    //mechanizm varargs
    private static int zwracaSume(int... tablicaLiczb) throws MojWlasnyWyjatek {
        int suma = 0;
        if (tablicaLiczb.length == 0) {
            throw new MojWlasnyWyjatek("Brak elementów tablicy");
        } else {
            for (int liczbaZTablicy : tablicaLiczb) {
                suma += liczbaZTablicy;
            }
        }
        return suma;
    }
}

//tworze wlasny wyjatek ktory musi dziedziczyc po Exception lub innej klasie dot wyjatków
class MojWlasnyWyjatek extends Exception {
    MojWlasnyWyjatek(String message) {

        super(message);
    }
}
