package com.company.FrogRiver;

// you can also use imports, for example:
// import java.util.*;

import java.util.HashSet;
import java.util.Set;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");
//For example, you are given integer X = 5 and array A such that:
//
//  A[0] = 1
//  A[1] = 3
//  A[2] = 1
//  A[3] = 4
//  A[4] = 2
//  A[5] = 3
//  A[6] = 5
//  A[7] = 4
//In second 6
class FrogRiver {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 3, 1, 4, 2, 3, 5, 4};
        System.out.println(solution(5, tab));
    }

    public static int solution(int X, int[] A) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < A.length; i++) {
            set.add(A[i]);
            if (set.size() == X) {
                return i;
            }
        }
        return -1;
    }
}