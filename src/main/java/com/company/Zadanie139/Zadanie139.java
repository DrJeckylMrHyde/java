package com.company.Zadanie139;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zadanie139 {
    private static String sciezkaDoPliku;

    public static void main(String[] args) {
        try {
            //ctrl alt v wywolanie zmiennej
            List<Pair<String, String>> listaDanych = odczytajPlik("plikiDoCwiczenOdZad132/Zadanie139.txt");
            wprowadzanieDanych(listaDanych);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static List<Pair<String, String>> odczytajPlik(String sciezkaDoPliku) throws IOException {
        //lista par, Pair<typ,typ>
        List<Pair<String, String>> lista = new ArrayList<>();
        FileReader plik = new FileReader(sciezkaDoPliku);
        BufferedReader bufor = new BufferedReader(plik);
        String linia = bufor.readLine();
        while (linia != null) {
            String[] dane = linia.split(" ");
            // musimy podac typ
            Pair<String, String> p = new Pair<>(dane[0], dane[1]);
            lista.add(p);
            linia = bufor.readLine();
        }
        return lista;
    }

    private static boolean wprowadzanieDanych(List<Pair<String, String>> lista) throws IOException {
        Scanner input = new Scanner(System.in);
        String login = "";
        String haslo = "";
        int proba = 3;
        while (!sprawdzeniePoprawnosciDanych(lista, login, haslo) && proba >= 0) {
            System.out.print("Podaj login: ");
            login = input.nextLine();
            System.out.print("Podaj haslo: ");
            haslo = input.nextLine();
            if(!sprawdzeniePoprawnosciDanych(lista,login,haslo)){
                System.out.printf("Hasło niepoprawne pozostały Ci %s próby.\n",proba);
            }
            proba--;
        }
        // ctrl alt m stworzenie domyslnej metody
        return sprawdzeniePoprawnosciDanych(lista, login, haslo);
    }

    private static boolean sprawdzeniePoprawnosciDanych(List<Pair<String, String>> lista, String login, String haslo) {
        for (Pair<String, String> pair : lista) {
            if (pair.getKey().equals(login) && pair.getValue().equals(haslo)) {
                return true;
            }
        }
        return false;
    }
}
