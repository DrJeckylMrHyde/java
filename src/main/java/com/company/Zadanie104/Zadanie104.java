package com.company.Zadanie104;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie104 {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>(Arrays.asList(
                1, 2, 3, 4, 5, 6, 7, 8, 9
        ));
        System.out.println(zwracaUcietaListe(lista, 3));
        System.out.println(zwracaUcietaListe2(lista, 3));
    }

    private static List<Integer> zwracaUcietaListe(List<Integer> lista, int indeksUciecia) {
        return lista.subList(indeksUciecia + 1, lista.size());
    }

    private static List<Integer> zwracaUcietaListe2(List<Integer> lista, int indeksUciecia) {
        List<Integer> ucietaLista = new ArrayList<>();
        for (int index = indeksUciecia+1; index < lista.size(); index++) {
            int liczba = lista.get(index);
            ucietaLista.add(liczba);
        }
        return ucietaLista;
    }




}
