package com.company.Zadanie101;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zadanie101 {
    // final uniemozliwia zmiane
    private static final Integer end = -1;

    public static void main(String[] args) {

        System.out.println("Moja lista " + zwrociListePodanychLiczb());
    }

     static List<Integer> zwrociListePodanychLiczb() {
        List<Integer> listaLiczb = new ArrayList<>();
        Scanner podanaLiczba = new Scanner(System.in);
        //while(true) tworzy petle nieskonczona
        while (true) {
            int liczba;
            System.out.println("Podaj liczbę do listy: ");
            liczba = podanaLiczba.nextInt();
            if (liczba == end) {
                break;
            }
            listaLiczb.add(liczba);
        }
        System.out.println("Srednia " + zwrociSredniaPodanychLiczb(listaLiczb));
        return listaLiczb;
    }

     static Double zwrociSredniaPodanychLiczb(List<Integer> lista) {
        Double suma = 0.0;
        for (Integer integer : lista) {
            suma += integer;
        }
        return suma / lista.size();
    }
}
