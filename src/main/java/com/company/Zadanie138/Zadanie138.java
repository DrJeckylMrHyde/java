package com.company.Zadanie138;

import java.io.*;

public class Zadanie138 {

    public static void main(String[] args) {
        try {
            // te pliki sa bez rozszerzen i w zasadzie nie musza byc
            // jesli stworzylem je bez tworzenia rozszerzenia to go nie podaje
            scalPliki("plikiDoCwiczenOdZad132/Zadanie138a", "plikiDoCwiczenOdZad132/Zadanie138b");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void scalPliki(String sciezka1, String sciezka2) throws IOException {
        BufferedReader odczytPierwszegoPliku = new BufferedReader(new FileReader(sciezka1));
        BufferedReader odczytDrugiegoPliku = new BufferedReader(new FileReader(sciezka2));
        //windows nie rozpoznaje duzych i malych liter
        BufferedWriter zapisNowegoPliku = new BufferedWriter(new FileWriter("plikiDoCwiczenOdZad132/Zadanie138.txt"));

        while (true) {
            String lineFirst = odczytPierwszegoPliku.readLine();
            String lineSecond = odczytDrugiegoPliku.readLine();
            if (lineFirst == null && lineSecond == null) {
                break;
            }
            if (lineFirst != null) {
                zapisNowegoPliku.write(lineFirst);
                zapisNowegoPliku.newLine();
            }
            if ((lineSecond != null)) {
                zapisNowegoPliku.write(lineSecond);
                zapisNowegoPliku.newLine();
            }
        }
        zapisNowegoPliku.close();
    }
}
