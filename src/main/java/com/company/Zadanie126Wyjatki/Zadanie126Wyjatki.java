package com.company.Zadanie126Wyjatki;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Zadanie126Wyjatki {
    public static void main(String[] args) {
        int[] table = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        podajIndeks(table);
    }

    private static void podajIndeks(int[] table) {
        Scanner input = new Scanner(System.in);
        while (true) {
            try {
                System.out.print("Podaj indeks: ");
                int indeks = input.nextInt();
                System.out.printf("Na indekse %s znajduje się liczba %s\n",
                        indeks,
                        table[indeks]);
                break;
            } catch (ArrayIndexOutOfBoundsException e) {
                //wyswietla czerwony komunikat o bledzie
                //e.printStackTrace();
                System.out.println("Podano indeks spoza zakresu");
            } catch (InputMismatchException e) {
                //e.printStackTrace();
                System.out.println("Podano znak zamiast numeru indeksu");
                //oczyszcza obiekt typu Scanner
                input.nextLine();
            }
        }
    }
}
