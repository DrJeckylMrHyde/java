package com.company.Zadanie125;

public class Pociag extends Pojazd {
    private int liczbaWagonow;
    private boolean czyMaWagonBarowy;

    Pociag(String nazwa, int liczbaWagonow, boolean czyMaWagonBarowy) {
        super(nazwa);
        this.liczbaWagonow = liczbaWagonow;
        this.czyMaWagonBarowy = czyMaWagonBarowy;
    }



    @Override
    public String toString() {
        return String.format("Pociąg %s ma %s wagonów i %s wagon barowy jest w stanie przewieźć %s pasażerów jego maksymalna prędkość wynosi %s",
                nazwa,
                liczbaWagonow,
                czyMaWagonBarowy ? "ma" : "nie ma",
                liczbaPasazerow,
                maxSpeed);
    }
}
