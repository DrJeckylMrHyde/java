package com.company.Zadanie125;

abstract class Pojazd implements CharakterystykaPojazdu {
     String nazwa;
     int liczbaPasazerow;
     double maxSpeed;

    Pojazd(String nazwa) {

        this.nazwa = nazwa;
    }


    //metod finalnych nie mozna nadpisywac
    @Override
    public final double maxPredkosc() {

        return maxSpeed;
    }

    @Override
    public final int liczbaPasazerow() {

        return liczbaPasazerow;
    }

    void setParametry(int liczbaPasazerow,double maxSpeed) {
        this.liczbaPasazerow = liczbaPasazerow;
        this.maxSpeed = maxSpeed;
    }
}
