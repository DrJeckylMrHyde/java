package com.company.Zadanie125;

public class Samochod extends Pojazd{
    private int liczbaKol;
    private int liczbaDrzwi;


    Samochod(String nazwa, int liczbaKol, int liczbaDrzwi) {
        super(nazwa);
        this.liczbaKol = liczbaKol;
        this.liczbaDrzwi = liczbaDrzwi;
    }

    @Override
    public String toString() {
        return String.format("Samochód %s posiada %s kół oraz %s drzwi jego maksymalna prędkość wynosi %s i jest w stanie przewieźć %s pasażerów",
                nazwa,
                liczbaKol,
                liczbaDrzwi,
                maxPredkosc(),
                liczbaPasazerow());
    }
}
