package com.company.Zadanie125;

public interface CharakterystykaPojazdu {

    double maxPredkosc();
    int liczbaPasazerow();

}
