package com.company.Zadanie125;

import java.util.ArrayList;
import java.util.List;

public class Zadanie125 {
    public static void main(String[] args) {
        Pociag p1 = new Pociag("TLK",16,true);
        Pociag p2 = new Pociag("IC",8,false);
        Samochod s1 = new Samochod("Audi",4,4);
        Samochod s2 = new Samochod("Volvo",4,4);
        p1.setParametry(160,240);
        p2.setParametry(80,220);
        s1.setParametry(4,280);
        s2.setParametry(4,240);

        List<CharakterystykaPojazdu> pojazdy = new ArrayList<>();
        pojazdy.add(p1);
        pojazdy.add(p2);
        pojazdy.add(s1);
        pojazdy.add(s2);

        // to nie tworzy obiektu typu charakterystyka tylko korelacje z interfejsem
        for (CharakterystykaPojazdu charakterystykaPojazdu : pojazdy) {
            System.out.println(charakterystykaPojazdu);
        }
    }
}
