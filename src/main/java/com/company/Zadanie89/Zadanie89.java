package com.company.Zadanie89;

public class Zadanie89 {
    public static void main(String[] args) {
        System.out.println(zwracaWynikDzialania("multiply 3 4"));
    }

    public static int zwracaWynikDzialania(String dzialanie) {
        int liczba = 0;
        String [] tablica = dzialanie.split(" ");
        switch (tablica[0]) {
            case "add":
                for (int i = 1; i < tablica.length; i++) {
                    liczba += Integer.parseInt(tablica[i]);
                }
                break;
            case "substract":
                    liczba = Integer.parseInt(tablica[1])-Integer.parseInt(tablica[2]);
                break;
            case "multiply":
                liczba = Integer.parseInt(tablica[1])*Integer.parseInt(tablica[2]);
                break;
            case "divide":
                liczba = Integer.parseInt(tablica[1])/Integer.parseInt(tablica[2]);
                break;
        }
        return liczba;
    }
}
