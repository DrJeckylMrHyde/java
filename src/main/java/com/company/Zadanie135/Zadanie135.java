package com.company.Zadanie135;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Zadanie135 {
    private static final String separator = ",";
    public static void main(String[] args) {
        try {
            System.out.println(zwrocListeOsobZPliku("plikiDoCwiczenOdZad132/zadanie135.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<Osoba> zwrocListeOsobZPliku(String sciezkaPliku) throws IOException {
        List<Osoba> listaOsob = new ArrayList<>();
        BufferedReader bufor = new BufferedReader(new FileReader(sciezkaPliku));
        String line = bufor.readLine();
        while (line != null) {
            listaOsob.add(getOsobaZLinii(line));
            line = bufor.readLine();
        }
        return listaOsob;
    }

    public static Osoba getOsobaZLinii(String line){
        String [] dane = line.split(separator);
        boolean plec = Boolean.parseBoolean(dane[3]);
        int wiek = Integer.parseInt(dane[2]);
        return new Osoba(dane[0],dane[1],wiek,plec);
    }
}
