package com.company.Zadanie135;

public class Osoba {
    private String name;
    private String nazwisko;
    private int wiek;
    private boolean isWoman;

    public Osoba(String name, String nazwisko, int wiek, boolean isWoman) {
        this.name = name;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.isWoman = isWoman;
    }

    public String toString(){
        return String.format("%s %s %s (%s lat)",
                isWoman?"Kobieta":"Mężczyzna",
                name,
                nazwisko,
                wiek);
    }
}
