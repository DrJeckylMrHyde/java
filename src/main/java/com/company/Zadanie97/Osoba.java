package com.company.Zadanie97;

public class Osoba {


    private String imie;
    private String nazwisko;
    private Plec plec;

    Osoba(String imie, String nazwisko,Plec plec){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s",
                imie,
                nazwisko,
                plec);
    }
}
