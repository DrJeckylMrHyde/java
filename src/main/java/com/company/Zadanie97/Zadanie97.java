package com.company.Zadanie97;

public class Zadanie97 {
    public static void main(String[] args) {
        Osoba [] osoby = new Osoba[]{
                new Osoba("Tomasz","Góral", Plec.MEZCZYZNA),
                new Osoba("Anna","Góral", Plec.KOBIETA),
                new Osoba("Tomasz","Skrzynkowski", Plec.MEZCZYZNA)
        };
        for (Osoba osoba : osoby) {
            System.out.println(osoba);
        }
    }
}
