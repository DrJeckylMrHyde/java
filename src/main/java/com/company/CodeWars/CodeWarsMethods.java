package com.company.CodeWars;

import java.util.Arrays;
import java.util.stream.IntStream;

public class CodeWarsMethods {

    static int a = 5;
    static int b = 2;

    public static void main(String[] args) {
        //reverse(5);
        //System.out.println(dontGiveMeFive(1,5));
        //int[] array = null;
        //System.out.println(Arrays.toString(averages(array)));
        int[] table1 = new int[]{1, 2, 3, 4, 5};
        int[] table2 = new int[]{1, 3, 4, 5};
        //System.out.println(Arrays.toString(minMax2(table1)));
        //System.out.println(arithmetic(a, b, "add"));
        //maxRot(12345);
//        System.out.println(findDeletedNumber(table1, table2));
//        System.out.println(findDeletedNumber2(table1, table2));
        System.out.println(squareDigits(9119));
    }

    public static int[] reverse(int n) {
        int[] tab = new int[n];
        for (int i = 0; i < tab.length; i++) {
            tab[i] = tab.length - i;
        }
        return new int[n];
    }

    public static int dontGiveMeFive(int start, int end) {
        int sum = 0;
        String liczba = "";
        external:
        for (int i = start; i <= end; i++) {
            liczba = Integer.toString(i);
            for (int j = 0; j < liczba.length(); j++) {
                if (liczba.charAt(j) == '5') {
                    continue external;
                }
            }
            sum++;
        }
        return sum;
    }

    public static double[] averages(int[] numbers) {
        if (numbers == null || numbers.length < 2) {
            double[] avg = new double[0];
            return avg;
        } else {
            double[] avg = new double[numbers.length - 1];
            for (int i = 0; i < numbers.length - 1; i++) {
                avg[i] = (numbers[i] + numbers[i + 1]) / 2.0;
            }
            return avg;
        }
    }

    public static int[] minMax(int[] arr) {
        int[] lowestAndHighest = new int[2];
        int max = arr[0];
        int min = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        lowestAndHighest[0] = min;
        lowestAndHighest[1] = max;
        return lowestAndHighest;
    }

    public static int[] minMax2(int[] arr) {
        Arrays.sort(arr);
        return new int[]{arr[0], arr[arr.length - 1]};
    }

    public static int arithmetic(int a, int b, String operator) {
        switch (operator) {
            case "add":
                return a + b;
            case "substract":
                return a - b;
            case "multiply":
                return a * b;
            case "divide":
                return a / b;
            default:
                return 0;
        }
    }

    public static long maxRot(long n) {
        long max = n;
        String liczba = "" + n;
        int poczatek = 1;
        for (int i = 0; i < liczba.length() - 1; i++) {
            String nowy = "";
            for (int j = poczatek; j < liczba.length(); j++) {
                nowy += liczba.charAt(j);

            }
            nowy += liczba.charAt(0);
            poczatek++;
            System.out.println(nowy);
        }
        return max;
    }


    public static int findDeletedNumber(int[] arr, int[] mixedArr) {                // szuka jednego brakujacego elementu w tablicy
        if (arr.length == mixedArr.length) {
            return 0;
        }
        external:
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < mixedArr.length; j++) {
                if (arr[i] == mixedArr[j]) {
                    continue external;
                }
            }
            return arr[i];
        }
        return 0;
    }

    public static int findDeletedNumber2(int[] arr, int[] mixedArr) {
        int sum1 = 0;
        int sum2 = 0;
        for (int i = 0; i < arr.length; i++) {
            sum1 += arr[i];
        }
        for (int i = 0; i < mixedArr.length; i++) {
            sum2 += mixedArr[i];
        }
        return sum1-sum2;
    }

    public static int squareDigits(int n) {
        String liczba = Integer.toString(n);
        int [] table1 = new int [liczba.length()];
        int tens = 0;
        int wynik = 0;
        for (int i = table1.length-1; i >= 0; i--) {
            table1[i] = (int) Math.pow(n%10,2)*10*tens;             // to jest bledne poniewaz zeruje pierwszy element
            wynik += table1[i];
            tens++;
        }
        return wynik;
    }

}


