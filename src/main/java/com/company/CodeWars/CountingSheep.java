package com.company.CodeWars;

public class CountingSheep {
    public static void main(String[] args) {
        System.out.println(countingSheep(5));
    }

    public static String countingSheep(int num) {
        String sheep = " sheep...";
        String counted = "";
        for(int i = 1; i <= num; i++){
            counted = i+sheep;
        }//Add your code here
        return counted;
    }
}
