package com.company.CodeWars;


import java.lang.reflect.Array;
import java.util.Arrays;


public class TwoToOne {
    public static void main(String[] args) {
        char sign2 = 'b';
        char sign = 'a';

        System.out.println(sign);
        System.out.println(sign2);
        String s1 = "xyaabbbccccdefww";
        String s2 = "xxxxyyyyabklmopq";
        System.out.println(longest(s1, s2));
        System.out.println(longest2(s1, s2));
        System.out.println(longest3(s1, s2));
        //abcdefklmopqwxy
        //System.out.println(accum("ZpglnRxqenU"));
    }

    public static String longest(String s1, String s2) {
        String[] uniqueA = Arrays.stream(s1.split("")).distinct().toArray(String[]::new);
        String[] uniqueB = Arrays.stream(s2.split("")).distinct().toArray(String[]::new);
        String[] both = Arrays.copyOf(uniqueA, uniqueA.length + uniqueB.length);
        System.arraycopy(uniqueB, 0, both, uniqueA.length, uniqueB.length);
        String[] resultUnique = (Arrays.stream(both).distinct().toArray(String[]::new));
        Arrays.sort(resultUnique);
        String result = "";
        for (int i = 0; i < resultUnique.length; i++) {
            result += resultUnique[i];
        }
        return result;
    }

    public static String longest2(String s1, String s2) {
        String[] uniqueA = s1.split("");
        String[] uniqueB = s2.split("");
        String[] both = Arrays.copyOf(uniqueA, uniqueA.length + uniqueB.length);
        System.arraycopy(uniqueB, 0, both, uniqueA.length, uniqueB.length);
        String[] resultUnique = (Arrays.stream(both).distinct().toArray(String[]::new));
        Arrays.sort(resultUnique);
        String result = "";
        for (int i = 0; i < resultUnique.length; i++) {
            result += resultUnique[i];
        }
        return result;
    }

    public static String longest3(String s1, String s2) {
        String longString = s1 + s2;
        String result = "";
        for (char sign = 'a'; sign <= 'z'; sign++) {
            if (longString.contains(String.valueOf(sign))) {
                result += sign;
            }
        }
        return result;
    }

}
