package com.company.CodeWars;


import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;


public class Solution {

    public static void main(String[] args) {
//        int[] table = new int[]{4, 3, 5};
//        int[] table2 = new int[]{5, 4, 2, 3};
//        System.out.println(maxProduct(table, 2));
//        System.out.println(minSum(table2));
//        System.out.println(replace("HoHOho!!"));
        System.out.println(replace2("HoHOho!!"));
    }

    public static String replace2(final String s){
        return s.replaceAll("[aeiouAEIOU]","!");
    }

    public static String replace(final String s) {
        String result = s;
        List<Character> vowels = Arrays.asList('A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u');
        for (int i = 0; i < result.length(); i++) {
            for (int j = 0; j < vowels.size(); j++) {
                if (result.charAt(i) == vowels.get(j)) {
                    result = result.replace(result.charAt(i), '!');
                }
            }
        }
        return result;
    }

    public static int minSum(int[] passed) {


        int sum = 0;
        Arrays.sort(passed);
        int j = passed.length - 1;
        for (int i = 0; i < passed.length / 2; i++) {
            sum += passed[i] * passed[j];
            j--;
        }
        return sum;
    }

    public static long maxProduct(int[] numbers, int sub_size) {

        return Arrays.stream(numbers)
                .sorted()
                .skip(numbers.length - sub_size)
                .mapToLong(Long::valueOf)
                .reduce(1, (x, y) -> x * y);

//        long product = 1;
//        Arrays.sort(numbers);
//        for (int i = numbers.length-1; i >= numbers.length - sub_size; i--) {
//            product *= numbers[i];
//        }
//        return product;
    }

    public static double solution(double[] arrVal, String[] arrUnit) {

//        double G = 6.67E-11;

        for (int i = 0; i < arrVal.length; i++) {

            switch (arrUnit[i]) {

                case "g":
                    arrVal[i] /= 1_000;
                    break;
                case "mg":
                    arrVal[i] /= 1_000_000;
                    break;
                case "μg":
                    arrVal[i] /= 1_000_000_000;
                    break;
                case "lb":
                    arrVal[i] *= 0.453592;
                    break;
                case "cm":
                    arrVal[i] /= 1_00;
                    break;
                case "mm":
                    arrVal[i] /= 1_000;
                    break;
                case "μm":
                    arrVal[i] /= 1_000_000;
                    break;
                case "ft":
                    arrVal[i] *= 0.3048;
                    break;
            }
        }

        double g = 6.67E-11;
        return g * arrVal[0] * arrVal[1] / Math.pow(arrVal[2], 2);
    }

    private static int[] extraPerfect2(int number) {
        int[] table = new int[number];
        for (int i = 0; i < table.length; i++) {
            table[i] = i + 1;
        }
        return Arrays.stream(table)
                .filter(x -> x % 2 != 0)
                .toArray();
    }

    private static int[] extraPerfect3(int number) {

        return IntStream.rangeClosed(1, number)
                //zawartosc filtra zwraca wartosc logiczna
//                .filter(num -> isBinaryFiltered(num))
//                .filter(Solution::isBinaryFiltered)
                .filter(num -> {
                    boolean result = isBinaryFiltered(num);
                    System.out.printf("decimal %s binary %s condition %s\n",
                            num
                            , Integer.toBinaryString(num)
                            , result);
                    return result;
                })
                .toArray();
    }

    private static boolean isBinaryFiltered(int num) {
        final char CONDITION = '1';
        String binaryString = Integer.toBinaryString(num);
        if (binaryString.charAt(0) == CONDITION
                && binaryString.charAt(binaryString.length() - 1) == CONDITION) {
            if (binaryString.length() <= 2) {
                return true;
            }
            String subBinaryString = binaryString.substring(1, binaryString.length() - 1);
            return !subBinaryString.contains("1");
        }
        return false;
    }

    static String removeExclamationMarks(String s) {

        return s.replaceAll("!", "");
    }
}

