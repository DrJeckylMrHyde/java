package com.company.CodeWars;

public class GrassHopper {
    public static void main(String[] args) {
        System.out.println(summation(17));
    }

    public static int summation(int n) {
        double r = (1 + n) / 2.0 * n;
        return (int) r;
    }
}
