package com.company.CodeWars;

public class Clock {
    private static final int SECTOMILI = 1_000;
    private static final int MINTOSEC = 60;
    private static final int HOURTOMIN = 60;

    public static int Past(int h, int m, int s)
    {
        return h* SECTOMILI * MINTOSEC *HOURTOMIN + m*SECTOMILI*MINTOSEC + s* SECTOMILI;
    }
}
