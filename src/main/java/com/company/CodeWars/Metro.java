package com.company.CodeWars;

import java.util.ArrayList;
import java.util.List;

public class Metro {
    public static void main(String[] args) {
        ArrayList<int[]> list = new ArrayList<int[]>();
        list.add(new int[]{10, 0});
        list.add(new int[]{3, 5});
        list.add(new int[]{2, 5});
        System.out.println(countPassengers(list));

    }

    public static int countPassengers(ArrayList<int[]> stops) {
        int result = 0;
//        for(int i = 0; i < stops.size();i++) {
//            result += stops.get(i)[0];
//            result -= stops.get(i)[1];
//        }
        for (int[] busStop : stops) {
            result += busStop[0];
            result -= busStop[1];
        }
        return result;
    }

    public static int countPassengersStream(ArrayList<int[]> stops){
        return stops.stream()
                .mapToInt(array -> array[0]-array[1])
                .sum();
    }
}
