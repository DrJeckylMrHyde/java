package com.company.CodeWars;

public class TrafficLights {
    public static void main(String[] args) {

    }

    public static String updateLight(String current) {

        switch (current) {

            case "green":
                return "yellow";
            case "red":
                return "green";
            default:
                return "red";
        }
    }
}
//            case "yellow":
//                return "red";
