package com.company.CodeWars;

public class Average {

    public static void main(String[] args) {
        int [] table = new int []{1,2,3,4,5,6,7,8,9};
        System.out.println(find_average(table));
    }

    public static double find_average(int[] array){
        int sum = 0;
        for(int i = 0; i < array.length; i++){
            sum += array[i];
        }
        return (double) sum/array.length;
    }
}
