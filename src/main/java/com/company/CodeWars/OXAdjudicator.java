package com.company.CodeWars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OXAdjudicator {

    public static void main(String[] args) {
        List<Integer> history = new ArrayList<>();



//        X_win
        history.add(0);
        history.add(1);
        history.add(4);
        history.add(8);
        history.add(3);
        history.add(6);
        history.add(5);

//        no_winner
//        history.add(4);
//        history.add(2);
//        history.add(8);
//        history.add(0);
//        history.add(1);
//        history.add(7);
//        history.add(5);
//        history.add(3);
//        history.add(6);

        //        O_win
//        history.add(0);
//        history.add(2);
//        history.add(4);
//        history.add(5);
//        history.add(6);
//        history.add(8);

        System.out.println(history);

        System.out.println(judge2(history));

    }

    public static String judge2(List<Integer> history) {
        return null;
    }

    public static String judge(List<Integer> history) {
        int Osum = 0;
        int Xsum = 0;
        int commonX = 0;
        int commonO = 0;

        for (int i = 0; i < history.size(); i++) {
            if (i % 2 == 0) {
                Xsum += history.get(i);
                commonX++;
            } else {
                Osum += history.get(i);
                commonO++;
            }
        }
        if (Xsum / commonX == history.get(commonX)) {
            return "X_WIN";
        } else if (Osum / commonO == history.get(commonO/2)) {
            return "Y_WIN";
        } else {
            return "NO_WINNER";
        }
    }
}
