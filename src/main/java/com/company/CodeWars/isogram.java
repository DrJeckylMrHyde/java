package com.company.CodeWars;

public class isogram {
    public static void main(String[] args) {
        System.out.println(isIsogram("moose"));
        System.out.println(isIsogram("isogram"));
        System.out.println(isIsogram("Dermatoglyphics"));
        System.out.println(isIsogram(""));
        System.out.println(isIsogram("aba"));
        System.out.println(isIsogram("moOse"));

    }

    public static boolean isIsogram(String str){
        String [] letters = str.split("");
        for (int i = 0; i < letters.length; i++) {
            for (int j = i+1; j < letters.length; j++) {
                if(letters[i].toLowerCase().equals(letters[j].toLowerCase())){
                    return false;
                }
            }
        }
        return true;
    }
}
