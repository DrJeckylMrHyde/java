package com.company.CodeWars;

public class KataMixedColouredTriangles {
    public static void main(String[] args) {
        System.out.println(triangle("RBRG"));
    }

    public static char triangle(final String row) {
        char[] table = row.toCharArray();
        char [] table2 = table;

        for (int j = 0; j < row.length(); j++) {
            if (table.length == 1) {
                return table2[0];
            }
            table2 = new char[table.length - 1];
            for (int i = 0; i < table.length - 1; i++) {
                if (table[i] == table[i + 1]) {
                    table2[i] = table[i];
                } else {
                    table2[i] = "RGB".replace(String.valueOf(table[i]), "")
                            .replace(String.valueOf(table[i + 1]), "")
                            .charAt(0);
                }
            }
            table = table2;
        }
        return table2[0];
    }
}
