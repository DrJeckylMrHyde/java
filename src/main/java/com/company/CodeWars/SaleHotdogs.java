package com.company.CodeWars;

public class SaleHotdogs {
    public static void main(String[] args) {
        System.out.println(saleHotdogs(1));
        System.out.println(saleHotdogs(4));
        System.out.println(saleHotdogs(5));
        System.out.println(saleHotdogs(9));
        System.out.println(saleHotdogs(10));
        System.out.println(saleHotdogs(100));
    }

    public static int saleHotdogs(final int n){

        return n < 5 ? n*100 : n < 10 ? n*95 : n*90;
    }

}
