package com.company.CodeWars;

public class SolutionValidatePin {
    public static void main(String[] args) {
        System.out.println(validatePin("1230"));
        System.out.println(validatePin("123456"));
        System.out.println(validatePin("12345"));
    }

    public static boolean validatePin(String pin){
        //return pin.matches ("[0-9]{4}|[0-9]{6}");
        return pin.matches ("\\d{4}|\\d{6}");
    }
}
