package com.company.CodeWars;


public class Solve {
    public static void main(String[] args) {
        System.out.println(solve(2));
        System.out.println(solve(4));
        System.out.println(solve(5));
        System.out.println(solve(8));
    }

    public static double solve(double m) {

        return (2 * m + 1 - Math.sqrt(4 * m + 1)) / (2 * m);
    }

}