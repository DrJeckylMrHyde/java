package com.company.CodeWars;

import java.util.stream.IntStream;

public class GpsSpeed {
    public static void main(String[] args) {
        double[] x = new double[]{0.0, 0.23, 0.46, 0.69, 0.92, 1.15, 1.38, 1.61};
//        x = new double[] {0.0, 0.11, 0.22, 0.33, 0.44, 0.65, 1.08, 1.26, 1.68, 1.89, 2.1, 2.31, 2.52, 3.25};
        x = new double[]{0.0, 0.18, 0.36, 0.54, 0.72, 1.05, 1.26, 1.47, 1.92, 2.16, 2.4, 2.64, 2.88, 3.12, 3.36, 3.6, 3.84};
//        System.out.println(gps(20, x));
//        System.out.println(gps(12, x));
        System.out.println(gps2(20, x));
    }

    public static int gps(int s, double[] x) {
        int maxFloorSpeed = 0;
        if (x.length < 2) {
            return maxFloorSpeed;
        }
        for (int i = 0; i < x.length - 1; i++) {
            double speed = 3600 * (x[i + 1] - x[i]) / s;
            if (speed > maxFloorSpeed) {
                maxFloorSpeed = (int) speed;
            }
        }
        return maxFloorSpeed;
    }

    public static int gps2(int s, double[] x) {
        double maxSpeed = IntStream
                .range(0, x.length - 1)
                .mapToDouble(n -> (3600 * (x[n + 1] - x[n])) / s)
                .max().orElse(0.0);
        return (int) maxSpeed;

    }

}
