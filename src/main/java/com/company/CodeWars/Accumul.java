package com.company.CodeWars;

public class Accumul {
    private final static String separator= "-";

    public static void main(String[] args) {
        System.out.println(accum("ziomek"));
    }
    private static String accum(String s) {
        String[] letters = s.toLowerCase().split("");
        StringBuilder multiplyLetters = new StringBuilder();
        for (int i = 0; i < letters.length; i++) {
            for (int j = 0; j < i + 1; j++) {
                if (j == 0) {
                    multiplyLetters.append(letters[i].toUpperCase());
                }
                else {
                    multiplyLetters.append(letters[i]);
                }
            }
            //if(i != letters.length-1) {
                multiplyLetters.append(separator);
            //}
        }
        // replaceFirst zamienia pierwsze dopasowanie
        // .$ oznacza dowolny znak na koncu linii
        //return multiplyLetters.replaceFirst(".$","");
        return multiplyLetters.substring(0,multiplyLetters.length()-1);
    }
}
