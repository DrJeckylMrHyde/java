package com.company.CodeWars;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Vowels {
    public static void main(String[] args) {

    }

    public static int getCount(String str) {


        return (int) str
                .chars()
                .mapToObj(i -> (char) i)
                .filter(c -> "aeiou".contains(String.valueOf(c)))
                .count();
    }
}
