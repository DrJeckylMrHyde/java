package com.company.CodeWars;

import java.util.Arrays;

public class SmallestIntegerFinder {
    public static void main(String[] args) {
        int [] table = new int[]{-3,33,-66};
        System.out.println(findSmallestInt(table));
    }
    public static int findSmallestInt(int[] args) {
        Arrays.sort(args);
        return args[0];
    }
}
