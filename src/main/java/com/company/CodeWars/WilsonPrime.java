package com.company.CodeWars;

import static java.lang.Math.pow;

public class WilsonPrime {
    public static void main(String[] args) {

//        System.out.println(am_i_wilson(1));
        System.out.println(am_i_wilson(5));
//        System.out.println(Math.ceil(158));
    }

    public static boolean am_i_wilson(double n) {
        if(n == 0 || n == 1){
            return false;
        }
        double r = (factorial((n - 1)) + 1) / pow(n,2);
        if(r == Math.floor(r) && !Double.isInfinite(r)){
            return true;
        } else {
            return false;
        }
    }

    public static double factorial(double n) {
        if (n < 2)
            return 1;
        else
            return (n * factorial(n - 1));
    }
}
