package com.company.CodeWars;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SortAndStar {
    public static void main(String[] args) {
        String [] s = new String[]{"bitcoin", "take", "over", "the", "world", "maybe", "who", "knows", "perhaps"};
//        System.out.println(twoSort(s));
        System.out.println(twoSort2(s));
        Map<String,Integer> map = new HashMap<>();

    }

    public static String twoSort(String[] s) {
        String result = "";
        Arrays.sort(s);
        for (int i = 0; i < s[0].length()-1; i++) {
            result += s[0].charAt(i)+"***";
        }
        result += s[0].charAt(s[0].length()-1);
        return result;
    }

    public static String twoSort2(String [] s){
//        Arrays.sort(s);
        String [] letters = Arrays.stream(s)
                .sorted()
                .findFirst()
                .orElse("")
                .split("");
        //pierwszy parametr to co bedzie miedzy znakami
        // drugi to mech varargs tablica lub inna kolekcja
        return String.join("***",letters);
    }
}
