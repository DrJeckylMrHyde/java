package com.company.CodeWars;

public class Nessie {
    public static void main(String[] args) {
        System.out.println(isLockNessMonster("Your girlscout cookies are ready to ship. Your total comes to tree fiddy"));
    }

    public static boolean isLockNessMonster(String s){
        return s.contains("tree fiddy") || s.contains("3.50");
    }
}
