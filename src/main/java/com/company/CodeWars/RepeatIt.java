package com.company.CodeWars;

import java.util.stream.IntStream;

public class RepeatIt {

    public static void main(String[] args) {
        System.out.println(repeatString("Hi", 2));
    }

    public static String repeatString(final Object toRepeat, final int n) {

            StringBuilder sb = new StringBuilder();
        if(toRepeat instanceof String){
            for (int i = 0; i < n; i++) {
            sb.append(toRepeat);
            }
        }
        else{
            return "Not a string";
        }
        return sb.toString();
    }
}
