package com.company.CodeWars;

public class StringUtils {
    public static void main(String[] args) {
        String hello = "helLo world2";
        System.out.println(toAlternativeString(hello));

    }

    // bardzo przydatne zadanie praca na klasie Character umozliwia zmiane rozmiary liter
    // na string jest ignorowane i sluzy tylko do wyswietlania
    public static String toAlternativeString(String string) {
        StringBuilder sentence = new StringBuilder();
        char[] chars = string.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (Character.isUpperCase(chars[i])) {
                sentence.append(Character.toLowerCase(chars[i]));

            } else {
                sentence.append(Character.toUpperCase(chars[i]));
            }
        }
        return sentence.toString();
    }

}