package com.company.CodeWars;

import java.awt.*;

public class DingleMouse {

//    tworze klase wewnetrzna
    private static class Punkt<FIRST,SECOND> {
        FIRST x;
        SECOND y;

        public Punkt(FIRST x, SECOND y) {
            this.x = x;
            this.y = y;
        }

    }

    private static final char[][] keyboard = {
            {'a', 'b', 'c', 'd', 'e', '1', '2', '3'},
            {'f', 'g', 'h', 'i', 'j', '4', '5', '6'},
            {'k', 'l', 'm', 'n', 'o', '7', '8', '9'},
            {'p', 'q', 'r', 's', 't', '.', '@', '0'},
            {'u', 'v', 'w', 'x', 'y', 'z', '_', '/'}};

    public static void main(String[] args) {
        System.out.println(tvRemote("work"));
        System.out.println(tvRemote("does"));
        System.out.println(tvRemote("your"));
        System.out.println(tvRemote("solution"));
    }

    public static int tvRemote(final String word) {

        int counter = word.length();
        Punkt<Integer, Integer> start = new Punkt<>(0, 0);
        Punkt<Integer, Integer> end;


        for (int i = 0; i < word.length(); i++) {
            end = coordinates(word.charAt(i));
//            System.out.println(start + "->" + end);
            counter += distanceBeetweenPointsAndConfirm(start,end);
            start = end;
        }
        return counter;
    }

    public static Punkt<Integer, Integer> coordinates(char letter) {
        for (int wysokosc = 0; wysokosc < keyboard.length; wysokosc++) {
            for (int szerokosc = 0; szerokosc < keyboard[wysokosc].length; szerokosc++) {
                if (keyboard[wysokosc][szerokosc] == letter) {
                    return new Punkt<>(wysokosc, szerokosc);
                }
            }
        }
        return null;
    }

    public static int distanceBeetweenPointsAndConfirm(Punkt<Integer, Integer> start, Punkt<Integer, Integer> end) {

        int wysokosc = Math.abs(start.x - end.x);
        int szerokosc = Math.abs(start.y - end.y);

        return wysokosc + szerokosc;
    }
}
