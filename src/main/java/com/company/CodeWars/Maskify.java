package com.company.CodeWars;

import java.util.Arrays;

public class Maskify {
    public static void main(String[] args) {
        System.out.println(maskify("4556364607935616"));
    }

    public static String maskify(String str) {
        String[] letters = str.split("");
        if (str.length() <= 4) {
            return str;
        }
        String bufor = str;
        str = "";
        for (int i = 0; i < letters.length; i++) {
            if (i <= letters.length - 5) {
                letters[i] = "#";
                str += letters[i];
            }
        }
        return str + bufor.substring(bufor.length() - 4);
    }
}
