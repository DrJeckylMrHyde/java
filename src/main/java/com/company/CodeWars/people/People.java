package com.company.CodeWars.people;

public class People {

    public static void main(String[] args) {
        People person = People
                .builder()
                .name("Adam")
                .lastName("Kononowicz")
                .age(25)
                .job("Engineer")
                .city("Łódź")
                .build();

        People person2 = People
                .builder()
                .name("Tomek")
                .lastName("Kasperkiewicz")
                .age(26)
                .job("Plumb")
                .city("Warszawa")
                .build();

        System.out.println(person.getName());
        System.out.println(person.getLastName());
        System.out.println(person.getAge());
        System.out.println(person.getJob());
        System.out.println(person.getCity());
        System.out.println(person.greet());

        System.out.println(person);
        System.out.println(person2);
    }

    private final int age;
    private final String name;
    private final String lastName;
    private final String job;
    private final String city;

    private People(PeopleBuilder builder) {
        this.age = builder.age;
        this.name = builder.name;
        this.lastName = builder.lastName;
        this.job = builder.job;
        this.city = builder.city;
    }

    @Override
    public String toString() {
        return "People{" + "age=" + age +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", job='" + job + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    private static PeopleBuilder builder() {
        return new People.PeopleBuilder();
    }

    private String greet() {
        String greet = "hello";
        return greet + " my name is " + this.name;
    }

    private String getName() {
        return name;
    }

    private int getAge() {
        return age;
    }

    private String getLastName() {
        return lastName;
    }

    private String getJob() {
        return job;
    }

    private String getCity() {
        return city;
    }

    static class PeopleBuilder {
        private int age;
        private String name;
        private String lastName;
        private String city;
        private String job;

        PeopleBuilder city(String city) {
            this.city = city;
            return this;
        }

        PeopleBuilder job(String job) {
            this.job = job;
            return this;
        }

        PeopleBuilder name(String name) {
            this.name = name;
            return this;
        }

        PeopleBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        PeopleBuilder age(int age) {
            this.age = age;
            return this;
        }

        People build() {
            return new People(this);
        }

    }
}
