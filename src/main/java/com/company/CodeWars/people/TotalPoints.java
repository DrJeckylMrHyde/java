package com.company.CodeWars.people;

public class TotalPoints {

    public static void main(String[] args) {
        String [] table =
                new String[]{"1:0","2:0","3:0","4:0","2:1","3:1","4:1","3:2","4:2","4:3"};

        System.out.println(points(table));
    }

    public static int points(String[] games) {

        int sumPoints = 0;
        for (String singleGame : games) {
            String[] digits = singleGame.split(":");
            int homeGoals = Integer.parseInt(digits[0]);
            int guestsGoals = Integer.parseInt(digits[1]);

            if (homeGoals > guestsGoals) {
                sumPoints += 3;
            } else if (homeGoals == guestsGoals) {
                sumPoints += 1;
            }
        }
        return sumPoints;
    }
}
