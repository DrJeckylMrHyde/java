package com.company.CodeWars;

public class Movie {

    public static void main(String[] args) {
        System.out.println(movie(500, 15, 0.9));
    }

    public static int movie(int card, int ticket, double perc) {
        int n = 1;
        int singleTickets = ticket;
        double rek = ticket;
        while (card >= singleTickets) {
            double toCard = rek * perc;
            rek = toCard;
//            System.out.println(toCard);
            card += rek;
            singleTickets += ticket;
            System.out.println(singleTickets);
            n++;
        }
        return n;
    }
}
