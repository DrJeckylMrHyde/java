package com.company.CodeWars;

public class Printer {

    public static void main(String[] args) {
        System.out.println(printerError("aaabbbbhaijjjm"));
        System.out.println(printerError("aaaxbbbbyyhwawiwjjjwwm"));
    }

    public static String printerError(String s) {
        String wrong = "nopqrstuvwxyz";
        String[] litery = wrong.split("");
        String[] slowoNaLitery = s.split("");
        int licznik = 0;
        external:
        for (int i = 0; i < slowoNaLitery.length; i++) {
            for (int j = 0; j < litery.length; j++) {
                if (slowoNaLitery[i].equals(litery[j])) {
                    licznik++;
                    continue external;
                }
            }
        }
        return String.format("%s/%s",
                licznik,
                s.length());
    }

    public static String printerError2(String s) {
        return s.replaceAll("[a=m]", "").length() + "/" + s.length();
    }
}
