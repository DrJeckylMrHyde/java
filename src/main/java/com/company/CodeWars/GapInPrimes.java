package com.company.CodeWars;

import java.util.Arrays;

public class GapInPrimes {
    public static void main(String[] args) {
//        System.out.println(Arrays.toString(gap(2, 3, 50)));
        System.out.println(Arrays.toString(gap2(2, 3, 50)));
    }

    public static long[] gap(int g, long m, long n) {

        long firstPrime = 0;
        long secondPrime = 0;
        long gap = 1;
        int rep = 0;

        while (gap != g && gap != 0) {
            for (long i = m; i < n; i++) {
                if (rep > 0) {
                    firstPrime = secondPrime;
                    break;
                }
                if (isPrimeOrNot(i)) {
                    firstPrime = i;
//                    System.out.println(firstPrime);
                    break;
                }
            }

            for (long i = firstPrime + 1; i < n; i++) {
                if (isPrimeOrNot(i)) {
                    secondPrime = i;
//                    System.out.println(secondPrime);
                    break;
                }
            }
            gap = secondPrime - firstPrime;
//            System.out.println("gap "+gap);
            rep++;
        }
        if(gap == g){
            return new long []{firstPrime,secondPrime};
        } else {
            return null;
        }
    }



    public static boolean isPrimeOrNot(long c) {

        for (int i = 2; i <= c / 2; i++) {
            if (c % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static long[] gap2(int g, long m, long n) {
        long last = Long.MIN_VALUE;
        for (long i = m; i < n; i++) {
            if (isPrimeOrNot(i)) {
                if (i - last == g) {
                    return new long[]{last, i};
                }
                last = i;
                System.out.println(last);
            }
        }

        return null;
    }
}
