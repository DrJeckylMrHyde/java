package com.company.CodeWars;

public class BumpsTheRoad {
    public static void main(String[] args) {
        System.out.println(bumps("_nnnnnnn_n__n______nn__nn_nnn"));
    }

    public static String bumps(final String road) {
        String [] letters = road.split("");
        int bumps = 0;
        for (int i = 0; i < letters.length; i++) {
            if(letters[i].equals("n")){
                bumps++;
            }
        }
        if(bumps > 15){
            return "Car Dead";
        }
        return "Woohoo!";
    }
}
