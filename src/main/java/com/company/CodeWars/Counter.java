package com.company.CodeWars;

import java.util.stream.IntStream;

public class Counter {
    public static void main(String[] args) {
        Boolean[] array1 = {true, true, true, false,
                true, null, true, true,
                true, false, true, false,
                true, false, false, true,
                true, true, true, true,
                false, false, true, true};
        System.out.println(countSheeps(array1));
    }

    public static int countSheeps(Boolean[] arrayOfSheeps) {
        if (arrayOfSheeps == null || arrayOfSheeps.length == 0) return 0;
        int counter = 0;
        for (Boolean arrayOfSheep : arrayOfSheeps) {
            if (arrayOfSheep == null) continue;

            if (arrayOfSheep) counter++;
        }
        return counter;
    }
//        long sheeps = IntStream.range(0, arrayOfSheeps.length)
//                .filter(x -> arrayOfSheeps[x])
//                .count();
//        return (int) sheeps;
//    }


}
