package com.company.CodeWars;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class KataLeetSpeak {
    public static void main(String[] args) {
        System.out.println(toLeetSpeak("LEET"));
    }

    static String toLeetSpeak(final String speak){
        Map<String, String> leetMap = new HashMap<>();

        leetMap.put("A", "@");
        leetMap.put("B", "8");
        leetMap.put("C", "(");
        leetMap.put("D", "D");
        leetMap.put("E", "3");
        leetMap.put("F", "F");
        leetMap.put("G", "6");
        leetMap.put("H", "#");
        leetMap.put("I", "!");
        leetMap.put("J", "J");
        leetMap.put("K", "K");
        leetMap.put("L", "1");
        leetMap.put("M", "M");
        leetMap.put("N", "N");
        leetMap.put("O", "0");
        leetMap.put("P", "P");
        leetMap.put("Q", "Q");
        leetMap.put("R", "R");
        leetMap.put("S", "$");
        leetMap.put("T", "7");
        leetMap.put("U", "U");
        leetMap.put("V", "V");
        leetMap.put("W", "W");
        leetMap.put("X", "X");
        leetMap.put("Y", "Y");
        leetMap.put("Z", "2");
        leetMap.put(" ", " ");

//        String [] table = speak.split("");
//
//        StringBuilder result = new StringBuilder();
//
//        for (String letter : table) {
//            result.append(leetMap.getOrDefault(letter, letter));
//
//            if(leetMap.containsKey(letter)){
//                result.append(leetMap.get(letter))
//            } else {
//                result.append(letter);
//            }
//        }
//
//        return result.toString();

        return Arrays.stream(speak.split(""))
                .map(letter -> leetMap.getOrDefault(letter, letter))
                .collect(Collectors.joining());
    }
}
