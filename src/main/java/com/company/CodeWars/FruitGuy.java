package com.company.CodeWars;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.stream.Stream;

public class FruitGuy {
    public static void main(String[] args) {
        String[] fruitBasket = new String[]{"rottenApple"};
        System.out.println(Arrays.toString(removeRotten(fruitBasket)));
    }

    public static String[] removeRotten(String[] fruitBasket) {
        if(fruitBasket == null){
            return new String[0];
        }
        return Arrays.stream(fruitBasket)
                .map(fruit -> fruit.toLowerCase().replace("rotten",""))
                .toArray(String[]::new);
//                .toArray(array -> new String [array]));
    }
}
