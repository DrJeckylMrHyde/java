package com.company.CodeWars;

public class Suite {
    public static void main(String[] args) {
        System.out.println(going(5));
        System.out.println(going2(5));
        System.out.println(going2(6));
        System.out.println(going2(7));
    }

    public static double going(int n) {
        double ulamek = 1;
        double wynik = 1;

        for (int i = n; i > 1; i--) {
            ulamek /= i;
            wynik += ulamek;
        }

        return (int) (wynik * 1000000) / 1000000.0;
    }

    public static double going2(int n) {
        double wynik = 1;

        for (int i = 2; i <= n; i++) {
            double ulamek = 1;
            for (int j = i; j <= n; j++) {
                ulamek /= j;
            }
            wynik += ulamek;
        }
        return (int) (wynik * 1e6) / 1e6;
    }
}
