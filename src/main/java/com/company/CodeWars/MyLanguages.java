package com.company.CodeWars;


import java.util.*;
import java.util.stream.Collectors;

public class MyLanguages {

    public static void main(String[] args) {
        Map<String, Integer> results = new HashMap<>();
        results.put("Hindi", 60);
        results.put("Dutch", 93);
        results.put("Greek", 71);
        System.out.println(myLanguages(results));
        System.out.println(myLanguagesStream(results));
    }

    private static List<String> myLanguages(final Map<String, Integer> results) {
        List<Map.Entry<String,Integer>> list = new ArrayList<>(results.entrySet());
        list.sort(Map.Entry.comparingByValue());
        Map<String, Integer> result2 = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : list) {
            result2.put(entry.getKey(), entry.getValue());
        }
        List<String> finish = new LinkedList<>();
        for (Map.Entry<String, Integer> entry : result2.entrySet()) {
            if(entry.getValue() >= 60){
                finish.add(entry.getKey());
            }
        }
        Collections.reverse(finish);
        return finish;
    }

    private static List<String> myLanguagesStream(final Map<String, Integer> results) {
        return results.entrySet()
                .stream()
                .filter(entry -> entry.getValue() >= 60)
                //tworze tu comparator
                .sorted((lang1,lang2) -> Integer.compare(lang2.getValue(),lang1.getValue()))
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());
    }

}
