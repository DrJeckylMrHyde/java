package com.company.CodeWars;

public class Banjo {
    public static void main(String[] args) {
        System.out.println(areYouPlayingBanjo("Bambo"));
        System.out.println(areYouPlayingBanjo("Rambo"));
    }

    public static String areYouPlayingBanjo(String name) {
        return name + (name.toLowerCase().startsWith("r") ? " plays banjo" : " does not play banjo");
//        return name + (name.toLowerCase().charAt(0) == 'r' ? " plays banjo" : " does not play banjo");
    }
}
