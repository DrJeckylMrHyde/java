package com.company.CodeWars;

public class CodeWars {
    public static void main(String[] args) {
        System.out.println(strCount("Hello", 'o'));
        System.out.println(strCount("Hello", 'l'));
        System.out.println(strCount("", 'z'));
    }

    public static int strCount(String str, char letter) {
//        if(str.length() == 0 ){
//        return 0;
//        }
//        int counter = 0;
//        for (int i = 0; i < str.length(); i++) {
//            if (str.charAt(i) == letter) {
//                counter++;
//            }
//        }
//        return counter;
        return (int) str.chars().filter(x -> x == letter).count();
    }


}
