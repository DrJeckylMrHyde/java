package com.company.CodeWars;

import java.util.Arrays;
import java.util.stream.Collectors;

public class SpinWords {

    public static void main(String[] args) {
        System.out.println(spinWords("Hey fellow warriors"));
    }

    public static String spinWords(String sentence){
        String separator = " ";
        String [] split = sentence.split(separator);
        return Arrays
                .stream(split)
                .map(slowo -> {
                    if(slowo.length()>4){
//                        return new StringBuilder(slowo).reverse().toString();
                        String noweSlowo = "";
                        for (char s : slowo.toCharArray()) {
                            noweSlowo = s + noweSlowo;
                        }
                        return noweSlowo;
                    } else {
                        return slowo;
                    }
                })
                .collect(Collectors.joining(separator));
    }
}
