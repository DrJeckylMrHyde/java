package com.company.CodeWars;

import java.util.Arrays;

public class School {

    public static void main(String[] args) {
        int [] table = new int []{1,1,1,1,1,1,1,2};
        System.out.println(getAverage(table));
    }

    public static int getAverage(int[] marks){
        return Arrays.stream(marks).sum()/marks.length;
    }
}
