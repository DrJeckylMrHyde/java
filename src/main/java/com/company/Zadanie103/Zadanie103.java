package com.company.Zadanie103;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie103 {
    public static void main(String[] args) {
        Integer [] tablica = new Integer[]{ 1, 2, 3, 4, 4, 6, 4, 8, 9};
        List<Integer> lista = new ArrayList<>(Arrays.asList(tablica));
        System.out.println(listaZPozycjamiSzukanejLiczbyWLiscie(lista, 4));
    }

    private static List<Integer> listaZPozycjamiSzukanejLiczbyWLiscie(List<Integer> lista, Integer liczba){
        List<Integer> listaIndeksow = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            if(liczba.equals(lista.get(i))){
                listaIndeksow.add(i);
            }
        }
        return listaIndeksow;
    }
}


