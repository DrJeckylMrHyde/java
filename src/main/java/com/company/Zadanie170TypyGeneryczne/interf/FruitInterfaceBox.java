package com.company.Zadanie170TypyGeneryczne.interf;

public class FruitInterfaceBox {

    private FruitInterface fruit;

    public FruitInterfaceBox(FruitInterface fruit) {
        this.fruit = fruit;
    }

    public FruitInterface getFruit() {
        return fruit;
    }

    public void setFruit(FruitInterface fruit) {
        this.fruit = fruit;
    }
}
