package com.company.Zadanie170TypyGeneryczne.methods;

import com.company.Zadanie170TypyGeneryczne.shape.Circle;
import com.company.Zadanie170TypyGeneryczne.shape.Rectangle;
import com.company.Zadanie170TypyGeneryczne.shape.ShapeBox;
import com.company.Zadanie170TypyGeneryczne.shape.Square;

public class VoidMethods {

//   Wymagamy obiekt klasy ShapeBox z Circle w srodku
    public static void method1(ShapeBox<Circle> box){

        System.out.println(box.getNameOfShape());
    }

//    ShapeBox z "czyms" w srodku
//    Nie weryfikujemy co jest w srodku
//    ograniczeniem zajmuj sie klasa ShapeBox
    public static void method2(ShapeBox<?> box){

        System.out.println(box.getNameOfShape());
    }
//Wymagana jest klasa dziedzicząca po "Rectangle"
    public static void method3(ShapeBox<? extends Rectangle> box){
        System.out.println(box.getNameOfShape());
    }

//    Akceptujemy obiekty klasy "Rectangle" i WSZYSTKIE jej nadklasy
    public static void method4(ShapeBox<? super Rectangle> box){

    }
}
