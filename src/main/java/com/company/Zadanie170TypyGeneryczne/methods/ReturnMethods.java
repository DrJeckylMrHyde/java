package com.company.Zadanie170TypyGeneryczne.methods;

import com.company.Zadanie170TypyGeneryczne.shape.Circle;
import com.company.Zadanie170TypyGeneryczne.shape.Shape;
import com.company.Zadanie170TypyGeneryczne.without.Orange;

public class ReturnMethods  {

    public static <T> void method5(T element){
        System.out.println("Wrzucono: "+element.getClass().getSimpleName());
    }

    public static <T> T method6(T element){
        return element;
    }

    public static <T extends Shape> T method7(T shape){
        shape.hello();
        return shape;
    }

    public static <T> T method8(T first, T second){
        System.out.println("Do metody przekazano zmienne typu: ");
        System.out.println(first.getClass().getSimpleName());
        System.out.println("oraz");
        System.out.println(second.getClass().getSimpleName());

        return first;
    }

    public static <T extends Shape, K extends Orange> boolean method9(T first, K second){
        return first.equals(second);
    }


}
