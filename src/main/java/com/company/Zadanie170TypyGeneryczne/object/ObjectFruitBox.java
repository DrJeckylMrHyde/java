package com.company.Zadanie170TypyGeneryczne.object;

public class ObjectFruitBox {

    private Object object;

    public ObjectFruitBox(Object object) {
        this.object = object;
    }

    public Object getFruit() {
        return object;
    }

    public void setFruit(Object object) {
        this.object = object;
    }
}
