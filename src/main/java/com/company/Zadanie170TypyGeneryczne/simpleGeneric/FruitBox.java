package com.company.Zadanie170TypyGeneryczne.simpleGeneric;

/**
 * T - typ
 * K - klucz
 * V - wartosc
 * E - element (kolekcji)
 * N - liczba
 */
public class FruitBox<T> {

    private T fruit;

    public FruitBox(T fruit){
        this.fruit = fruit;
    }

    public T getFruit(){
        return fruit;
    }

    public void setFruit(T fruit){
        this.fruit = fruit;
    }
}
