package com.company.Zadanie170TypyGeneryczne.simpleGeneric;

public class BiggerFruitBox<A,B> {

    private A first;
    private B second;

    public BiggerFruitBox(A first, B second) {
        this.first = first;
        this.second = second;
    }

    public A getFirst(){
        return first;
    }

    public B getSecond(){
        return second;
    }

}


