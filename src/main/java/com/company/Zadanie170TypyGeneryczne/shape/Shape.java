package com.company.Zadanie170TypyGeneryczne.shape;

public interface Shape {

    String getName();

    //implementacja domyslna dla danej metody
    default void hello(){
        System.out.println("Hello, I'm shape!");
    }
}
