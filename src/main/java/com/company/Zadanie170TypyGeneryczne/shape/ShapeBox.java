package com.company.Zadanie170TypyGeneryczne.shape;

/**
 * Klasa opakowująca akceptuje tylko te klasy,
 * które mają relację z klasą Shape
 * (tzn. po niej dziedziczą lub implementują ten interfejs)
 */
public class ShapeBox <T extends Shape> {

    private T shape;

    public ShapeBox(T shape) {
        this.shape = shape;
    }

    public String getNameOfShape(){
        //mam dostep do metod interfejsu po ktorym dziedzicze
        return shape.getName();
    }
}
