package com.company.Zadanie170TypyGeneryczne;

import com.company.Zadanie170TypyGeneryczne.interf.FruitInterface;
import com.company.Zadanie170TypyGeneryczne.interf.FruitInterfaceBox;
import com.company.Zadanie170TypyGeneryczne.methods.ReturnMethods;
import com.company.Zadanie170TypyGeneryczne.methods.VoidMethods;
import com.company.Zadanie170TypyGeneryczne.object.ObjectFruitBox;
import com.company.Zadanie170TypyGeneryczne.shape.*;
import com.company.Zadanie170TypyGeneryczne.simpleGeneric.BiggerFruitBox;
import com.company.Zadanie170TypyGeneryczne.simpleGeneric.FruitBox;
import com.company.Zadanie170TypyGeneryczne.without.Apple;
import com.company.Zadanie170TypyGeneryczne.without.AppleBox;
import com.company.Zadanie170TypyGeneryczne.without.Orange;
import com.company.Zadanie170TypyGeneryczne.without.OrangeBox;

import java.util.ArrayList;
import java.util.List;

public class Zadanie170 {
    public static void main(String[] args) {
/**         dedykowana klasa opakowująca (OrangeBox, AppleBox)
 dla każdej z klas "w środku" (Orange, Apple)
 co powoduje duplikację rozwiązań.
 */
//        method1();
/**         klasa opakowująca akceptuje obiekt klasy Object,
 *          co powoduje zaakceptowanie także obiektów klas
 *          Orange i Apple (które po niej dziedziczą).
 *
 *          Akceptowane są także WSZYSTKIE obiekty klas,
 *          które dziedziczą po Object.
 *
 *          Czyli wewnątrz klasy opakowującej jestesmy w stanie
 *          umiescic ABSOLUTNIE dowolny typ (np. String lub Byte)
 */
//        method2();
/**         Klasa opakowująca akcpetuje tylko  i wyłącznie
 *          obiekty tych klas które implementują interfejs "FruitInterface"
 */
//        method3();
        /**
         *Klasa opakowująca jest klasą generyczną
         * czyli przy deklaracji musimy jawnie podac
         * jaki typ elementow bedzie znajdowac sie w srodku
         */
//        method4();
        /**Klasa opakowująca jest klasą generyczną
         *
         * Przyjmuje dwa obiekty klas które musimy
         * podać przy tworzeniu obiektu, czyli:
         * BiggerFruitBox <String, Integer>
         */
//        method5();
        /**
         * Sprawdzamy, że generyczne klasy są w stanie przyjąć,
         * tylko te obiekty których typ został podany przy deklaracji obiektu
         * List<Integer> a = new ArrayList<>();
         * moge dostac obiekty klasy Integer oraz wszystkich
         * klas dziedziczacych po Integer
         *
         * Nie jesteśmy w stanie dodać obiektów żadnej "nadklasy".
         *
         */
//        method6();
        /**
         * Metody które przyjmują typy generyczne
         */
//        method7();
        /**
         * Metody które w sygnaturze mają typ generyczny
         */
        method8();
    }

    private static void method8() {
        String imie = "Natalia";
        ReturnMethods.method5(imie);
        ReturnMethods.method5(4);
        ReturnMethods.method5(true);

        String napis = ReturnMethods.method6("Pies");
        int liczba = ReturnMethods.method6(4);

        ReturnMethods.method7(new Circle());

        ReturnMethods.method8("Niedziela",false);

        ReturnMethods.method9(new Rectangle(), new Orange());
    }



    private static void method7() {
        ShapeBox<Circle> circleBox = new ShapeBox<>(new Circle());
        ShapeBox<Rectangle> rectangleBox = new ShapeBox<>(new Rectangle());
        ShapeBox<Square> squareBox = new ShapeBox<>(new Square());

        /**
         * Akceptowana klasa jest podana w sygnaturze metody
         */
//        VoidMethods.method1(rectangleBox);
//        VoidMethods.method1(squareBox);
        VoidMethods.method1(circleBox);
        /**
         * Akceptowany jest dowolny Shapebox
         */
        VoidMethods.method2(squareBox);
        VoidMethods.method2(rectangleBox);
        VoidMethods.method2(circleBox);
        /**
         * Akceptowane są klasy które dziedziczą po Rectangle
         */
        VoidMethods.method3(squareBox);
        VoidMethods.method3(rectangleBox);
//        VoidMethods.method3(circleBox);

        /**
         * Akcpetowane są NADKLASY klasy "Rectangle".
         */
        VoidMethods.method4(rectangleBox);
//        VoidMethods.method4(squareBox);
//        VoidMethods.method4(circleBox);

    }

    private static void method6() {

        Rectangle rectangle = new Rectangle();
        ShapeBox<Rectangle> box1 = new ShapeBox<>(rectangle);

//        Wywołujemy metodę na elemencie który jest w środku
        System.out.println(box1.getNameOfShape());

        Circle circle = new Circle();
        ShapeBox<Circle> box2 = new ShapeBox<Circle>(circle);
        System.out.println(box2.getNameOfShape());

//        Operacja prawidłowa
//        Dodajemy element bardziej szczegółówy
//        czyli POSIADA cechy rodzica
        ShapeBox<Rectangle> a = new ShapeBox<>(new Square());

//        Operacja NIE prawidłowa
//        Dodajemy element bardziej ogólny
//        gdy wymagany jest szczegółowy
//        ShapeBox<Square> a = new ShapeBox<>(new Rectangle());
    }

    private static void method5() {
        String imie = "Kuba";
        Integer wiek = 38;

//        Utworzenie obiektu generycznej klasy opakowujacej.
//
//        Elementami w srodku moga byc TYLKO I WYLACZNIE
//        instancje klas (czyli nie akceptujemy "int" ale "Integer"
        BiggerFruitBox<String, Integer> box = new BiggerFruitBox<>(imie, wiek);

//        Wyswietlenie elementów ze srodka
        System.out.println(box.getFirst());
        System.out.println(box.getSecond());

        BiggerFruitBox<String, String> box2 = new BiggerFruitBox<>("Ala", "ma kota");

        System.out.println(box2.getFirst());
        System.out.println(box2.getSecond());

    }

    private static void method4() {
//        Tworzymy nowy obiekt
        Apple apple = new Apple();

//        Utworzenie (generycznej) klasy opakowującej
        FruitBox<Apple> box = new FruitBox<>(apple);
//        Pobranie lementu ze srodka obiektu
        Apple fruit = box.getFruit();

        Orange orange = new Orange();
//        box.setFruit(orange); // BŁĘDNA OPERACJA!!

        FruitBox<Orange> box2 = new FruitBox<>(orange);

        Boolean czyZima = true;
        FruitBox<Boolean> box3 = new FruitBox<>(czyZima);
    }

    private static void method3() {
//        Tworzymy nowy obiekt
        Apple apple = new Apple();

//        Tworzymy nowy obiekt klasy opakowującej
        FruitInterfaceBox box = new FruitInterfaceBox(apple);

//          Wyświetlenie nazwy klasy elementu który znajduje się w środku
        System.out.println(box.getFruit().getClass().getSimpleName());

//        pobranie elementu ze środka
        FruitInterface fruit = box.getFruit();
//        rzutujemy zwrócony obiekt na klasę "Apple"
        Apple appleFruit = (Apple) fruit;

//        String info = "Ala ma kota";
//        box.setFruit(info);  // OPERACJA BŁĘDNA!

//        Zmiana elementu w środku na obiekt klasy Orange
        Orange orange = new Orange();
        box.setFruit(orange);
//        Wyświetlenie nazwy klasy elementu który znajduje się w środku
        System.out.println(box.getFruit().getClass().getSimpleName());

//        Obiekt klasy "Object" NIE JEST AKCEPTOWANY
//        ponieważ nie implementuje wybranego interfejsu.
//        box.setFruit(new Object());
    }

    private static void method2() {
//        tworzymy nowy obiekt
        Apple apple = new Apple();
        ObjectFruitBox box = new ObjectFruitBox(apple);
//        sprawdzam czy obiekt w srodku jest
//        instancja (obiektem) klasy Apple
        System.out.println(box.getFruit() instanceof Apple);

//        tworzymy nowy obiekt
        Orange orange = new Orange();
//          Zmieniamy elemente w srodku
        box.setFruit(orange);
//        Sprawdzamy czy obiekt w srodku jest
//        instancja obiektem klasy Orange
        System.out.println(box.getFruit() instanceof Orange);

//        Pobieram element ze srodka
        Object fruit = box.getFruit();
//        Pobieram nazwe klasy elementu ze srodka
        String className = fruit.getClass().getSimpleName();
        System.out.println(className);

//        Rzutowanie by zwracany obiekt był traktowany jak
//          obiekt klasy Orange
        Orange orangeFruit = (Orange) box.getFruit();

//        tworzymy nową zmienną którą umieścimy w pudelku
        String info = "Ala ma kota";
//        Zmiana elementu w srodku
        box.setFruit(info);
//        Pobieranie elementu ze srodka
        Object fruit2 = box.getFruit();
//        Pobieram nazwe klasy elementu ze srodka
        String className2 = fruit2.getClass().getSimpleName();
        System.out.println(className2);

//        Rzutujemy element ze srodka na klase String
        String info2 = (String) box.getFruit();
        System.out.println(info2);
    }

    private static void method1() {
        Apple apple = new Apple();
        AppleBox box = new AppleBox(apple);
        box.getApple();

        Orange orange = new Orange();
        OrangeBox box2 = new OrangeBox(orange);
    }

}
