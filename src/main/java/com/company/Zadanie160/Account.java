package com.company.Zadanie160;

public class Account {
    private double accountBalance = 0;

    double getBalance() {
        return accountBalance;
    }

    void deposit(double amount){
        accountBalance += amount;
    }

    void withdraw(double amount){
        accountBalance -= amount;
    }
}
