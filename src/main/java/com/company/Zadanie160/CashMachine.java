package com.company.Zadanie160;

public class CashMachine {
    Account acc1 = new Account();
    Verify ver1 = new Verify();

    void withdraw(double amount, String PIN) {
        if (ver1.isPINOk(PIN)) {
            if (acc1.getBalance() >= amount) {
                System.out.println("Balance account before: " + acc1.getBalance());
                acc1.withdraw(amount);
                System.out.println("Balance account before: " + acc1.getBalance());
            } else {
                System.out.println("No money left");
            }
        } else {
            System.out.println("Wrong PIN number");
        }
    }

    void deposit(double amount) {
        System.out.println("Balance account after: " + acc1.getBalance());
        acc1.deposit(amount);
        System.out.println("Balance account after: " + acc1.getBalance());
    }

}
