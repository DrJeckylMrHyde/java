package com.company.Zadanie160;

import java.util.Scanner;


public class Zadanie160 {
    private static final int WITHDRAW = 1;
    private static final int DEPOSIT = 2;
    private static final int BALANCE = 3;

    public static void main(String[] args) {
        CashMachine atm1 = new CashMachine();
        Account acc1 = new Account();
        Verify ver1 = new Verify();
        Scanner userInput = new Scanner(System.in);
        System.out.print("Podaj PIN: ");
        String PIN = userInput.nextLine();
        if(!ver1.isPINOk(PIN)){
            System.out.println("Błędny PIN");
            System.exit(0);
        }


        System.out.println(WITHDRAW + ". withdraw");
        System.out.println(DEPOSIT + ". deposit");
        System.out.println(BALANCE + ". balance");

        int action = userInput.nextInt();
        double money = 0;
        switch (action) {
            case WITHDRAW:
                System.out.print("Podaj kwotę: ");
                money = userInput.nextDouble();
                atm1.withdraw(money,PIN);
                break;
            case DEPOSIT:
                System.out.print("Podaj kwotę: ");
                money = userInput.nextDouble();
                atm1.deposit(money);
                break;
            case BALANCE:
                break;
            default:
                break;
        }


    }
}
