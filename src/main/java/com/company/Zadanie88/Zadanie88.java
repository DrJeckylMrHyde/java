package com.company.Zadanie88;

public class Zadanie88 {
    public static void main(String[] args) {
//        System.out.println(odwracaWyraz("Kotek"));
        System.out.println("->" + odwracaSlowaWWyrazach("Alicja ma kota")+"<-");
    }

    public static String odwracaSlowaWWyrazach(String zdanie) {
        String noweZdanie = "";
        String[] wyrazy = zdanie.split(" ");
        for (int pozycjaWTablicy = 0; pozycjaWTablicy < wyrazy.length; pozycjaWTablicy++) {
            noweZdanie += odwracaWyraz(wyrazy[pozycjaWTablicy]) + " ";
        }

        return noweZdanie.trim();
//        return noweZdanie;
    }

    public static String odwracaWyraz(String wyraz) {
        String nowyWyraz = "";
        for (int pozycjaLitery = 0; pozycjaLitery < wyraz.length(); pozycjaLitery++) {
            // to sluzy dodaniu znaku na poczatku a nie na koncu
            // charAt wyciaga znak z danej pozycji w String
            nowyWyraz = wyraz.charAt(pozycjaLitery) + nowyWyraz;
        }
        return nowyWyraz;
    }


}
