package com.company.Zadanie158Stream;

import com.company.Zadanie97.Osoba;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Zadanie158 {

    private static List<Person> listaOsob = Arrays.asList(
            new Person("Marcin", 24),
            new Person("Marek", 19),
            new Person("Tola", 18),
            new Person("Krzysiek", 42),
            new Person("Bambino", 31),
            new Person("Tadeusz", 27),
            new Person("Dagmara", 27),
            new Person("Stanisław", 36),
            new Person("Wojtek", 17),
            new Person("Antonina", 21)
    );

    public static void main(String[] args) {
//        metoda1();
//        System.out.println();
//        metoda2();
//        System.out.println();
//        metoda3();
//        metoda4();
//        metoda5();
//        metoda6();
//        metoda7();
//        metoda8();
//        metoda9();
//        metoda10();
//        metoda11();
//        metoda12();
//        metoda13();
//        metoda14();
//        metoda15();
//        metoda16();
//        metoda17();
//        metoda18();
//        metoda19();
//        metoda20();
        metoda21();
    }

    private static void metoda21() {
        String zlepek = listaOsob
                .stream()
                .map(osoba -> osoba.getImie())
                //joining laczy elementy, dodatkowo moge nadac rozdzielac
                .collect(Collectors.joining(","));

        System.out.println(zlepek);
    }

    private static void metoda20() {
        Map<Integer, List<Person>> kluczPoWieku = listaOsob
                .stream()
                .collect(Collectors.groupingBy(osoba -> osoba.getWiek()));
        System.out.println(kluczPoWieku);
    }


    private static void metoda19() {
        List<Integer> listaWiek = listaOsob
                .stream()
//                .map(osoba -> osoba.getWiek())
                .map(Person::getWiek)
                .sorted()
                .collect(Collectors.toList());
        System.out.println(listaWiek);
    }



    private static void metoda18() {
        List<String> listaNapisow = Arrays.asList("A1", "A", "C4a");
        boolean info = listaNapisow
                .stream()
                .anyMatch(slowo -> slowo.length() == 2);
        System.out.println(info);

        List<Integer> listaLiczb = Arrays.asList(2, 222, 44, 8, 0);
        boolean info2 = listaLiczb
                .stream()
                .allMatch(liczba -> liczba % 2 == 0);
        System.out.println(info2);

    }

    private static void metoda17() {
        Stream.of("A1", "A2", "B3", "C4", "B3")
                .filter(slowo -> {
                    System.out.println("Teraz filtruje " + slowo);
                    return true;
                })
                .map(slowo -> {
                    System.out.println("Teraz mapuje " + slowo);
                    return slowo;
                })
                .forEach(slowo -> System.out.println("forEach " + slowo));
    }

    private static void metoda16() {
        Stream.of("A1", "A2", "B3", "C4", "B3")
                .map(slowo -> slowo.substring(1))
//                .mapToInt(slowo -> Integer.parseInt(slowo))
                //po parseInt zyskuje dostep do metod statistics
                .mapToInt(Integer::parseInt)
                .max()
                .ifPresent(liczba -> System.out.println(liczba));
    }

    private static void metoda15() {
        List<String> listaNapisow = Arrays.asList("A1", "A2", "C4", "C6", "Q7", "W6", "A1A", "AA");
        List<Integer> listaRzednych = listaNapisow
                .stream()
                .filter(slowo -> slowo.length() == 2)
                .map(slowo -> slowo.substring(1))
//              .map(Integer::parseInt)
//              jesli mam wiecej niz jedno polecenie dodaje {} po ->
                .map(slowo -> {
                    // ctrl + alt + t surround with
                    try {
                        return Integer.parseInt(slowo);
                    } catch (Exception e) {
                        return null;
                    }
                })
                // on tu equals nie obsluzy wartosci null
//                .filter(liczba -> liczba != null)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        System.out.println(listaRzednych);
    }

    private static void metoda14() {
        List<String> listaNapisow = Arrays.asList("Kotek", "Rybka", "", "Słonik", "Waszka G");
        List<Integer> dlugosciSlow = listaNapisow
                .stream()
                .filter(slowo -> !slowo.isEmpty())
                .map(String::length)
                //od tego momentu mam inty
                .collect(Collectors.toList());
        System.out.println(dlugosciSlow);
    }

    private static void metoda13() {
        List<String> listaNapisow = Arrays.asList("Kotek", "Rybka", "", "Słonik", "Waszka G");
        List<String> kolekcjaSlow = listaNapisow
                .stream()
                .filter(slowo -> !slowo.isEmpty())
                .filter(slowo -> slowo.startsWith("R") || slowo.startsWith("K"))
//                .map(slowo -> slowo.toUpperCase())
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println(kolekcjaSlow);
    }

    private static void metoda12() {
        List<Integer> lista = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        IntSummaryStatistics statistics = lista.stream().mapToInt(x -> x).summaryStatistics();
        //szybkie pozyskiwanie kluczowych informacji w zbiorze
        System.out.println("max to: " + statistics.getMax());
        System.out.println("średnia wynosi: " + statistics.getAverage());
        System.out.println("ilość elementów wynosi: " + statistics.getCount());
        System.out.println("min to: " + statistics.getMin());
        System.out.println("suma elementów: " + statistics.getSum());
    }

    private static void metoda11() {
        List<Integer> lista = IntStream
                .rangeClosed(10, 100)
                .filter(liczba -> Math.sqrt(liczba) % 1 == 0)
                .boxed()
                .collect(Collectors.toList());
        System.out.println(lista);
    }

    private static void metoda10() {
        IntStream
                //iterate powoduje ze w tym przypadku ide co druga liczbe od 1
                .iterate(1, liczba -> liczba + 2)
                .limit(5)
                .forEach(liczba -> System.out.print(liczba + " "));
    }

    private static void metoda9() {
        IntStream
                .rangeClosed(10, 20)
                .filter(liczba -> liczba % 2 == 0)
                .average()
                // zrob cos (w tym wypadku wyswietl)
                // jesli mozliwe sa obliczenia
                .ifPresent(liczba -> System.out.println(liczba));
    }

    private static void metoda8() {
        int[] table = new int[20];
        for (int i = 1; i <= table.length; i++) {
            table[i - 1] = i;
        }
        double srednia = Arrays.stream(table).filter(liczba -> {
            if (liczba % 3 == 0) {
                return true;
            }
            return false;
            // srednia
        }).average()
                //tu wrzucam jaka wartosc ma byc zwrocona jesli nie da sie obliczyc
                .orElse(0);
        System.out.println(srednia);
    }

    private static void metoda7() {
        IntStream
                .rangeClosed(1, 20)
                //wybiera liczby po warunku
                .filter(liczba -> liczba % 3 == 0)
                .forEach(liczba -> System.out.print(liczba + ","));
    }

    private static void metoda6() {
        int[] table = new int[]{4, 6, 8, 4};
        Arrays.stream(table)
                .map(liczba -> liczba * 2)
                .forEach(liczba -> System.out.print(liczba + ","));

    }

    private static void metoda1() {
        IntStream
                .range(1, 10)
                //                    System.out.print(liczba);
                .forEach(System.out::print);
    }

    private static void metoda2() {
        IntStream
                //rangeClosed uwzglednia ostatni element w przedziale
                .rangeClosed(1, 10)
                .forEach(System.out::print);
    }

    private static void metoda3() {
        Random r = new Random();
        //ustalam ze chce losowac inty
        //i moge ustalic przedzial losowanych liczb
        //ale bez wlacznie koncowej liczby
        r.ints(1, 10)
                .limit(5)
                .forEach(System.out::println);
    }

    private static void metoda4() {
        List<Integer> list = new Random().ints(1, 5)
                .limit(10)
                //opakowuje wszystkie liczby w typy klasowe
                .boxed()
                //collect jest potrzebne zeby zapisac cos w kolekcji
                .collect(Collectors.toList());

        System.out.println(list);
    }

    private static void metoda5() {
        int[] table = new int[]{3, 5, 7};
        Arrays.stream(table).forEach(liczba -> System.out.print(liczba + ","));
    }


}
