package com.company.Zadanie158Stream;

public class Person {
    private String imie;
    private int wiek;

    public Person(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    public String getImie() {
        return imie;
    }

    public int getWiek() {
        return wiek;
    }

    @Override
    public String toString() {
        return String.format("Imię: %s, wiek: %s",imie,wiek);
    }
}
