package com.company.Zadanie100;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Zadanie100 {
    public static void main(String[] args) {
        List <String> list = new ArrayList<>(Arrays.asList(
                "Ala","ma","kota"
        ));
        //System.out.println(reversedList(list));
        //System.out.println(reversedList2(list));
        System.out.println(reversedList3(list));
    }

    static List<String> reversedList (List<String> entryList){
         Collections.reverse(entryList);
         return entryList;
    }

    static List<String> reversedList2 (List<String> entryList){
        List <String> reversedList = new ArrayList<>();
        for (int i = 0; i < entryList.size(); i++) {
            reversedList.add(0,entryList.get(i));
        }
        return reversedList;
    }
    static List<String> reversedList3 (List<String> entryList){
        List <String> reversedList = new ArrayList<>();

        for (String element : entryList) {
            // metoda lista.add(index, String) umozliwia wstawianie elementu na pierwszym miejscu w liscie
            // dzieki temu one przesuwaja sie o jeden w prawo bez nadpisywania
            reversedList.add(0,element);

        }
        return reversedList;
    }
}
