package com.company.Zadanie167BlokInicjalizacyjny;

class Samochod extends Pojazd {
    static final Integer KOD_KRAJU = 48;

    {
        System.out.println("Blok inicjalizacyjny klasy samochód2");
    }

    static {
        System.out.println("STATYCZNY blok inicjalizacyjny samochodu");
    }

    {
        System.out.println("Blok inicjalizacyjny klasy samochód");
    }

    Samochod() {
        System.out.println("Konstruktor samochodu");
    }
}
