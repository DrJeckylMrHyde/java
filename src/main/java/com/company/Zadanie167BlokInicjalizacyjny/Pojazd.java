package com.company.Zadanie167BlokInicjalizacyjny;

 class Pojazd {
     //blok inicjalizacyjny (instancyjne zwiazane z obiektem, instancja danej klasy) - wywolywane sa PRZED konstruktorem
     // mozemy tu nadac wartosc zmiennej ktora np czesto powtarza sie w konstruktorze
     // bo jest np domyslna
     {
         System.out.println("Blok inicjalizacyjny klasy pojazd");
     }
     //bloki statyczne sa wywolane przy pierwszym uzyciu klasy
     static {
         System.out.println("STATYCZNY blok inicjalizacyjny pojazdu");
     }
     {
         System.out.println("Blok inicjalizacyjny pojazdu 2");
     }

     Pojazd() {
        System.out.println("Konstruktor pojazdu");
    }
}
