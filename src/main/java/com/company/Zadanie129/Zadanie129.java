package com.company.Zadanie129;
//*ZADANIE #129*
//Utwórz metodę która wczytuje liczbę (czyli typ `int`) od użytkownika a następnie ją wyświetla.
// W przypadku podania innej wartości (np. litery), wyświetl użytkownikowi komunikat i pobierz informację ponownie.
// Zaimplementuj to na dwa sposoby (pierwszy - obsługa wyjątku przez metodę pobierającą, drugi - metoda rzucająca wyjątek)

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Zadanie129 {
    public static void main(String[] args) {
        //obsluga wyjatku wewnatrz metody za pomoca try catch
        //wyswietliPodanaWartosc();

        //metoda rzucajaca wyjatek i obsluga w main
        try {
            wyswietlPodanaWartosc2();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void wyswietliPodanaWartosc() {
        Scanner input = new Scanner(System.in);
        while (true) {
            try {
                System.out.print("Podaj liczbę do wyświetlenia ");
                int liczba = input.nextInt();
                System.out.println("Podana liczba to: " + liczba);
                break;
            } catch (InputMismatchException e) {
                System.out.println("Podano znak zamiast liczby");
                input.nextLine();
            }
        }
    }

    private static void wyswietlPodanaWartosc2() throws Exception {
        Scanner input = new Scanner(System.in);
        System.out.print("Podaj liczbę do wyświetlenia ");
        int liczba = input.nextInt();
        System.out.println("Podana liczba to: " + liczba);
    }
}
