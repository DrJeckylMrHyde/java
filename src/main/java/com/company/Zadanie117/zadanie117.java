package com.company.Zadanie117;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class zadanie117 {
    public static void main(String[] args) {
        Map<String, Integer> mapa = new HashMap<>();
        mapa.put("1", 3);
        mapa.put("2", 3);
        mapa.put("3", 4);
        int powtorzenia = liczbaWystapienWMapie(mapa,3);
        System.out.printf("Liczba powtórzeń wartości %s w mapie wynosi %s",3,powtorzenia);
    }

    private static int liczbaWystapienWMapie(Map<String, Integer> mapa, int liczba) {
        int licznik = 0;
        for (Integer wartoscZMapy : mapa.values()) {
            if (wartoscZMapy == liczba) {
                licznik++;
            }
        }
        return licznik;
    }
}
