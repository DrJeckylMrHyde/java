package com.company.Zadanie78;

public class Zadanie78 {
    public static void main(String[] args) {
        Rectangle prostokat = new Rectangle(3,4);
        Rectangle prostokat2 = new Rectangle(3,4);
        Rectangle prostokat3 = new Rectangle(3,5);
        Rectangle prostokat4 = new Rectangle(1,4);
        Rectangle prostokat5 = new Rectangle(6,4);
        System.out.println(prostokat.policzObwod());
        System.out.println(prostokat.policzPole());
        System.out.println(prostokat.getDlugosc());
        System.out.println(Rectangle.czyPolaSaRowne(prostokat, prostokat2));
        Rectangle [] tablica = new Rectangle[] {prostokat, prostokat2, prostokat3, prostokat4, prostokat5};
        for (Rectangle nextRectangle : tablica) {
            nextRectangle.wyswietlPolaProstokatow(tablica);
        }

    }

}
