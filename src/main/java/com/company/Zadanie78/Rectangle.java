package com.company.Zadanie78;

import java.util.Arrays;

public class Rectangle {

    private int dlugosc;
    private int szerokosc;

    public Rectangle(int dlugosc, int szerokosc) {
        this.dlugosc = dlugosc;
        this.szerokosc = szerokosc;
    }

    int policzObwod() {
        return 2 * dlugosc + 2 * szerokosc;
    }

    int policzPole() {
        return dlugosc * szerokosc;
    }

    public int getDlugosc() {
        return dlugosc;
    }

    public int getSzerokosc() {
        return szerokosc;
    }

//    public void setDlugosc(int dlugosc) {
//        this.dlugosc = dlugosc;
//    }

    static boolean czyPolaSaRowne(Rectangle p1, Rectangle p2) {          // metody statyczne sa przywiazane do klase a nie obiektu
        return p1.policzPole() == p2.policzPole();
    }

    void wyswietlPolaProstokatow(Rectangle[] tablica1) {
            System.out.print(dlugosc + " x " + szerokosc + " = " + policzPole() + "\n");
    }
}
