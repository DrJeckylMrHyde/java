package com.company.Zadanie164Obserwator;

import java.util.Scanner;

public class Zadanie164 {
    public static void main(String[] args) {
        Weather weather = new Weather();

        Observer obs1 = new Observer("Tomek", 20, -8);
        Observer obs2 = new Observer("Michał", 18, -10);
        Observer obs3 = new Observer("Kasia", 15, -2);
        Observer obs4 = new Observer("Tosia", 16, -4);

        weather.addObserver(obs1, obs2, obs3);
        weather.addObserver(obs4);


        Scanner input = new Scanner(System.in);
        for (; ; ) {
            System.out.print("Ustaw temperaturę: ");
            int temperature = input.nextInt();
            weather.updateTemp(temperature);
        }

    }
}
