package com.company.Zadanie164Obserwator;

public class Observer {
    private String imie;
    private int maxTemp;
    private int minTemp;
//    private final int crisisTemp = 100;

    Observer(String imie, int maxTemp, int minTemp) {
        this.imie = imie;
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
    }

    public String getImie() {

        return imie;
    }

    public int getMaxTemp() {

        return maxTemp;
    }

    public int getMinTemp() {

        return minTemp;
    }

    void react(int currentTemp) {
        String observerReact = String.format("%s twierdzi, że %s to %%s. %%s %s nie wyjdę z domu\n",
                imie,
                currentTemp,
                maxTemp);
        if (currentTemp > maxTemp) {
            System.out.printf(observerReact,"za ciepło","powyżej");
        } else if (currentTemp < minTemp) {
            System.out.printf(observerReact,"za zimno","poniżej");
        }
    }
}
