package com.company.Zadanie164Obserwator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Weather {
    private List<Observer> listaObserwatorow = new ArrayList<>();
    private int currentTemp;

    void addObserver(Observer observer){
        listaObserwatorow.add(observer);
    }

    void addObserver(Observer ... observers){
        listaObserwatorow.addAll(Arrays.asList(observers));
    }

    void updateTemp(int newTemp){
        currentTemp = newTemp;
        notifyObserver();
    }


    //powiadomienie obserwatora o zmianie temperatury
    void notifyObserver() {
//        for (Observer observer : listaObserwatorow) {
//            observer.react(currentTemp);
//        }

        //metoda tutaj iteruje po kazdym obserwatorze w liscie
        listaObserwatorow.forEach(observer -> observer.react(currentTemp));
    }


}
