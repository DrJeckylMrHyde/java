package com.company.Zadanie151;

public class Person {

    public static void main(String[] args) {
        Person p = new Person();
        p.setWiek(4);
        p.setNazwisko("Wegner");
        p.setImie("Marcin");
        System.out.println(p.getWiek());
        System.out.println(p.utworzLogin());
    }

    private String imie;
    private String nazwisko;
    private int wiek;
    private boolean czyMezczyzna;

    public Person() {

    }

    public Person(String imie, String nazwisko, int wiek, boolean czyMezczyzna) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.czyMezczyzna = czyMezczyzna;
    }

    public String pobierzPelneDane() {
        if (nazwisko == null && imie == null) {
            return null;
        } else if (nazwisko == null) {
            return imie;
        } else if (imie == null) {
            return nazwisko;
        } else {
            return imie + " " + nazwisko;
        }
    }

    public boolean czyOsobaJestPelnoletnia() {
        return wiek > 17;
    }

    public int ileLatDoEmerytury() {
        if (wiek == 0) {
            return -1;
        }

        int wyliczonyWiekDoEmerytury = (czyMezczyzna ? 67 : 65) - wiek;

        return wyliczonyWiekDoEmerytury > 0 ? wyliczonyWiekDoEmerytury : 0;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getWiek() {
        return wiek;
    }

    public boolean isCzyMezczyzna() {
        return czyMezczyzna;
    }


    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setWiek(int wiek) {
        if (wiek > 0) {
            this.wiek = wiek;
        }
    }

    public void setCzyMezczyzna(boolean czyMezczyzna) {
        this.czyMezczyzna = czyMezczyzna;
    }

    public String utworzLogin() {
        if (imie == null && nazwisko == null) {
            return null;
        } else if (imie == null) {
            return "XXX" + nazwisko.toLowerCase().substring(0, 3) + nazwisko.length();
        } else if (nazwisko == null) {
            return imie.toLowerCase().substring(0, 3) + "YYY" + imie.length();
        } else if (imie.length() < 3) {
            return imie.toLowerCase().substring(0, 2) + "Z" + nazwisko.toLowerCase().substring(0, 3) + (imie + nazwisko).length();
        } else if (nazwisko.length() < 3) {
            return imie.toLowerCase().substring(0, 3) + nazwisko.toLowerCase().substring(0, 2) + "Z" + (imie + nazwisko).length();
        } else {
            return imie.toLowerCase().substring(0, 3) + nazwisko.toLowerCase().substring(0, 3) + (imie + nazwisko).length();
        }
    }

}
