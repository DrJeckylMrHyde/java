package com.company.Zadanie123Interface;

public interface Obliczenia {
    //interfejsy to lista metod
    double policzPole();
    double policzObwod();
    double policzObjetosc(int wysokosc);
}
