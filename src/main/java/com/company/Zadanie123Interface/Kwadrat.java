package com.company.Zadanie123Interface;

public class Kwadrat extends Prostokat {

    public Kwadrat(int dlugosc) {
        super(dlugosc, dlugosc);
    }



    @Override
    String wyswietlInformacjeOSobie() {
        return String.format("Jestem kwadratem o boku %s",
                dlugosc);
    }
}
