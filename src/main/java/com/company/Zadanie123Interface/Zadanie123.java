package com.company.Zadanie123Interface;

import java.util.ArrayList;
import java.util.List;

public class Zadanie123 {
    public static void main(String[] args) {
        Prostokat p1 = new Prostokat(3, 4);
        Kwadrat k1 = new Kwadrat(3);
        Kolo ko1 = new Kolo(3);
//        System.out.println(p1.wyswietlInformacjeOSobie());
//        System.out.println(k1.wyswietlInformacjeOSobie());
//        System.out.println(ko1.wyswietlInformacjeOSobie());

        List<Ksztalt> listaFigur = new ArrayList<>();
        listaFigur.add(p1);
        listaFigur.add(k1);
        listaFigur.add(ko1);

        for (Ksztalt ksztalt : listaFigur) {
            System.out.println(ksztalt.wyswietlInformacjeOSobie());
            System.out.printf("Pole wynosi: %.2f\n", ksztalt.policzPole());
        }
    }
}
