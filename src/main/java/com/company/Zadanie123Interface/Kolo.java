package com.company.Zadanie123Interface;

public class Kolo extends Ksztalt {
    private int promien;
    public Kolo(int promien) {
        this.promien = promien;
    }

    @Override
    public double policzPole() {
        return 2*Math.PI*Math.pow(promien,2);
    }

    @Override
    public double policzObwod() {
        return 2*Math.PI*promien;
    }

    @Override
    public double policzObjetosc(int wysokosc) {
        return 4/3*Math.PI*Math.pow(promien,3);
    }

    @Override
    String wyswietlInformacjeOSobie() {
        return String.format("Jestem kołem i mój promień wynosi %s",
                promien);
    }

}
