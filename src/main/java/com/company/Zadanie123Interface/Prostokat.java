package com.company.Zadanie123Interface;

public class Prostokat extends Ksztalt{
    protected int dlugosc;
    private int szerokosc;

    public Prostokat(int dlugosc, int szerokosc) {
        this.dlugosc = dlugosc;
        this.szerokosc = szerokosc;
    }

    @Override
    public double policzPole() {
        return dlugosc*szerokosc;
    }

    @Override
    public double policzObwod() {
        return 2*dlugosc + 2*szerokosc;
    }

    @Override
    public double policzObjetosc(int wysokosc) {
        return policzPole()*wysokosc;
    }

    @Override
    String wyswietlInformacjeOSobie() {
        return String.format("Jestem prostokątem moje boki mają długość %s oraz %s",
                dlugosc,
                szerokosc);
    }
}
