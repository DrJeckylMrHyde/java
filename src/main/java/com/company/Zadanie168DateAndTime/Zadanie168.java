package com.company.Zadanie168DateAndTime;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.Scanner;
import java.util.Set;

public class Zadanie168 {
    public static void main(String[] args) {
/**        LOCAL TIME */
//        method1();
//        method2();
//        method3();
//        method4();
//        method5();
/**        LOCAL DATE */
//        method6();
//        method7();
//        method8();
//        method9();
//        method10();
/**        LOCAL DATE TIME - obsluga daty i czasu */
//        method11();
//        method12();
        /** ZONED DATE TIME*/
//        method13();
//        method14();
//        method15();
/**         PERION & DURATION */
//        method16();
//        method17();
        method18();
    }

    private static void method18() {
        String firstDateTime = "10.05.2018 13:20";
        String secondDateTime = "28.11.2018 19:44";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy HH:mm");

        LocalDateTime first = LocalDateTime.parse(firstDateTime, formatter);
        LocalDateTime second = LocalDateTime.parse(secondDateTime, formatter);

        long months = ChronoUnit.MONTHS.between(first, second);
        long weeks = ChronoUnit.WEEKS.between(first, second);
        long days = ChronoUnit.DAYS.between(first, second);
        long hours = ChronoUnit.HOURS.between(first, second);
        long minutes = ChronoUnit.MINUTES.between(first, second);
        long seconds = ChronoUnit.SECONDS.between(first, second);

        System.out.printf("Pomiędzy %s a %s różnica wynosi:\n", firstDateTime, secondDateTime);
        System.out.println(months + " miesięcy");
        System.out.println(weeks + " tygodni");
        System.out.println(days + " dni");
        System.out.println(hours + " godzin");
        System.out.println(minutes + " minut");
        System.out.println(seconds + " sekund");
    }

    private static void method17() {
        String firstDate = "10.05.2018";
        String secondDate = "23.12.2018";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate firstLocalDate = LocalDate.parse(firstDate, formatter);
        LocalDate secondLocalDate = LocalDate.parse(secondDate, formatter);

        Period period = Period.between(firstLocalDate, secondLocalDate);
        System.out.printf("Pomiędzy %s a %s różnica wynosi:\n", firstDate, secondDate);
        System.out.println(period.getDays() + " dni");
        System.out.println(period.getMonths() + " miesięcy");
        System.out.println(period.getYears() + " lat");
    }

    private static void method16() {
        String firstTime = "11:10";
        String secondTime = "16:20";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

        LocalTime firstLocalTime = LocalTime.parse(firstTime, formatter);
        LocalTime secondLocalTime = LocalTime.parse(secondTime, formatter);

        Duration duration = Duration.between(firstLocalTime, secondLocalTime);
        System.out.printf("Pomiędzy %s a %s różnica wynosi:\n", firstTime, secondTime);
        System.out.println(duration.toHours() + " godzin");
        System.out.println(duration.toMinutes() + " minut");
        System.out.println(duration.getSeconds() + " sekund");

    }

    private static void method15() {
        Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();
        availableZoneIds.stream()
                .sorted()
                .forEach(System.out::println);
    }

    private static void method14() {
        LocalDateTime inputLocalDateTime = LocalDateTime.now();

        ZoneId inputZone = ZoneId.of("Europe/Warsaw");

        ZoneId outputZone = ZoneId.of("Europe/Moscow");

        LocalDateTime outputLocalDateTime = inputLocalDateTime.atZone(inputZone)
                .withZoneSameInstant(outputZone)
                .toLocalDateTime();

        System.out.println(inputLocalDateTime);
        System.out.println(outputLocalDateTime);
    }

    private static void method13() {
        LocalDateTime inputDate = LocalDateTime.now();

        ZonedDateTime zonedDateTime = ZonedDateTime.of(inputDate, ZoneOffset.UTC);

        ZoneId zoneId = ZoneId.of("Europe/Warsaw");

        ZonedDateTime outputZonedDateTime = zonedDateTime.withZoneSameInstant(zoneId);

        LocalDateTime dateTime = outputZonedDateTime.toLocalDateTime();

        System.out.println(inputDate);
        System.out.println(dateTime);
    }

    private static void method12() {
        String inputDate = "12.07.2018 11:22";

        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(inputDate, inputFormatter);
        System.out.println(dateTime);

        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy (D 'dzień roku'), HH:mm ");
        System.out.println(dateTime.format(outputFormatter));
    }

    private static void method11() {
        LocalDateTime dateTime = LocalDateTime.of(2018, Month.DECEMBER, 1, 14, 39, 05);
        System.out.println(dateTime.toString());

        DayOfWeek dayOfWeek = dateTime.getDayOfWeek();
        System.out.println(dayOfWeek);
        System.out.println(dayOfWeek.getValue());

//        Przesuniecie dnia o 1 dzien
        DayOfWeek plus = dayOfWeek.plus(1);
        System.out.println(plus);

        Month month = dateTime.getMonth();
        System.out.println(month);
        System.out.println(month.getValue());

        int minuteOfDay = dateTime.get(ChronoField.MINUTE_OF_DAY);
        System.out.println(minuteOfDay);

        long nanoOfDay = dateTime.getLong(ChronoField.NANO_OF_DAY);
        System.out.println(nanoOfDay);


    }

    private static void method10() {
        String inputDate = "4/8/2018";

        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("d/M/yyyy");

        LocalDate date = LocalDate.parse(inputDate, inputFormat);
        System.out.println("ISO_DATE -> " + date.format(DateTimeFormatter.ISO_DATE));
        System.out.println("BASIC_ISO_DATE -> " + date.format(DateTimeFormatter.BASIC_ISO_DATE));
        System.out.println("ISO_ORDINAL_DATE -> " + date.format(DateTimeFormatter.ISO_ORDINAL_DATE));
        System.out.println("ISO_WEEK_DATE -> " + date.format(DateTimeFormatter.ISO_WEEK_DATE));
        System.out.println("ISO_LOCAL_DATE -> " + date.format(DateTimeFormatter.ISO_LOCAL_DATE));
    }

    private static void method9() {
        String inputDate = "01/12/2018";

        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        LocalDate date = LocalDate.parse(inputDate, inputFormatter);
//        LocalDate date = LocalDate.now();

        int dayOfYear = date.get(ChronoField.DAY_OF_YEAR);
        System.out.println(dayOfYear);

        DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("D");
        System.out.println(date.format(outputFormat));

        System.out.println(date.getDayOfYear());
    }

    private static void method8() {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj format daty: ");
        String pattern = input.nextLine();

        try {
            LocalDate date = LocalDate.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            System.out.println("Wynik: " + date.format(formatter));
        } catch (UnsupportedTemporalTypeException e) {
            System.out.println("Gratulacje! Zły wzorzec");
        } catch (IllegalArgumentException e) {
            System.out.println("Za dużo znaków we wzorcu");
        }
    }

    private static void method7() {
        LocalDate date = LocalDate.of(2018, 12, 1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        System.out.println(date.format(formatter));

        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyy");
        System.out.println(date.format(formatter2));

        DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("MM");
        System.out.println(date.format(formatter3));

        DateTimeFormatter formatter4 = DateTimeFormatter.ofPattern("LLLL");
        System.out.println(date.format(formatter4));

        DateTimeFormatter formatter5 = DateTimeFormatter.ofPattern("EEE");
        System.out.println(date.format(formatter5));

        DateTimeFormatter formatter6 = DateTimeFormatter.ofPattern("d MMMM yyyy (EEEE)");
        System.out.println(date.format(formatter6));
    }

    private static void method6() {
        LocalDate date = LocalDate.of(2018, Month.DECEMBER, 1);
        System.out.println(date.toString());
    }

    private static void method5() {
        String inputTime = "13:05:33";
        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime t1 = LocalTime.parse(inputTime, inputFormat);
        DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("HH_mm_ss");

        String output = t1.format(outputFormat);
        System.out.println(output);
    }

    private static void method4() {
        String currentTime = "13+51--28";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH+mm--ss");
        LocalTime t1 = LocalTime.parse(currentTime, formatter);
        System.out.println(t1);


        //POLE powiazane z czasem
        int hour = t1.get(ChronoField.HOUR_OF_DAY);
        int minute = t1.get(ChronoField.MINUTE_OF_HOUR);
        System.out.printf("Mamy %s min. po godzinie %s-tej\n",
                minute
                , hour);
    }

    private static void method3() {
        LocalTime t1 = LocalTime.now();
        System.out.println(t1);
    }

    private static void method2() {
        LocalTime t1 = LocalTime.parse("12:43:25");
        System.out.println(t1);
        System.out.println(t1.getHour());
        System.out.println(t1.getMinute());
        System.out.println(t1.getSecond());
        System.out.println(t1.getNano());
    }

    private static void method1() {
        LocalTime t1 = LocalTime.of(12, 36, 3);
        System.out.println(t1.getHour());
        System.out.printf("Godzina: %s, minuta: %s, sekundy: %s",
                t1.getHour(),
                t1.getMinute(),
                t1.getSecond());
    }
}