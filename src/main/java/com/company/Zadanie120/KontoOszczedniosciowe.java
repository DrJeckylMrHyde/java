package com.company.Zadanie120;

public class KontoOszczedniosciowe extends Konto {
    private double procentOszczedniosciowy;

    public KontoOszczedniosciowe(String wlasciciel, double procentOszczedniosciowy) {
        super(wlasciciel);
        this.procentOszczedniosciowy = procentOszczedniosciowy;
    }

    void kapitalizacja(int iloscMiesiecy) {
        for (int i = 1; i <= iloscMiesiecy; i++) {
            stan = stan + stan * procentOszczedniosciowy;
        }
    }

    @Override
    public String toString() {
        return super.toString() + " .Konto Oszczednościowe";
    }
}
