package com.company.Zadanie120;

public class KontoFirmowe extends Konto{
    double oplataZaTransakcje;

    public KontoFirmowe(String wlasciciel, double oplataZaTransakcje) {
        super(wlasciciel);
        this.oplataZaTransakcje = oplataZaTransakcje;
    }

    @Override
    public String toString() {
        return super.toString()+"";
    }

    @Override
    void deposit(double kwotaWplaty) {
        super.deposit(kwotaWplaty);
    }

    @Override
    boolean withdraw(double kwotaWyplaty) {
        //stan jest z klasy Konto pole stan musi byc protected
        double kwotaTransakcji = kwotaWyplaty + oplataZaTransakcje*kwotaWyplaty;
        if (stan >= kwotaTransakcji) {
            stan -= kwotaTransakcji;
            return true;
        }
        return false;
    }

}
