package com.company.Zadanie120;

public class Konto {

    protected double stan;
    private String wlasciciel;
    private int numerKonta;
    //to pole przechowuje swoja wartosc, uzywamy do zliczania utworzonych kont, NIE uzywac do innych obliczen
    private static int identyfikatorKonta = 0;


    public Konto(String wlasciciel) {
        this.wlasciciel = wlasciciel;
        numerKonta = identyfikatorKonta++;
    }

    @Override
    public String toString() {
        return String.format("Właścicielem konta o id %s jest %s. Aktualny stan konta wynosi %s", numerKonta, wlasciciel, stan);
    }

    public double getStanKonta() {
        return stan;
    }

    public int getNumerKonta() {
        return numerKonta;
    }

    void deposit(double kwotaWplaty) {
        stan += kwotaWplaty;
    }

    boolean withdraw(double kwotaWyplaty) {
        if (stan >= kwotaWyplaty) {
            stan -= kwotaWyplaty;
            return true;
        }
        return false;
    }

    static void transfer(Konto nadawcy, Konto odbiorcy, double kwotaPrzelewu) {
        if (nadawcy.withdraw(kwotaPrzelewu)) {
            odbiorcy.deposit(kwotaPrzelewu);
        }
    }


}
