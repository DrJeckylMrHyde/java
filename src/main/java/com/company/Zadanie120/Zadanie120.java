package com.company.Zadanie120;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie120 {
    public static void main(String[] args) {
        Konto konto1 = new Konto("Marian Kowalski");
        Konto konto2 = new Konto("Mariusz Nowak");
        Konto konto3 = new Konto("Kamil Nowak");
        //System.out.println(konto1);
        //System.out.println(konto2);
        //System.out.println(konto3);
        konto1.deposit(40);
        //System.out.println(konto1);
        konto1.withdraw(10);
        //System.out.println(konto1);

        //metody statyczne wywoluje na klasie!!!
        Konto.transfer(konto1, konto2, 25);
        //System.out.println(konto1);
        //System.out.println(konto2);

        KontoFirmowe kontoKW = new KontoFirmowe("Kamil Nowak", 0.02);
        kontoKW.deposit(50);
        kontoKW.withdraw(25);
        //System.out.println(kontoKW);

        KontoOszczedniosciowe kontoOs = new KontoOszczedniosciowe("Kamil Nowak", 0.03);
        kontoOs.deposit(30);
        kontoOs.kapitalizacja(12);
        //System.out.println(kontoOs);

        List<Konto> listaKont = Arrays.asList(
                konto1,
                konto2,
                konto3,
                kontoKW,
                kontoOs);
        for (Konto kontoZListy : listaKont) {
            //operator instanceof sprawdza czy obiekt jest obiektem danej klasy
            if (kontoZListy instanceof KontoFirmowe) {
                System.out.print("Konto firmowe ");
            } else if (kontoZListy instanceof KontoOszczedniosciowe) {
                System.out.print("Konto oszczędnościowe ");
            } else {
                System.out.print("Konto osobiste");
            }
            System.out.printf("(%s) %s\n",kontoZListy.getNumerKonta(),kontoZListy.stan);
        }
    }
}
