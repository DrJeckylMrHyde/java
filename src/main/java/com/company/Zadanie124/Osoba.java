package com.company.Zadanie124;

public class Osoba implements PrzedstawSie {
    private String imie;
    private String nazwisko;
    private double wiek;
    private Plec plec;

    public Osoba(String imie, String nazwisko, double wiek, Plec plec) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.plec = plec;
    }

    @Override
    public String zwrocPelneDane() {
        return String.format("Nazywam się %s %s mam, %s lat",
                imie,
                nazwisko,
                wiek);
    }

    @Override
    public String zwrocImie() {
        return String.format("Mam na imię %s", imie);
    }

    @Override
    public double zwrocWiekDoEmerytury() {
        return (plec.equals(Plec.MEZCZYZNA) ? 67 : 65) - wiek;
    }

    @Override
    public String toString() {
        return zwrocPelneDane();
    }

    enum Plec {
        MEZCZYZNA, KOBIETA
    }
}
