package com.company.Zadanie124;

public interface PrzedstawSie {

    String zwrocPelneDane();
    String zwrocImie();
    double zwrocWiekDoEmerytury();
}
