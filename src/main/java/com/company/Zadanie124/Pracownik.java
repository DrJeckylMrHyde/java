package com.company.Zadanie124;

public class Pracownik extends Osoba {
    private Double pensja;

    public Pracownik(String imie, String nazwisko, double wiek, Plec plec) {
        super(imie, nazwisko, wiek, plec);
    }

    public void ustawPensje(Double pensja) {
        this.pensja = pensja;
    }

    @Override
    public String zwrocPelneDane() {
        if (pensja != null) {
            return super.zwrocPelneDane() + " moja pensja wynosi " + pensja + ", do emerytury zostało mi " + zwrocWiekDoEmerytury() + " lat";
        } else {
            return super.zwrocPelneDane() + ", do emerytury zostało mi " + zwrocWiekDoEmerytury() + " lat";
        }
    }


}
