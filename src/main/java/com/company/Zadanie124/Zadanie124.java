package com.company.Zadanie124;
import static com.company.Zadanie124.Osoba.Plec.MEZCZYZNA;
import static com.company.Zadanie124.Osoba.Plec.KOBIETA;
public class Zadanie124 {
    public static void main(String[] args) {
        Osoba o1 = new Osoba("Marcin","Wegner",24,MEZCZYZNA);
        System.out.println(o1);
        Pracownik p1 = new Pracownik("Jan","Kowalski",47, MEZCZYZNA);
        Pracownik p2 = new Pracownik("Anna","Kowalski",27, KOBIETA);
        System.out.println(p1);
        System.out.println(p2);
        p1.ustawPensje(8700.0);
        p2.ustawPensje(6700.0);
        System.out.println(p1);
        System.out.println(p2);

    }
}
