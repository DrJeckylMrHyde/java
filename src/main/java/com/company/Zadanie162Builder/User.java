package com.company.Zadanie162Builder;

public class User {
    //pola klasy zewn sa takie same co buildera
    private String name;
    private String surname;
    private int age;
    private boolean isMan;

    //przepisuje pola z builder do pol user
    private User(UserBuilder userBuilder) {
        name = userBuilder.name;
        surname = userBuilder.surname;
        age = userBuilder.age;
        isMan = userBuilder.isMan;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", isMen=" + (isMan ? "Wiadomka" : "Nie dzisiaj") +
                '}';
    }

    //sluza dla klas wewn klasy w klasie
    //moge utworzyc obiekt klasy wewn bez tworzenia klasy zewn
    static class UserBuilder {
        private String name = "brak";
        private String surname = "brak";
        private int age;
        private boolean isMan;

        UserBuilder setName(String name) {
            this.name = name;
            //zwrocenie obiektu na ktorym pracuje
            return this;
        }

        UserBuilder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        UserBuilder setAge(int age) {
            this.age = age;
            return this;
        }

        UserBuilder setGender(boolean isMan){
            this.isMan = isMan;
            return this;
        }

        User build() {

            return new User(this);
        }


    }
}
