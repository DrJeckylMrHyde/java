package com.company.Zadanie162Builder;

public class Zadanie162 {
    public static void main(String[] args) {
    User u1 = new User.UserBuilder()
            .setName("Marek")
            .setSurname("Mostowiak")
            .setAge(37)
            .setGender(true)
            .build();

    User u2 = new User.UserBuilder()
            .setName("Hanka")
            .setSurname("Mostowiak")
            .setAge(32)
            .setGender(false)
            .build();

    User u3 = new User.UserBuilder()
            .setSurname("Kowalski")
            .setAge(27)
            .setGender(true)
            .build();

        System.out.println(u1);
        System.out.println(u2);
        System.out.println(u3);
    }
}
