package com.company.ZadaniaRekrutacyjne;

public class SquareRoot {

    public static void main(String[] args) {

        System.out.println("" + solution(10, 20));
        System.out.println("" + solution(6000, 5000000000L));
    }

    private static int solution(int start, long end) {
        int maxCounter = 0;
        int counter;
        for (int i = start; i <= end; i++) {
            counter = -1;
            double liczba = i;
            while (czyCalkowita(liczba)) {
                liczba = Math.sqrt(liczba);
                counter++;
            }
            if (counter > maxCounter) {
                maxCounter = counter;
            }
        }
        return maxCounter;
    }

    private static boolean czyCalkowita(double liczba) {

        return liczba % 1 == 0;
    }

    private static int pierwiastkuj(double liczba) {
        int counter = -1;
        while (czyCalkowita(liczba)) {
            liczba = Math.sqrt(liczba);
            counter++;
        }
        return counter;
    }

}