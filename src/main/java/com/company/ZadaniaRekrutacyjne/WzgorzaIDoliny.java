package com.company.ZadaniaRekrutacyjne;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WzgorzaIDoliny {

    public static void main(String[] args) {
        //dlugosc = 12
        //int[] table = new int[]{2,2,3,4,3,3,2,2,1,1,2,5};
        int [] table = new int []{1,2,1,2,2,1,1,3};
//        int [] table = new int []{3,2,2,1};
        System.out.println(solution(table));
        //System.out.println(Arrays.toString(tenSamPoziom(table)));
    }

    private static int solution(int[] table) {
        int counter = 0;
        for (int i = 0; i < table.length-1; i++) {
            if(table[i] == table[i+1]){
                counter++;
            }
        }
        if(counter == table.length-1){
            return 1;
        }
        counter = 0;
        int[] lista = new int[2];
        for (int i = 0; i < table.length; i++) {
            lista[0] = i;
                for (int j = i + 1; j < table.length; j++) {
                    if (table[i] == table[j]) {
                        lista[1] = j;
                        break;
                    } else if (table[i] != table[j]) {
                        lista[1] = lista[0];
                        break;
                    }
                }
            //wzgorze
            if (((lista[0] > 0) && (lista[1] < (table.length - 1)) && (table[lista[0]] > table[lista[0] - 1]) && (table[lista[1]] > table[lista[1] + 1])) ||
                    //dolina
                    ((lista[0] > 0) && (lista[1] < (table.length - 1)) && (table[lista[0]] < table[lista[0] - 1]) && (table[lista[1]] < table[lista[1] + 1])) ||
                    (lista[0] == 0) || (lista[0] == (table.length - 1))) {
                counter++;
            }
//            System.out.println(Arrays.toString(lista));
//            System.out.println(counter);
        }

        return counter;
    }
}
