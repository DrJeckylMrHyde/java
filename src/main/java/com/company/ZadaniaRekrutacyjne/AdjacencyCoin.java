package com.company.ZadaniaRekrutacyjne;

import java.util.Arrays;
import java.util.Random;

public class AdjacencyCoin {
    public static void main(String[] args) {
//        Random generator = new Random();
//        int[] table = new int[1000];
//        for (int i = 0; i < table.length; i++) {
//            table[i] = generator.nextInt(2);
//        }
//        System.out.println(Arrays.toString(table));
        int[] table = new int[]{0, 0, 0, 0, 0};
        System.out.println(solution(table));
    }

    private static int solution(int[] A) {
        int n = A.length;
        int result = 0;
        for (int i = 0; i < n - 1; i++) {
            if (A[i] == A[i + 1]) {
                result = result + 1;
            }
            //System.out.println(result);
        }
        if (result == A.length - 1) {
            return result - 1;
        }
        int r = 0;
        for (int i = 0; i < n; i++) {
            int count = 0;
            if (i > 0) {
                if (A[i - 1] != A[i]) {
                    count = count + 1;
                } else {
                    count = count - 1;
                }
            }
            if (i < n - 1) {
                if (A[i + 1] != A[i]) {
                    count = count + 1;
                } else {
                    count = count - 1;
                }
            }
            r = Math.max(r, count);
//            System.out.println("count " + count);
//            System.out.println("r " + r);
        }
        return result + r;
    }
}

