package com.company.Zadanie106;

import java.util.*;

public class Zadanie106 {
    public static void main(String[] args) {
//        Set<Integer> secik = new HashSet<>(Arrays.asList(1,2,1,4,15,23,1,3,12));
//        System.out.println(secik);
        System.out.println(zwracaUnikaty());
    }

    private static Set<Integer> zwracaUnikaty() {
        Set<Integer> unikaty = new HashSet<>();
        Scanner liczbaWprowadzona = new Scanner(System.in);
        int liczba;
        for (; ; ) {
            System.out.println("Podaj liczbę: ");
            liczba = liczbaWprowadzona.nextInt();
            //HashSet sam z siebie przyjmuje tylko unikaty
            if(!unikaty.add(liczba)){
                break;
            }
                unikaty.add(liczba);
        }
        return unikaty;
    }
}
