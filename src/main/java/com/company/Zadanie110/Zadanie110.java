package com.company.Zadanie110;

import java.util.HashMap;
import java.util.Map;

public class Zadanie110 {
    public static void main(String[] args) {

        Map<String, String> map = zwracaMapeZeSkrotamiPanstw();
        System.out.println(map);

        Map<Integer, String> mapDialNum = zwracaMapeZKierunkowym();
        System.out.println(mapDialNum);

        System.out.println(zwracaKrajOZadanymKierunkowym(mapDialNum,48));
    }

    private static Map<String,String> zwracaMapeZeSkrotamiPanstw (){
        Map<String,String> mapka = new HashMap<>();
        mapka.put("PL "," Polska");
        mapka.put("DE "," Niemcy");
        mapka.put("ES "," Hiszpania");
        return mapka;
    }

    private static Map<Integer,String> zwracaMapeZKierunkowym (){
        //KLUCZE mapy są unikalne zdublowanie powoduje nadpisanie klucza
        //WARTOSCI MOGA SIE DUPLIKOWAC
        Map<Integer,String> mapka = new HashMap<>();
        mapka.put(48," Polska");
        mapka.put(47," Niemcy");
        mapka.put(34," Hiszpania");
        return mapka;
    }

    private static String zwracaKrajOZadanymKierunkowym (Map<Integer,String> mapka,Integer kierunkowy){

        return mapka.get(kierunkowy);
    }


}
