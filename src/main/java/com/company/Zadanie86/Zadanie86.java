package com.company.Zadanie86;

public class Zadanie86 {
    public static void main(String[] args) {
        System.out.println(zwrociRok("12/05/2018", "/"));
        System.out.println(zwrociRok2("12/05/2018"));
//        System.out.println(zwrociRok("12.05.2018", "."));
    }

    public static String zwrociRok(String data, String separator) {
        String[] tablica = data.split(separator);
        return tablica[tablica.length - 1];
    }

    public static String zwrociRok2(String data) {
        return zwrociRok(data, "/");
    }
}
