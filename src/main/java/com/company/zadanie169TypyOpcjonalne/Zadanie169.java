package com.company.zadanie169TypyOpcjonalne;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


//typy opcjonalne to nowowsc od JAVY 8 pozwala po wielu operacjach np na streamach uniknac nulla na koncu
public class Zadanie169 {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
        method4();
    }

    private static void method4() {
        List<Integer> list = Arrays.asList(
                1, 6, 8, 20, 70, 2, 5, 22
        );

        Optional<Integer> first = list.stream()
                .filter(number -> number > 100)
                .findFirst();

        Integer second = list.stream()
                .filter(number -> number > 100)
                .findFirst()
                .orElse(0);

        if (first.isPresent()) {
            System.out.println("Tak znaleziono liczbę: " + first.get());
        } else {
            System.out.println("Żadna liczba nie spełnia warunku.");
        }


    }

    private static void method3() {
        Optional<Double> result = safeDivision(2, 1);
        System.out.println(result.isPresent());
        double wynik = result.orElse(0D);
        System.out.println(wynik);

        Optional<Double> result2 = safeDivision(7, 0);
        System.out.println(result.isPresent());
        double wynik2 = result2.orElse(0D);
        System.out.println(wynik2);

    }

    private static Optional<Double> safeDivision(double first, double second) {
        if (second == 0) {
            return Optional.empty();
        } else {
            return Optional.of(first / second);
        }
    }

    private static void method2() {
        Car car = new Car();
//        typ opcjonalny powoduje ze zawsze dostaje wartosc nigdy nie dostaje nulla
        Optional<Integer> age = car.getAge();
//        jesli nie dostalem wartosci przyjmij w tym przypadku 0
        int realAge = age.orElse(0);

//        sprawdzam czy jakas wartosc (rozna od null) jest w srodku
        if (age.isPresent()) {
            int real = age.get();
        }
    }

    private static void method1() {
        Car car = new Car();
        System.out.println(car.getName());

        if (car.getName() != null) {
            String carName = car.getName().toUpperCase();
        }
    }
}
