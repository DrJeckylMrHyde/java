package com.company.Zadanie95;

enum TypWaluty {

    USD("dolar", "USA"), EUR("euro", "UE"), PLN("złoty", "Polska");

    private String opis;
    private String kraj;

    TypWaluty(String opis, String kraj) {

        this.opis = opis;
        this.kraj = kraj;
    }

    String getOpis() {
        return opis;
    }

    String getKraj() {

        return kraj;
    }
}
