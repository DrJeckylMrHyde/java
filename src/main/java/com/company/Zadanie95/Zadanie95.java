package com.company.Zadanie95;

 class Zadanie95 {
    public static void main(String[] args) {
        Waluta w = new Waluta(3.5,4.5,TypWaluty.EUR);
        System.out.println(w);
        w.pokazeZysk(1000);
        System.out.println();
        System.out.println(TypWaluty.USD.getOpis());
        System.out.println(TypWaluty.USD.getKraj());
        System.out.println(w.getTypWaluta().getOpis());
        System.out.println(w.getTypWaluta().getKraj());
    }
}
