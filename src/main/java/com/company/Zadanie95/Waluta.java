package com.company.Zadanie95;

 class Waluta {

    private double kursSprzedazy;
    private double kursKupna;
    private TypWaluty typWaluta;

    Waluta(double kursSprzedazy, double kursKupna, TypWaluty typWaluta) {
        this.kursKupna = kursKupna;
        this.kursSprzedazy = kursSprzedazy;
        this.typWaluta = typWaluta;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s",
                kursKupna,
                kursSprzedazy,
                typWaluta.getOpis());
    }

    void pokazeZysk(double kwota){
        double przelicznik = kwota/kursSprzedazy;
        // zapis %.2f umozliwia wyswietlenie z zaokragleniem do 2. miejsca po przecinku
        System.out.printf("Za %s zł kupisz %.2f %s",
                kwota,
                przelicznik,
                typWaluta.getOpis());
    }

     public TypWaluty getTypWaluta() {
         return typWaluta;
     }
 }
