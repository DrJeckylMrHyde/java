package com.company.Zadanie93;

import java.util.Arrays;

public class Zadanie93 {
    public static void main(String[] args) {
        //System.out.println(Arrays.toString(howLong("aaaaaaaaaabbbbbbbbcccc")));
        String a = "kot";
        String b = "kot";
        System.out.println(a.hashCode());
        System.out.println(b.hashCode());
    }

    public static int[] howLong(String ciagZnakow) {
        String[] tablica = ciagZnakow.split("");
        String tenSamElement = tablica[0];
        int zliczoneWyswietlenia = 0;
        int[] wynik = new int[tablica.length];
        int indexTablicyWynikowej = 0;

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i].equals(tenSamElement)) {
                zliczoneWyswietlenia++;
            } else {
                tenSamElement = tablica[i];
                wynik[indexTablicyWynikowej] = zliczoneWyswietlenia;
                zliczoneWyswietlenia = 1;
                indexTablicyWynikowej++;
            }
        }
        wynik[indexTablicyWynikowej] = zliczoneWyswietlenia;

        return skracaTabliceDoRozmiaru(wynik);
    }

    public static int[] skracaTabliceDoRozmiaru(int[] tablica) {
        int dlugosc = 0;
        for (; dlugosc < tablica.length; dlugosc++) {
            if (tablica[dlugosc] == 0) {
                break;
            }
        }
        int[] shorterTable = new int[dlugosc];
        for (int i = 0; i < shorterTable.length; i++) {
            shorterTable[i] = tablica[i];
        }

        return shorterTable;
    }
}
