package com.company.Zadanie155;

public class Zadanie155 {
    public static void main(String[] args) {
        metoda1();
        System.out.println();
        metoda2();
    }

    private static void metoda1() {
        Osoba o1 = new Osoba("Marek", 27);
        String informacje = o1.przedstawSie();
        System.out.println(informacje);
    }

    private static void metoda2() {
        String informacje = new Osoba("Roman", 32) {
            @Override
            protected String przedstawSie() {
                return String.format("Jestem %s, mam skończone %s lat", getImie(), getWiek());
            }
        }.przedstawSie();
        System.out.println(informacje);
    }
}
