package com.company.Zadanie155;

public class Osoba {
    private String imie;
    private int wiek;

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    String przedstawSie(){
        return String.format("Mam na imię %s i mam %s lat",imie,wiek);
    }

    public String getImie() {
        return imie;
    }

    public int getWiek() {
        return wiek;
    }
}
