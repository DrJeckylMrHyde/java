package com.company.Zadanie79;

import java.util.Scanner;

public class Zadanie79 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Rownanie r1 = new Rownanie();
        Rownanie r2 = new Rownanie(3,2,1);
        System.out.print("Podaj a: ");
        r1.setA(s.nextInt());
        System.out.print("Podaj b: ");
        r1.setB(s.nextInt());
        System.out.print("Podaj c: ");
        r1.setC(s.nextInt());
        System.out.print(r1);
        System.out.println(r1.obliczRownanie());
        System.out.print(r2);
        System.out.println(r2.obliczRownanie());
        System.out.print("Podaj parametr: ");
        int parametr = s.nextInt();
        System.out.println(r1.czyWiekszaodParametr(parametr));
    }
}
