package com.company.Zadanie79;

public class Rownanie {
    private int a;
    private int b;
    private int c;

    Rownanie(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    Rownanie() {

    }

    public void setA(int a) {               // set nic nie zwraca ewentualnie get

        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public void setC(int c) {

        this.c = c;
    }

    @Override
    public String toString() {
        return String.format("%s^2 + %s^3 + %s^4 = ",
                a,
                b,
                c);
    }

    double obliczRownanie() {
        return (Math.pow(a,2) + Math.pow(b,3) + Math.pow(c,4));
    }

    boolean czyWiekszaodParametr(int parametr){
        return obliczRownanie() > parametr;
    }
}
