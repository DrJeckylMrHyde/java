package com.company.zadanie178Lombok;

import lombok.*;

@Getter
@ToString
@AllArgsConstructor
@Setter
public class Car {
     String name;
     String color;
//    zablokowany getter dla pola rokProdukcji
    @Getter(AccessLevel.NONE)
     int yearOfProduction;
     int numberOfDoors;
}
