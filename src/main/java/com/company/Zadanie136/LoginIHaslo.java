package com.company.Zadanie136;

import java.util.List;

public class LoginIHaslo {

    String login;
    String haslo;

    public LoginIHaslo(String login, String haslo) {
        this.login = login;
        this.haslo = haslo;
    }

    public String toString() {
        return String.format("Login: %s, hasło: %s",
                login,
                haslo);
    }

}
