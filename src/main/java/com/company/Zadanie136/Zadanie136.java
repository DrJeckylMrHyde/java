package com.company.Zadanie136;

        import java.io.BufferedReader;
        import java.io.FileReader;
        import java.io.IOException;
        import java.util.*;

public class Zadanie136 {
    private static final String SEPARATOR = ":";
    private static final Double STO = 100.0;
    private static final String MAUPKA = "@";

    public static void main(String[] args) {
        try {
            List<LoginIHaslo> listaDanych = zwrocListeOsobZPliku("plikiDoCwiczenOdZad132/passwd.txt");
            System.out.println(iluUzytkownikowWycieklo(listaDanych));
            System.out.println(zwracaProcentLoginowRownyHaslom(listaDanych));
            System.out.println(liczbaUzytkownikowBezCyfrWHasle(listaDanych));
            System.out.println(bezDuzychLiterWHasle(listaDanych));
            System.out.println(dlugoscHaslaKrotszaNiz(listaDanych, 7));
            System.out.println(zwracaIloscWystapienHasel(listaDanych));
            System.out.println(zwraca10NajpopularniejszychHasel(listaDanych));
            System.out.println(ileRazyWystapilaKazdaZeSkrzynekMailowych(listaDanych));

        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

    }

    private static List<LoginIHaslo> zwrocListeOsobZPliku(String sciezkaPliku) throws IOException {
        List<LoginIHaslo> listaOsob = new ArrayList<>();
        BufferedReader bufor = new BufferedReader(new FileReader(sciezkaPliku));
        String line = bufor.readLine();
        while (line != null) {
            listaOsob.add(getOsobaZLinii(line));
            line = bufor.readLine();
        }
        return listaOsob;
    }

    private static LoginIHaslo getOsobaZLinii(String line) {
        String[] dane = line.split(SEPARATOR);
        return new LoginIHaslo(dane[0], dane[1]);
    }

    private static double zwracaProcentLoginowRownyHaslom(List<LoginIHaslo> lista) {
        int counter = 0;
        for (LoginIHaslo loginIHaslo : lista) {
            if (loginIHaslo.login.equals(loginIHaslo.haslo)) {
                counter++;
            }
        }
        double percent = STO * counter / lista.size();
        return Math.round(percent);
    }

    private static int iluUzytkownikowWycieklo(List<LoginIHaslo> lista) {
        return lista.size();
    }

    private static int liczbaUzytkownikowBezCyfrWHasle(List<LoginIHaslo> lista) {
        int counter = 0;
        for (LoginIHaslo loginIHaslo : lista) {
            if (loginIHaslo.haslo.matches("[^0-9]+")) {
                counter++;
            }
        }
        return counter;
    }

    private static double bezDuzychLiterWHasle(List<LoginIHaslo> lista) {
        int counter = 0;
        for (LoginIHaslo loginIHaslo : lista) {
            if (loginIHaslo.haslo.matches("[a-z0-9]+")) {
                counter++;
            }
        }
        double percent = STO * counter / lista.size();
        return Math.round(percent);
    }

    private static int dlugoscHaslaKrotszaNiz(List<LoginIHaslo> lista, int dlugosc) {
        int counter = 0;
        for (LoginIHaslo loginIHaslo : lista) {
            if (loginIHaslo.haslo.length() < dlugosc) {
                counter++;
            }
        }
        return counter;
    }

    private static Map<Integer, Integer> zwracaIloscWystapienHasel(List<LoginIHaslo> lista) {
        Map<Integer, Integer> mapaHasel = new HashMap<>();

        for (LoginIHaslo loginIHaslo : lista) {
            int iloscPowtorzen = 0;
            for (LoginIHaslo haslo : lista) {
                if (loginIHaslo.haslo.length() == haslo.haslo.length()) {
                    iloscPowtorzen++;
                }
            }
            mapaHasel.put(loginIHaslo.haslo.length(), iloscPowtorzen);
        }
        return mapaHasel;
    }

    private static List<Map.Entry<String, Integer>> zwraca10NajpopularniejszychHasel(List<LoginIHaslo> lista) {
        Map<String, Integer> mapaPopularnychHasel = new HashMap<>();
        for (LoginIHaslo loginIHaslo : lista) {
            int liczbaPowtorzen = 0;
            for (LoginIHaslo haslo : lista) {
                if (loginIHaslo.haslo.equals(haslo.haslo)) {
                    liczbaPowtorzen++;
                }
            }
            mapaPopularnychHasel.put(loginIHaslo.haslo, liczbaPowtorzen);
        }
        List<Map.Entry<String, Integer>> entries = new ArrayList<>(mapaPopularnychHasel.entrySet());
        entries.sort(Map.Entry.comparingByValue());
        List<Map.Entry<String, Integer>> listaPopularnychHasel = entries.subList(entries.size() - 10, entries.size());
        return listaPopularnychHasel;
    }

    private static Map<String, Integer> ileRazyWystapilaKazdaZeSkrzynekMailowych(List<LoginIHaslo> lista) {
        Map<String, Integer> mapaSkrzynek = new HashMap<>();
        for (LoginIHaslo loginIHaslo : lista) {
            if (loginIHaslo.login.contains(MAUPKA)) {
                String[] skrzynka = loginIHaslo.login.toLowerCase().split(MAUPKA);
                //zamiast odwolywac sie wielokrotnie do indeksu tworze nowa zmienna
                //bo w przypadku zmianny indeksu w tablicy zrobie to tylko raz
                //a nie w 10 miejscach
                String mailbox = skrzynka[1];
                if (skrzynka.length == 2) {
                    // tu mozna bylo stworzyc zmienna int = mapa.getOrDefault(....)
                    mapaSkrzynek.put(mailbox, mapaSkrzynek.getOrDefault(mailbox, 0) + 1);
                }
            }
        }
        return mapaSkrzynek;
    }
}


