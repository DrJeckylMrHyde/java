package com.company.zadanie77;

public class Zadanie77 {
    public static void main(String[] args) {
        Osoba o1 = new Osoba("Paweł", "Kowalski");
        System.out.println(o1);
        Osoba o2 = new Osoba("Marian","Kowalski",16, true);
        System.out.println(o2);
        o2.przedstawSie();
        if(o2.czyPelnoletni()){
            System.out.println(" i jestem pełnoletni");
        } else {
            System.out.println(" i jestem gówniakiem");
        }
    }
}
