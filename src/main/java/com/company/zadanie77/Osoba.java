package com.company.zadanie77;

public class Osoba {

    private String imie;
    private String nazwisko;
    private int wiek;
    private Boolean czyMezczyzna;

    Osoba(String imie, String nazwisko) {
        this.imie = imie;                       // dzieki this wie ze ma szukac w polach w klasie, a nie parametrach metody
        this.nazwisko = nazwisko;               // wymuszamy to gdy nazwa parametru i pola byly te same

                                                // this sluzy tez odwolaniom do metod
    }

    Osoba(String imie, String nazwisko, int wiek, boolean czyMezczyzna) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.czyMezczyzna = czyMezczyzna;
    }

    @Override
    public String toString() {
        if (wiek == 0 && czyMezczyzna == null) {
            return String.format("%s %s",
                    imie,
                    nazwisko);
        } else {
            return String.format("%s %s (%s l.) - %s",
                    imie,
                    nazwisko,
                    wiek,
                    czyMezczyzna ? "mężczyzna" : "kobieta");
        }
    }

    void przedstawSie(){
        System.out.printf("Witaj jestem %s %s, mam %s lat jestem %s",imie,nazwisko,wiek,czyMezczyzna ? "mężczyzną" : "kobietą");
    }

    boolean czyPelnoletni(){
        return wiek >= 18;
    }
}
