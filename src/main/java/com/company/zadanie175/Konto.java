package com.company.zadanie175;

public class Konto {
    private int accNumber;
    private String owner;
//    slowo kluczowe volatile
//    powoduje ze dostep do danego pola ma tylko jeden watek
//    nie ma wyscigu watkow
    private volatile double accBalance;

    public Konto(int accNumber, String owner, double accBalance) {
        this.accNumber = accNumber;
        this.owner = owner;
        this.accBalance = accBalance;
    }

    public int getAccNumber() {
        return accNumber;
    }

    public String getOwner() {
        return owner;
    }

    public double getAccBalance() {
        return accBalance;
    }

    @Override
    public String toString() {
        return String.format("Konto: %s, właściciel: %s, saldo: %s",
                accNumber,
                owner,
                accBalance);
    }

//    oznacza ze jesli ta metoda sie wykonuje i modyfikuje pewne pola klasy
//    to te pola sa zablokowane
    synchronized void deposit(double ammount){
        accBalance += ammount;
    }

    synchronized void withdraw(double amount){
        if (accBalance < amount){
//            throw new IllegalArgumentException("Brak środków na koncie");
        }
            accBalance -= amount;
    }
}
