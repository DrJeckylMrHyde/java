package com.company.zadanie175;

public class Zadanie175 {
    public static void main(String[] args) {
        metoda1();
    }

    private static void metoda1() {
        Konto k1 = new Konto(1, "Sławek", 30000);
        final int reps = 1000;
        final double amount = 200;
        Thread depositer = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < reps; i++) {
                    k1.deposit(amount);
//                    System.out.println(i + " wpłata " + k1.getAccBalance());
                }
            }
        });

        Thread withdrawer = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < reps; i++) {
                    k1.withdraw(amount);
//                    System.out.println(i + " wypłata " + k1.getAccBalance());
                }
            }
        });
        System.out.println(k1.toString());
        depositer.start();
        withdrawer.start();

        try {
            depositer.join();
            withdrawer.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(k1.toString());
    }
}
