package com.company.Zadanie96;

public enum Kierunkowy {

    POLSKA(48), NIEMCY(49), ROSJA(7);

    private int poczatekNumeru;

    Kierunkowy(int poczatekNumeru) {

        this.poczatekNumeru = poczatekNumeru;
    }

    public String jakoTekst() {
        return "+" + poczatekNumeru;
    }

    public int jakoLiczba(){

        return poczatekNumeru;
    }
}
