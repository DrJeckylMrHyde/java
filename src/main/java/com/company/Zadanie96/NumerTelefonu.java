package com.company.Zadanie96;

public class NumerTelefonu {

    private String numer;
    private Kierunkowy kierunkowy;

    NumerTelefonu(String numer, Kierunkowy kierunkowy){
        this.numer = numer;
        this.kierunkowy = kierunkowy;
    }

    @Override
    public String toString() {
        return String.format("%s %s",
                kierunkowy.jakoLiczba(),
                numer);
    }

    void wyswietlNumer(){
        System.out.println("("+kierunkowy.jakoTekst()+")"+numer);
    }
}
