package com.company.zadanie171;

public class PersonBox <T,A,G>{

    private T first;
    private A second;
    private G third;

     PersonBox(T first, A second, G third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @Override
    public String toString() {
        return "PersonBox{" +
                "first=" + first +
                ", second=" + second +
                ", third=" + third +
                '}';
    }
}
