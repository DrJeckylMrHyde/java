package com.company.zadanie171;

import com.company.Zadanie158Stream.Person;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Zadanie171 {
    public static void main(String[] args) {

        List<PersonBox<String, Integer, Boolean>> list = Arrays.asList(
                new PersonBox("Kasia",27,true),
                new PersonBox("Tomek",21,false),
                new PersonBox("Ola",24,true)
        );

        list.forEach(System.out::println);

        List<PersonBox<Integer, Integer, Integer>> list2 = Arrays.asList(
                new PersonBox<>(1,1,1),
                new PersonBox<>(2,3,4),
                new PersonBox<>(1,2,3)
        );

        List<PersonBox<Float, Integer, Integer>> list3 = Arrays.asList(
                new PersonBox<>(1F,1,1),
                new PersonBox<>(44F,3,4),
                new PersonBox<>(1_000_000F,2,3)
        );


    }
}
