package com.company.Zadanie154KlasyAnonimowe;

public class Zadanie154{
    public static void main(String[] args) {
//        metoda1();
//        System.out.println();
//        metoda2();
        metoda3();
//        metoda4();
    }

    private static void metoda2() {
        MojKomputer komp2 = new MojKomputer();
        komp2.start();
    }

    private static void metoda1() {
        Komputer komp1 = new Komputer();
        komp1.start();
    }

    //klasy anonimowe pozwalaja nadpisanie WYBRANYCH metod
    private static void metoda3(){
        new Komputer(){
            @Override
            protected void przygotowanieSystemu(){
                super.przygotowanieSystemu();
                System.out.println("Anonimowe uruchamianie");
            }
        }.start();
    }

//    private static void metoda4(){
//        Komputer komp3 = new Komputer(){
//            @Override
//            protected void przygotowanieSystemu() {
//                super.przygotowanieSystemu();
//                System.out.println("Jesteśmy Anonymus");
//            }
//            @Override
//            protected void start(){
//                super.start();
//                System.out.println("0 na koncie");
//            }
//        };
//        komp3.start();
//    }
}
