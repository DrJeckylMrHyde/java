package com.company.Zadanie131;

import java.util.Scanner;

//Utwórz metodę, która przyjmuje dwa parametry - napis (`String`) oraz liczbę (`int`) wcześniej odczytaną `Scanner`-em od użytkownika.
// Metoda ma wyświetlić podany napis, przekazaną liczbę razy.
// W przypadku gdy liczba wystąpień będzie mniejsza bądź równa zero powinien zostać rzucony własny wyjątek (dziedziczący po `RuntimeException`).
public class Zadanie131 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Podaj napis do wyświetlenia: ");
        String napis = input.nextLine();
        System.out.print("Podaj ilość powtórzeń wyświetlenia: ");
        int iloscRazy = input.nextInt();

        //tu nie musimy stosowac bloku try...catch bo dziedziczymy po Runtime Exception
        //jednak jesli chcemy to mozemy i wyswietlic komunikat wyjatku
        try {
            //metody typu void nie mozna wrzucac w metody wyswietlajace
            wyswietliNapisOkreslonaLiczbeRazy(napis, iloscRazy);
            // jesli obiekt nazywa sie ignore to wie zeby nie domagac sie tresci w bloku catch
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void wyswietliNapisOkreslonaLiczbeRazy(String napis, int iloscRazy) {
        if (iloscRazy <= 0) {
            throw new ZeroTimeDisplay("Ilość wyświetleń jest mniejsza od 1");
        }
        for (int i = 0; i < iloscRazy; i++) {
            System.out.println(napis);
        }
    }
}
