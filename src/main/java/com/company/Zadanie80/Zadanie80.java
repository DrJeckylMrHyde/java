package com.company.Zadanie80;

import java.util.Arrays;

public class Zadanie80 {
    public static void main(String[] args) {
        System.out.println(zwrocSume("43"));
    }

    private static String zwrocSume(String liczba) {
        String[] tablica = liczba.split("");
        int suma = 0;
        for (int i = 0; i < tablica.length; i++) {
            suma += Integer.parseInt(tablica[i]);
        }
        // return Integer.toString(suma);
        return String.valueOf(suma);
    }
}
