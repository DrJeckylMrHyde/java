package com.company.zadanie173;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Zadanie173 {
    public static void main(String[] args) {
        Student s1 = new Student(1, "Paweł");
        Student s2 = new Student(1, "Paweł");

        System.out.println("Student hashCode: " + s1.hashCode());
        System.out.println("Student hashCode: " + s2.hashCode());

        System.out.println("Czy obiekty są równe: " + s1.equals(s2));

        List<Student> studentList = new ArrayList<>();
        studentList.add(s1);
        studentList.add(s2);
        System.out.println("Rozmiar listy: " + studentList.size());
        System.out.println("Czy lista zawiera studenta? "
                + studentList.contains(new Student(1, "Paweł")));

        Set<Student> studentSet = new HashSet<>();
        studentSet.add(s1);
        studentSet.add(s2);
        System.out.println("Rozmiar seta: " + studentSet.size());
        System.out.println("Czy set zawiera studenta? "
                + studentSet.contains(new Student(1, "Paweł")));

        Student s3 = new Student(2, "Przemek");
        studentList.add(s3);
        studentSet.add(s3);
        System.out.println("Rozmiar listy: " + studentList.size());
        System.out.println("Rozmiar seta: " + studentSet.size());

        System.out.println("Lista studentów " + studentList);
        System.out.println("Set studentów" + studentSet);
    }
}
