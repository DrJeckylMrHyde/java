package com.company.zadanie173;

import java.util.Objects;
import java.util.Random;

public class Student {
    private int ID;
    private String imie;
    private int wiek;
    private int yearOfStudy;
    private String surname;
    private boolean isMan;

    public Student(int ID, String imie) {
        this.ID = ID;
        this.imie = imie;
    }

    public int getID() {
        return ID;
    }

    public String getImie() {
        return imie;
    }

//    @Override
//    public int hashCode() {
////        zadajemy tu liczbe pierwsza
//        int hash = 2;
////        mnozymy razy 31 zgodnie z dok Oracle
//        hash = 31 * hash + ID;
//        hash = 31 * hash + (imie.isEmpty() ? 0 : imie.hashCode());
//        return hash;
////        zwracanie wartosci losowej wyklada nam zastosowanie kolekcji SET
////        return new Random().nextInt();
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (obj instanceof Student) {
//            Student student = (Student) obj;
//            return this.getID() == student.getID()
//                    && this.getImie().equals(student.getImie());
//        } else {
//            return false;
//        }
//    }


//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Student student = (Student) o;
//        return getID() == student.getID() &&
//                Objects.equals(getImie(), student.getImie());
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(getID(), getImie());
//    }

    @Override
    public String toString() {
        return "Student{" +
                "ID=" + ID +
                ", imie='" + imie + '\'' +
                '}';
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Student student = (Student) o;
//
//        if (getID() != student.getID()) return false;
//        return getImie() != null ? getImie().equals(student.getImie()) : student.getImie() == null;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = getID();
//        result = 31 * result + getImie().hashCode();
//        return result;
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (getID() != student.getID()) return false;
        if (wiek != student.wiek) return false;
        if (yearOfStudy != student.yearOfStudy) return false;
        if (isMan != student.isMan) return false;
        if (getImie() != null ? !getImie().equals(student.getImie()) : student.getImie() != null) return false;
        return surname != null ? surname.equals(student.surname) : student.surname == null;
    }

    @Override
    public int hashCode() {
        int result = getID();
        result = 31 * result + (getImie() != null ? getImie().hashCode() : 0);
        result = 31 * result + wiek;
        result = 31 * result + yearOfStudy;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (isMan ? 1 : 0);
        return result;
    }
}
