package com.company.Zadanie114;

import java.util.*;

public class Zadanie114 {

    public static void main(String[] args) {
        System.out.println(pokazePozycjeLitery("kotek"));
    }

    private static Map<String, List<Integer>> pokazePozycjeLitery(String napis){
        Map<String, List<Integer>> mapka = new HashMap<>();
        String [] litery = napis.split("");

        for (int i = 0; i < napis.length(); i++) {

            //na poczatku zanim litery sie powtarzaja domyslna wartoscia mojej listy jest utworzenie ArrayListy
            List<Integer> miejsceWystapienia = mapka.getOrDefault(litery[i], new ArrayList<>());
            miejsceWystapienia.add(i);
            mapka.put(litery[i], miejsceWystapienia);

        }
        return mapka;
    }
}
