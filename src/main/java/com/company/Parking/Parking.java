package com.company.Parking;

import java.util.List;

public class Parking {

    private List<Zone> zones;

    Parking() {
    }

    Parking(List<Zone> zones) {

        this.zones = zones;
    }

    List<Zone> getZones() {

        return zones;
    }

    static Parking generateParking(List<Zone> zones) {
        return new Parking(zones);
    }

    @Override
    public String toString() {

        return String.format("Parking with zones: %s\n",getZones());
    }
}
