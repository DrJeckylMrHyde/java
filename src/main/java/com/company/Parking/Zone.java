package com.company.Parking;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Zone extends Parking {
    Integer zoneNo;
    Integer parkingSpaces;
    Integer freeParkingSpaces;

    public Zone(Integer zoneNo, Integer parkingSpaces, Integer freeParkingSpaces) {
        this.zoneNo = zoneNo;
        this.parkingSpaces = parkingSpaces;
        this.freeParkingSpaces = freeParkingSpaces;
    }

    static List<Zone> generateZones() {
        Random r = new Random();
        int zones = r.nextInt(11) + 1;
        List<Zone> zoneList = new ArrayList<>();
        for (int i = 0; i < zones; i++) {
            Integer parkingSpaces = r.nextInt(21) + 10;
            Integer freeParkingSpaces = r.nextInt(parkingSpaces) + 1;
            zoneList.add(new Zone(i + 1, parkingSpaces, freeParkingSpaces));
        }
        return zoneList;
    }


    public Integer getZoneNo() {
        return zoneNo;
    }

    public Integer getParkingSpaces() {
        return parkingSpaces;
    }

    public Integer getFreeParkingSpaces() {
        return freeParkingSpaces;
    }

    @Override
    public String toString() {
        return String.format("\nZone no %s Parking spaces: %s, Free parking spaces %s",
                getZoneNo(),
                getParkingSpaces(),
                getFreeParkingSpaces());
    }
}
