package com.company.Parking;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import static com.company.Parking.Vehicle.VehicleType.M;

public class Main {

    private static List<Zone> zoneList;
    private static Parking parking;
    private static PriceList priceList;
    private static Vehicle vehicle;

    public static void main(String[] args) {
        zoneList = Zone.generateZones();
        parking = Parking.generateParking(zoneList);
        menu();
    }

//    private static void generateParking() {
//
//        parking = new Parking(zoneList);
//    }


    private static void menu() {
        System.out.println("Welcome");
        for (; ; ) {
            System.out.println("1. Show parking");
            System.out.println("2. Enter the parking");
            System.out.println("3. Exit the program");
            System.out.println("What is the next move?");
            Scanner input = new Scanner(System.in);
            int choice = input.nextInt();
            switch (choice) {

                case 1:
                    System.out.println(parking);
                    break;
                case 2:
                    System.out.println(parking);
                    generatePriceList();
                    generateVehicle();
                    if (vehicle.cash >= priceList.carHourPrice * vehicle.predictedTimeForParking) {
                        vehicle.cash -= priceList.carHourPrice * vehicle.predictedTimeForParking;
                        zoneList.get(0).freeParkingSpaces--;
                    } else {
                        System.out.println("You don't have enough money");
                    }
                    System.out.println(parking);
                    break;
                case 3:
                    System.exit(0);
            }
        }
    }

    private static void generatePriceList() {
        priceList = new PriceList(3.99, 7.99, 0.99);
    }

    private static void generateVehicle() {
        vehicle = new Vehicle(M, 3, 49.99);
    }


}
