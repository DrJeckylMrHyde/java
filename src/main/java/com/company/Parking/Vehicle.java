package com.company.Parking;

import java.time.LocalTime;

public class Vehicle {

    Integer predictedTimeForParking;
    Double cash;
    Enum VehicleType;

    public Vehicle(VehicleType vehicleType, int wantedTimeForParking, Double cash) {
        this.predictedTimeForParking = wantedTimeForParking;
        this.cash = cash;
        this.VehicleType = vehicleType;
    }

    enum VehicleType{
        T,C,M
    }
}
