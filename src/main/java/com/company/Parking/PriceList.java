package com.company.Parking;

public class PriceList extends Parking {

    double carHourPrice;
    double trackHourPrice;
    double motorHourPrice;

    public PriceList(double carHourPrice, double trackHourPrice, double motorHourPrice) {
        this.carHourPrice = carHourPrice;
        this.trackHourPrice = trackHourPrice;
        this.motorHourPrice = motorHourPrice;
    }
}
