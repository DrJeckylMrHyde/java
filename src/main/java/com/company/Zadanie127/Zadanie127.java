package com.company.Zadanie127;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie127 {
    public static void main(String[] args) {
        int [] tablica = new int []{1,2,3,4,5,6,7,8,9};
        wyswietliTablice(tablica);
        System.out.println();
        //Arrays.asList zwraca ArrayList
        List<Integer> lista = Arrays.asList(1,2,3,4,5,6,7,8,9);
        wyswietliListe(lista);
    }

    private static void wyswietliTablice(int [] tablica){
        int indeks = 0;
        while (true){
            try {
                System.out.print(tablica[indeks++]+", ");
            } catch (ArrayIndexOutOfBoundsException e){
                break;
            }
        }
        System.out.print("\b\b");
    }

    private static void wyswietliListe(List<Integer> lista){
        int indeks = 0;
        while (true){
            try {
                System.out.print(lista.get(indeks++)+", ");
            } catch (IndexOutOfBoundsException e){
                break;
            }
        }
        System.out.print("\b\b");
    }
}
