package com.company.Zadanie146Testy;


public class Zadanie146 {
    public static void main(String[] args) {
        System.out.println(obliczPodatek(100.0, 23));
    }

    public static Double obliczPodatek(Double liczba,Integer p){
        if(p <= 0 || p > 100){
            return liczba;
        }
        else if(p == 100){
            return 2*liczba;
        }
        return liczba+liczba*p*0.01;
    }
}
