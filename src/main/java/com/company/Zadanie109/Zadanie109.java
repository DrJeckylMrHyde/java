package com.company.Zadanie109;

import java.util.ArrayList;
import java.util.LinkedList;

public class Zadanie109 {
    private static final Integer LICZBA_OPR = 100_000;
    private static final Integer LICZBA_DOD = 1_000;

    public static void main(String[] args) {

        long czasStart = System.currentTimeMillis();
        ArrayList<Integer> arrayList = dodawanieArrayList();
        long czasKoniec = System.currentTimeMillis();
        System.out.println("Czas konkatenacji ArrayList: " + (czasKoniec - czasStart));

        czasStart = System.currentTimeMillis();
        LinkedList<Integer> linkedList = dodawanieLinkedList();
        czasKoniec = System.currentTimeMillis();
        System.out.println("Czas konkatenacji LinkedList: " + (czasKoniec - czasStart));
//
//        czasStart = System.currentTimeMillis();
//        ArrayList<Integer> ArrayListBegin = dodawanieArrayListNaPoczatek();
//        czasKoniec = System.currentTimeMillis();
//        System.out.println("Czas konkatenacji na początek ArrayList: " + (czasKoniec - czasStart));
//
//        czasStart = System.currentTimeMillis();
//        LinkedList<Integer> linkedListBegin = dodawanieLinkedListNaPoczatek();
//        czasKoniec = System.currentTimeMillis();
//        System.out.println("Czas konkatenacji na poczatek LinkedList: " + (czasKoniec - czasStart));
//
//        czasStart = System.currentTimeMillis();
//        ArrayList<Integer> ArrayListEnd = dodawanieArrayListNaKoniec();
//        czasKoniec = System.currentTimeMillis();
//        System.out.println("Czas konkatenacji na koniec ArrayList: " + (czasKoniec - czasStart));
//
//        czasStart = System.currentTimeMillis();
//        LinkedList<Integer> linkedListEnd = dodawanieLinkedListNaKoniec();
//        czasKoniec = System.currentTimeMillis();
//        System.out.println("Czas konkatenacji na koniec LinkedList: " + (czasKoniec - czasStart));

        czasStart = System.currentTimeMillis();
        dodawanieArrayListNaIndeks(arrayList);
        czasKoniec = System.currentTimeMillis();
        System.out.println("Czas konkatenacji na indeks ArrayList: " + (czasKoniec - czasStart));

        czasStart = System.currentTimeMillis();
        dodawanieLinkedListNaIndeks(linkedList);
        czasKoniec = System.currentTimeMillis();
        System.out.println("Czas konkatenacji na indeks LinkedList: " + (czasKoniec - czasStart));

        czasStart = System.currentTimeMillis();
        dodawanieArrayListNaIndeksForEach(arrayList);
        czasKoniec = System.currentTimeMillis();
        System.out.println("Czas konkatenacji na indeks przy uzyciu for-each ArrayList: " + (czasKoniec - czasStart));

        czasStart = System.currentTimeMillis();
        dodawanieLinkedListNaIndeksForEach(linkedList);
        czasKoniec = System.currentTimeMillis();
        System.out.println("Czas konkatenacji na indeks przy uzyciu for-each LinkedList: " + (czasKoniec - czasStart));

        czasStart = System.currentTimeMillis();
        usuwanieArrayListNaIndeks(arrayList);
        czasKoniec = System.currentTimeMillis();
        System.out.println("Czas usuwania na indeks w ArrayList: " + (czasKoniec - czasStart));

        czasStart = System.currentTimeMillis();
        usuwanieLinkedListNaIndeks(linkedList);
        czasKoniec = System.currentTimeMillis();
        System.out.println("Czas usuwania na indeks w LinkedList: " + (czasKoniec - czasStart));


    }

    private static ArrayList<Integer> dodawanieArrayList() {
        ArrayList<Integer> lista = new ArrayList<>();
        for (int i = 0; i < LICZBA_OPR; i++) {
            lista.add(LICZBA_DOD);
        }
        return lista;
    }

    //LinkedList - lista dowiazaniowa przechodzi po wszystkich elementach
    private static LinkedList<Integer> dodawanieLinkedList() {
        LinkedList<Integer> lista = new LinkedList<>();
        for (int i = 0; i < LICZBA_OPR; i++) {
            lista.add(LICZBA_DOD);
        }
        return lista;
    }

    private static ArrayList<Integer> dodawanieArrayListNaPoczatek() {
        ArrayList<Integer> lista = new ArrayList<>();
        for (int i = 0; i < LICZBA_OPR; i++) {
            lista.add(0, LICZBA_DOD);
        }
        return lista;
    }

    private static LinkedList<Integer> dodawanieLinkedListNaPoczatek() {
        LinkedList<Integer> lista = new LinkedList<>();
        for (int i = 0; i < LICZBA_OPR; i++) {
            lista.add(0, LICZBA_DOD);
        }
        return lista;
    }

    private static ArrayList<Integer> dodawanieArrayListNaKoniec() {
        ArrayList<Integer> lista = new ArrayList<>();
        for (int i = 0; i < LICZBA_OPR; i++) {
            lista.add(lista.size(), LICZBA_DOD);
        }
        return lista;
    }

    private static LinkedList<Integer> dodawanieLinkedListNaKoniec() {
        LinkedList<Integer> lista = new LinkedList<>();
        for (int i = 0; i < LICZBA_OPR; i++) {
            lista.add(lista.size(), LICZBA_DOD);
        }
        return lista;
    }

    private static Integer dodawanieArrayListNaIndeks(ArrayList <Integer> arrayList) {
        Integer liczbaZListy = 0;
        for (int i = 0; i < LICZBA_OPR; i++) {
            liczbaZListy = arrayList.get(i);
        }
        return liczbaZListy;
    }

    private static Integer dodawanieLinkedListNaIndeks(LinkedList <Integer> linkedList) {
        Integer liczbaZListy = 0;
        for (int i = 0; i < linkedList.size(); i++) {
            liczbaZListy = linkedList.get(i);
        }
        return liczbaZListy;
    }

    private static Integer dodawanieArrayListNaIndeksForEach(ArrayList <Integer> arrayList) {
        int liczbaZListy = 0;
        for (Integer liczbaNaLiscie : arrayList) {
            liczbaZListy = liczbaNaLiscie;
        }
        return liczbaZListy;
    }

    private static Integer dodawanieLinkedListNaIndeksForEach(LinkedList <Integer> linkedList) {
        int liczbaZListy = 0;
        for (Integer liczbaNaLiscie : linkedList) {
            liczbaZListy = liczbaNaLiscie;
        }
        return liczbaZListy;
    }

    private static ArrayList<Integer> usuwanieArrayListNaIndeks(ArrayList <Integer> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            arrayList.remove(i);
        }
        return arrayList;
    }

    private static LinkedList<Integer> usuwanieLinkedListNaIndeks(LinkedList <Integer> linkedList) {
        for (int i = 0; i < linkedList.size(); i++) {
            linkedList.remove(i);
        }
        return linkedList;
    }


}
