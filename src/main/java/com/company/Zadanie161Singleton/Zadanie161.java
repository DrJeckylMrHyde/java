package com.company.Zadanie161Singleton;

public class Zadanie161 {
    public static void main(String[] args) {
        // stworzenie obiektu nastepuje poprzez metode getInstance()
        // a nie new Configuration()
        Configuration config = Configuration.getInstance();
        config.setUlubionaLiczba(7);

        metoda1();
        config.setUlubionaLiczba(69);
        metoda1();
    }

    private static void metoda1(){
        Configuration config = Configuration.getInstance();
        System.out.println(config.getUlubionaLiczba());
    }
}
