package com.company.Zadanie161Singleton;

//3 WAZNE ZASADY DOTYCZACE SINGLETON
// ZASTOSOWANIE: USTAWIENIA UZYTKOWNIKA W PROGRAMIE
// JEDNO POLACZENIE Z BAZA DANYCH ZEBY BYLO JEDNO ZRODLO WEJSCIA

public class Configuration {
    private int ulubionaLiczba;

    //2 tworzymy pole typu konfiguracja
    private static Configuration instance = null;

    //robiac private blokujemy mozliwosc tworzenia obiektow tej klasy
    // ! 1 PRYWATNY KONSTRUKTOR UNIEMOZLIWIAMY TWORZENIE OBIEKTU TEJ KLASY
    private Configuration() {
    }

    //3 metoda WYDAJACA OBIEKT KLASOWY
    public static Configuration getInstance(){
        if(instance == null){
            System.out.println("Obiekt tworzony pierwszy raz");
            instance = new Configuration();
        } else {
            System.out.println("Obiekt już istnieje");
        }
        System.out.println(instance.hashCode());
        return instance;
    }

    public int getUlubionaLiczba() {
        return ulubionaLiczba;
    }

    public void setUlubionaLiczba(int ulubionaLiczba) {
        this.ulubionaLiczba = ulubionaLiczba;
    }


}
