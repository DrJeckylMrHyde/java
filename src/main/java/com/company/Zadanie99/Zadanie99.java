package com.company.Zadanie99;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie99 {
    public static void main(String[] args) {
        List<Integer> maxList = new ArrayList<>(Arrays.asList(
                1,-10,1123,121312,121
        ));

        System.out.println(zwracaMax(maxList));
        System.out.println(zwracaMax2(maxList));
    }

    private static Integer zwracaMax(List<Integer> maxList) {
        Integer max = maxList.get(0);
        for (Integer integer : maxList) {
            if(integer > max){
                max = integer;
            }
        }
        return max;
    }

    private static Integer zwracaMax2(List<Integer> maxList) {
        Integer max = maxList.get(0);
        for (int i = 0; i < maxList.size(); i++) {
            if(maxList.get(i) > max){
                max = maxList.get(i);
            }
        }
        return max;
    }

}
