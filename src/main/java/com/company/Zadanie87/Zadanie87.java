package com.company.Zadanie87;

public class Zadanie87 {
    public static void main(String[] args) {
        System.out.println(ileRazyWystapiZnak("Szedł Sasza suchą szosą suszył sobie spodnie", 's'));
    }

    public static int ileRazyWystapiZnak(String wyraz, char znak) {
        int iloscPowtorzen = 0;
        for (int pozycja = 0; pozycja < wyraz.length(); pozycja++) {
            if (wyraz.toLowerCase().charAt(pozycja) == znak) {
                iloscPowtorzen++;
            }
        }

        return iloscPowtorzen;
    }

}
