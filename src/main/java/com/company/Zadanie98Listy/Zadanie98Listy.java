package com.company.Zadanie98Listy;

import java.util.ArrayList;
import java.util.List;

public class Zadanie98Listy {
    public static void main(String[] args) {

        List<Integer> listaLiczb = new ArrayList<>();
        listaLiczb.add(12);
        listaLiczb.add(13);
        listaLiczb.add(4);
        listaLiczb.add(2);

        System.out.println(sumujeLiczby(listaLiczb));
        System.out.println(sumujeLiczby2(listaLiczb));
    }

    public static Integer sumujeLiczby (List<Integer> listaLiczb){
        Integer sum = 0;
        for (int i = 0; i < listaLiczb.size(); i++) {
            sum += listaLiczb.get(i);
        }
        return sum;
    }

    public static Integer sumujeLiczby2 (List<Integer> listaLiczb){
        Integer sum = 0;
        for (Integer integer : listaLiczb) {
            sum += integer;
        }
        return sum;
    }
}
