package com.company.Zadanie148;


public class SumaElementow {

     public Integer ileElementowZsumowac(int[] tablica, int liczba) {
        if (tablica == null) {
            return null;
        }

        int suma = 0;
        int count = 0;
        for (int elTab : tablica) {
            suma += elTab;
            count++;
            if (suma > liczba) {
                return count;
            }
        }
        return null;
    }
}
