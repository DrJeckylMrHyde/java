package com.company.Zadanie118Dziedziczenie;

public class Zadanie118Dziedziczenie {
    public static void main(String[] args) {
        Osoba osoba = new Osoba("Marcin",24);
        System.out.println(osoba.przedstawSie());

        Osoba osoba2 = new Osoba("Paweł",38);
        System.out.println(osoba2);

        Pracownik pracownik = new Pracownik("Adam",42,15000);
        System.out.println(pracownik);

        System.out.println(pracownik.zwrocPensjeZaDanyOkres(5));


        Kierownik kierownik = new Kierownik("Janusz",55,20000);
        System.out.println(kierownik.zwrocPensjeZaDanyOkres(3));
        kierownik.ustawPremie(1000);
        System.out.println(kierownik.zwrocPensjeZaDanyOkres(3));
        System.out.println(kierownik.przedstawSie());
    }
}
