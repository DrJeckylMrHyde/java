package com.company.Zadanie118Dziedziczenie;

public class Pracownik extends Osoba{
    double pensja;

    public Pracownik(String imie, int wiek, double pensja) {
        // super odwoluje sie do konstruktora z klasy User
        super(imie, wiek);
        this.pensja = pensja;
    }

    //nadpisuje przedstawSie z klasy User
    @Override
    String przedstawSie() {
        return super.przedstawSie()+" i zarabiam "+pensja+" PLN";
    }

    //nadpisane przedstawSie w nadpisanym toString
    @Override
    public String toString() {
        return przedstawSie();
    }

    double zwrocPensjeZaDanyOkres(int iloscMiesiecy){
        return iloscMiesiecy*pensja;
    }
}
