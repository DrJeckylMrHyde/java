package com.company.Zadanie118Dziedziczenie;

public class Osoba {
    String imie;
    int wiek;

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    String przedstawSie(){
        return String.format("Witaj jestem %s mam %s lat",imie,wiek);
    }

    @Override
    public String toString() {
        return przedstawSie();
    }
}
