package com.company.Zadanie118Dziedziczenie;

public class Kierownik extends Pracownik{
    double premia;

    public Kierownik(String imie, int wiek, double pensja) {

        super(imie, wiek, pensja);
    }

    void ustawPremie(double premia){

        this.premia = premia;
    }

    @Override
    double zwrocPensjeZaDanyOkres(int iloscMiesiecy) {
        return super.zwrocPensjeZaDanyOkres(iloscMiesiecy)+premia*iloscMiesiecy;
    }

    @Override
    String przedstawSie() {
        return super.przedstawSie()+" moja premia wynosi "+premia;
    }
}
