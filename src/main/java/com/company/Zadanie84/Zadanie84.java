package com.company.Zadanie84;

public class Zadanie84 {
    public static void main(String[] args) {
        System.out.println(zwrocSrodkowyZnak("Ala"));
    }

    static String zwrocSrodkowyZnak(String wyraz) {
        return wyraz.length() % 2 == 0
                ? wyraz.substring(wyraz.length() / 2 - 1, wyraz.length() / 2 + 1)               // drugi parametr to granica przed ktora sie konczy
                : wyraz.substring(wyraz.length() / 2, wyraz.length() / 2 + 1);
    }
}
