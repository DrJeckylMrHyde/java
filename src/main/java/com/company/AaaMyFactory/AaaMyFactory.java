package com.company.AaaMyFactory;

public class AaaMyFactory {

    public static void main(String[] args) {
        ShapeFactory shapeFactory = new ShapeFactory();
        IShape triangle = shapeFactory.CreateShape(ShapeType.TRIANGLE);
        IShape square = shapeFactory.CreateShape(ShapeType.SQUARE);

        System.out.println(square.toString());
        System.out.println(triangle.toString());

        Object obj = new Object();
//        obj.
    }
}
