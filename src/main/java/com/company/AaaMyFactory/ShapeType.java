package com.company.AaaMyFactory;

public enum ShapeType {

    SQUARE(1),
    TRIANGLE(2);

    private int shapeNumber;

    ShapeType(int shapeNumber) {

        this.shapeNumber = shapeNumber;
    }
}
