package com.company.AaaMyFactory;

import static com.company.AaaMyFactory.ShapeType.SQUARE;

public class ShapeFactory {

    public IShape CreateShape(ShapeType shapeType){

        switch(shapeType){

            case SQUARE:
                return new Square();


            case TRIANGLE:
                return new Triangle();

            default:
                throw new IllegalArgumentException();
        }
    }
}
