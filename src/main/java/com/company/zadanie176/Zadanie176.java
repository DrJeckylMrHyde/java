package com.company.zadanie176;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Zadanie176 {
    public static void main(String[] args) {
        metoda1();
    }

    private static void metoda1() {

        ExecutorService es1 = Executors.newFixedThreadPool(4);
        System.out.println("Wątek: " + Thread.currentThread().getId());
        for (int i = 0; i < 10; i++) {
            Runnable r = getRunnable();
            es1.submit(r);
        }
        es1.shutdown();
    }

    private static Runnable getRunnable() {
        return new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Zadanie podjął wątek nr: "
                                + Thread.currentThread().getId());
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };
    }
}
