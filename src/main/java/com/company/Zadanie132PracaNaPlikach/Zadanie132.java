package com.company.Zadanie132PracaNaPlikach;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Zadanie132 {
    public static void main(String[] args) {
        try {
            odczytajPlikZnakPoZnaku("plikiDoCwiczenOdZad132/zadanie132.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void odczytajPlikZnakPoZnaku(String sciezkadoPliku) throws IOException {
        FileInputStream plik = new FileInputStream(sciezkadoPliku);
        int znak = plik.read();
        // wynika to z tego ze znak w kodzie ASCII nie ma wartosci ujemnej
        while (znak != -1) {
            //nie obsluzy polskich liter
            System.out.print((char) znak);
            znak = plik.read();
        }
    }
}
