package com.company.Zadanie82;

public class Zadanie82 {
    public static void main(String[] args) {
        System.out.println(viewLongestWord("Ala ma kota"));
    }

    public static String viewLongestWord(String sentence){
        String [] zdanie = sentence.split(" ");
        String wyraz = "";
        for (int i = 0; i < zdanie.length; i++) {
            if(zdanie[i].length() > wyraz.length()){
                wyraz = zdanie[i];
            }
        }
        return wyraz;
    }
}
