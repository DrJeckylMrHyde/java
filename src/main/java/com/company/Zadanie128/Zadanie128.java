package com.company.Zadanie128;

public class Zadanie128 {

    public Zadanie128() {
    }

    public static void main(String[] args) {

        System.out.println(zwrocWynikDzielenia(4, 0));
    }

    private static Integer zwrocWynikDzielenia(int a, int b){
        try {
            if (b == 0) {
                throw new ArithmeticException("nie dzielimy przez 0");
            }
            return a / b;
        } catch (ArithmeticException e){
            return null;
        }
    }
}
