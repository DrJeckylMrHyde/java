package com.company.Zadanie156Comparator;

import java.util.Comparator;

// to jest comparator!
public class NameComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Pracownik p1 = (Pracownik) o1;
        Pracownik p2 = (Pracownik) o2;
        return p1.getImie().compareTo(p2.getImie());
    }
}
