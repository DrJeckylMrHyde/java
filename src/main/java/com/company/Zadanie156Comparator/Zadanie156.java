package com.company.Zadanie156Comparator;

import java.util.*;

public class Zadanie156 {
    //ta lista musi byc statyczna zeby widzialy ja metody
    private static List<Pracownik> listaPracownikow = Arrays.asList(
            new Pracownik("Krzysiek", 26, 3000),
            new Pracownik("Tomek", 29, 4500),
            new Pracownik("Jagódka", 23, 2700),
            new Pracownik("Karolina", 29, 4900),
            new Pracownik("Karolina", 31, 4800),
            new Pracownik("Karolina", 31, 4700),
            new Pracownik("Tomek", 26, 4700)
    );

    public static void main(String[] args) {
        //metoda1();
        //metoda2();
        metoda3();
        wyswietlListe(listaPracownikow);
        //metoda 4() segreguje po imieniu,wieku,pensji
        metoda4();
        wyswietlListe(listaPracownikow);
    }

    static void metoda1() {
        wyswietlListe(listaPracownikow);
    }

    static void metoda2() {
        Collections.sort(listaPracownikow);
        wyswietlListe(listaPracownikow);
    }

    private static void metoda3(){
        // tworze to jak obiekt klasy
        listaPracownikow.sort(new NameComparator());
        //wyswietlListe(listaPracownikow);
    }

    private static void metoda4(){
        listaPracownikow.sort(new SameNameAgeComparator());
        //wyswietlListe(listaPracownikow);
    }

    private static void wyswietlListe(List<Pracownik> lista) {
        System.out.println("    IMIĘ   | WIEK | PENSJA");
        System.out.println("===========|======|=======");
        for (Pracownik pracownik : lista) {
            System.out.println(pracownik);
        }
    }
}
