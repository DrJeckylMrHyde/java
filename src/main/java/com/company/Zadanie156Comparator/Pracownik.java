package com.company.Zadanie156Comparator;

public class Pracownik implements Comparable<Pracownik> {
    private String imie;
    private int wiek;
    private double pensja;

    public Pracownik(String imie, int wiek, double pensja) {
        this.imie = imie;
        this.wiek = wiek;
        this.pensja = pensja;
    }

    @Override
    public String toString() {
        //%-10s dzieki "-" przykleja do lewej 10 oznacza ze rezerwuje max 10 znakow
        return String.format("%-10s |  %-4s| %-10s",
                imie,
                wiek,
                pensja);
    }

    public String getImie() {
        return imie;
    }

    public int getWiek() {
        return wiek;
    }

    public double getPensja() {
        return pensja;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public void setPensja(double pensja) {
        this.pensja = pensja;
    }

    @Override
    public int compareTo(Pracownik p) {
//        if (this.wiek == p.wiek) {
//            // 0 obiekty sa równe kryterium to wiek
//            return 0;
//        } else if (this.wiek < p.wiek) {
//            // -1  obecny this jest PRZED przekazywaniem
//            return 1;
//        } else {
//            // 1 obecny jest PO przekazywanym, czyli parametr jest pierwszy
//            return -1;
//        }

//        metoda sluzaca podaniu kryterium sortowania
//        return Integer.compare(this.wiek,p.wiek);
//        dla wartosci liczbowych np wieku mozemy je po prostu zwyczajnie odjac
        return p.wiek-this.wiek;
    }


}
