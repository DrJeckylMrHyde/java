package com.company.Zadanie156Comparator;

import java.util.Comparator;

public class SameNameAgeComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Pracownik p1 = (Pracownik) o1;
        Pracownik p2 = (Pracownik) o2;

        //posortowanie po imieniu
        int nameCompareResult = p1.getImie().compareTo(p2.getImie());

        //warunek mowiacy ze imiona obiektow sa takie same
        if (nameCompareResult == 0) {
            //posortowanie po wieku
            int ageCompareResult = Integer.compare(p1.getWiek(), p2.getWiek());
            //warunek mowiacy ze wieki obiektow sa takie same
            if (ageCompareResult == 0){
                //sortowanie po pensji
                return Double.compare(p1.getPensja(),p2.getPensja());
            }
            return ageCompareResult;
        }
        return nameCompareResult;
    }
}
