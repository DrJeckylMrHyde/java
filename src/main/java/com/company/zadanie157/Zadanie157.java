package com.company.zadanie157;

import java.io.*;

//serializacja sluzy zapisaniu obiektu do pliku i odczytaniu go na innym komputerze
//REGULA klasy musza nazywac sie tak samo
public class Zadanie157 {

    public static void main(String[] args) {
        Wiadomosc w1 = new Wiadomosc("Ala", "Pom Pom Pom Pom",".");
        //zapisDoPliku(w1);
        Wiadomosc odczytanaWiadomosc = odczytajZPliku();
        System.out.println(odczytanaWiadomosc);
    }

    private static void zapisDoPliku(Wiadomosc w1) {
        try {
            FileOutputStream file = new FileOutputStream("plikiDoCwiczenOdZad132/zadanie157.txt");
            ObjectOutputStream output = new ObjectOutputStream(file);
            output.writeObject(w1);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void zapisDoPliku2(Wiadomosc w1) {
        //try-with-resources
        try (FileOutputStream file = new FileOutputStream("plikiDoCwiczenOdZad132/zadanie157.txt");
             ObjectOutputStream output = new ObjectOutputStream(file)) {
            output.writeObject(w1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Wiadomosc odczytajZPliku() {
        try {
            FileInputStream file = new FileInputStream("plikiDoCwiczenOdZad132/zadanie157.txt");
            ObjectInputStream input = new ObjectInputStream(file);
            //odczytuje wartosci pól konstruktora obiektu
            Wiadomosc w2 = (Wiadomosc) input.readObject();
            return w2;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
