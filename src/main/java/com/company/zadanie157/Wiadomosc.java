package com.company.zadanie157;

import java.io.Serializable;

public class Wiadomosc implements Serializable {

    //keyword transient powoduje ze dane pole sie nie serializuje
    transient private String title;
    private String content;
    public String coma;

    public Wiadomosc(String title, String content, String coma) {
        this.title = title;
        this.content = content;
        this.coma = coma;
    }

    @Override
    public String toString() {
        return String.format("Tytuł: %s\ntreść: %s%s",
                title,
                content,
                coma);
    }
}