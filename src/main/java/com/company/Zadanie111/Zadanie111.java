package com.company.Zadanie111;

import java.util.HashMap;
import java.util.Map;

//Utwórz metodę, która przyjmuje jeden parametr - mapę w postaci `String : String`.
//        Metoda ma wyświetlić zawartość przekazanej mapy (w postaci `"PL" -> "Polska"`)
public class Zadanie111 {

    public static void main(String[] args) {

        //wyswietlaMape(tworzyMape());
        wyswietlaMape2(tworzyMape());
    }

    private static Map<String, String> tworzyMape() {
        Map<String, String> mapka = new HashMap<>();
        mapka.put("PL", "Polska");
        mapka.put("DE", "Niemcy");
        mapka.put("NL", "Holandia");

        return mapka;
    }

    private static void wyswietlaMape(Map<String, String> mapka) {
        for (String klucz : mapka.keySet()) {
            // pobranie listy klucz
            System.out.println(klucz + " -> " + mapka.get(klucz));
        }
    }

    private static void wyswietlaMape2(Map<String, String> mapka) {
        for (Map.Entry<String, String> entry : mapka.entrySet()) {
            // pobranie pary klucz i wartosc
            // zmienna.getKey pobranie klucz zmienna.getValue pobranie wartosci
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        }
    }
}
