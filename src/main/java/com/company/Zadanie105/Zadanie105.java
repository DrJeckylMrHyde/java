package com.company.Zadanie105;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie105 {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>(Arrays.asList(
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10
        ));
        System.out.println(zwrociListyZPodzialemNaParzyste(lista));
    }

    private static List<List<Integer>> zwrociListyZPodzialemNaParzyste (List<Integer> lista){
        List<Integer> parzystaLista = new ArrayList<>();
        List<Integer> nieparzystaLista = new ArrayList<>();
        List<List<Integer>> listaList = new ArrayList<>(Arrays.asList(parzystaLista, nieparzystaLista
        ));
        for (Integer integer : lista) {
            if(integer % 2 == 0){
                parzystaLista.add(integer);
            }
            else {
                nieparzystaLista.add(integer);
            }
        }
        return listaList;
    }


}
