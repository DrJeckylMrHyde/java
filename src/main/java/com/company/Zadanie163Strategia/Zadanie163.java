package com.company.Zadanie163Strategia;

import com.company.Zadanie123Interface.Obliczenia;

public class Zadanie163 {
    public static void main(String[] args) {
        Kalkulator kalkulator = new Kalkulator();

        kalkulator.setStrategy(new AddStrategy());
        System.out.println(kalkulator.policz(10, 20));

        kalkulator.setStrategy(new SubstractionStrategy());
        System.out.println(kalkulator.policz(30,2));

        //tu zastosowano Singleton
        kalkulator.setStrategy(MultiplyStrategy.getInstance());
        System.out.println(kalkulator.policz(2,3));

        kalkulator.setStrategy(new DivideStrategy());
        System.out.println(kalkulator.policz(7,0));

    }
}
