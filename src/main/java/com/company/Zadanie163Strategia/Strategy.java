package com.company.Zadanie163Strategia;

public interface Strategy {

    double policz(double numb1, double numb2);

    //default dostepny od javy 8
    //raczej sie tego unika bo mozna zapomniec nadpisac
    default String nazwaOperacji(){
        return "jakąś strategię";
    }
}
