package com.company.Zadanie163Strategia;

public class AddStrategy implements Strategy {

    @Override
    public double policz(double numb1, double numb2) {

        return numb1 + numb2;
    }

    @Override
    public String nazwaOperacji() {
        return "dodawanie";
    }
}
