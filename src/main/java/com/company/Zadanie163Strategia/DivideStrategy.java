package com.company.Zadanie163Strategia;

public class DivideStrategy implements Strategy {

    @Override
    public double policz(double numb1, double numb2) {
        if(numb2 == 0){
            return 0;
        }
        return numb1/numb2;
    }
}
