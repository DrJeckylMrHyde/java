package com.company.Zadanie163Strategia;

public class MultiplyStrategy implements Strategy{

    private static MultiplyStrategy multiplyStrategy = null;

    private MultiplyStrategy() {
    }

    //3 metoda WYDAJACA OBIEKT KLASOWY
    public static MultiplyStrategy getInstance() {
        if (multiplyStrategy == null) {
            multiplyStrategy = new MultiplyStrategy();
        }
            return multiplyStrategy;
    }

    @Override
    public double policz(double numb1, double numb2) {
        return numb1*numb2;
    }

    @Override
    public String nazwaOperacji() {
        return "mnożenie";
    }
}
