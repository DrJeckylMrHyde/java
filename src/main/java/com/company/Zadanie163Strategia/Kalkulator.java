package com.company.Zadanie163Strategia;

public class Kalkulator {
    private Strategy strategy;

    public void setStrategy(Strategy strategy) {

        this.strategy = strategy;
        System.out.println("Wybrano " + strategy.nazwaOperacji());
    }

    public double policz(double numb1, double numb2) {
        if (strategy == null) {
            return 0;
        }
        return strategy.policz(numb1, numb2);
    }

}
