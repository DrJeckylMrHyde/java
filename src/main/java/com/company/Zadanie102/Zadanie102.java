package com.company.Zadanie102;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie102 {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>(Arrays.asList(
                1, 2, 3, 4, 5, 6, 7, 8, 9
        ));
        //System.out.println(zwrociRozniceZZakresu(lista, 2, 4));
        System.out.println("Szukana liczba " + pokazeIndekszRoznicy(lista, 3, 6));
    }

    private static Integer pokazeIndekszRoznicy(List<Integer> lista, int start, int end) {
        Integer indeks = zwrociRozniceZZakresu(lista, start, end);
        if (indeks < 0 || indeks > lista.size() - 1) {
            return -1;
        }
        return lista.get(indeks);
    }

    private static Integer zwrociRozniceZZakresu(List<Integer> lista, int start, int end) {
        Integer max = lista.get(start);
        Integer min = lista.get(start);
        for (int i = start; i <= end; i++) {
            if (lista.get(i) > max) {
                max = lista.get(i);
            }
            if (lista.get(i) < min) {
                min = lista.get(i);
            }
        }
        return max - min;
    }

}
