package com.company.Zadanie137;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Zadanie137 {
    public static void main(String[] args) {
        try {
            zapisDoPliku();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void zapisDoPliku() throws IOException {
        Scanner input = new Scanner(System.in);
        FileWriter writer = new FileWriter("plikiDoCwiczenOdZad132/Zadanie137.txt");
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        while (true) {
            System.out.println("Wprowadź tekst");
            String linia = input.nextLine();
            if(linia.equals("")){
                break;
            } else {
                bufferedWriter.write(linia);
                //metoda newLine to wstawienie do nowej linii w pliku
                bufferedWriter.newLine();
            }
        }
        //zrzut elementu do pliku w zasadzie na duzych plikach powinien odbywac sie co obrot petli
        //dac if np po 10k wprowadzen linijek
        bufferedWriter.flush();
        //zakonczenie pracy z plikiem, za close nie wywolam tego pliku ponownie
        bufferedWriter.close();
        System.out.println("Zakończono zapis do pliku");
    }
}
